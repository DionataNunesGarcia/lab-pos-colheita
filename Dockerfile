FROM php:7.3.6-fpm-alpine3.9

LABEL maintainer="Pablo Sousa <pablosousa.ads@gmail.com>"

# install extensions and aplications linux
RUN apk add --no-cache openssl bash mysql-client icu-dev
RUN docker-php-ext-install pdo pdo_mysql

RUN docker-php-ext-install intl

# install composer
RUN curl -sS https://getcomposer.org/installer | php -- \
  --install-dir=/usr/local/bin --filename=composer

# install dockerizdr to controll dependencies containers
ENV DOCKERIZE_VERSION v0.6.1
RUN wget https://github.com/jwilder/dockerize/releases/download/$DOCKERIZE_VERSION/dockerize-alpine-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
  && tar -C /usr/local/bin -xzvf dockerize-alpine-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
  && rm dockerize-alpine-linux-amd64-$DOCKERIZE_VERSION.tar.gz

WORKDIR /var/www
RUN rm -rf /var/www/html
RUN ln -s webroot html

COPY ./.docker/app/php.ini /usr/local/etc/php/conf.d/

EXPOSE 9000

ENTRYPOINT ["php-fpm"]
