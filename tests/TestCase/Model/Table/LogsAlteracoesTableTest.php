<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\LogsAlteracoesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\LogsAlteracoesTable Test Case
 */
class LogsAlteracoesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\LogsAlteracoesTable
     */
    public $LogsAlteracoes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.logs_alteracoes'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('LogsAlteracoes') ? [] : ['className' => LogsAlteracoesTable::class];
        $this->LogsAlteracoes = TableRegistry::getTableLocator()->get('LogsAlteracoes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->LogsAlteracoes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
