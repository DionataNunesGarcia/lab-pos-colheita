<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SegsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SegsTable Test Case
 */
class SegsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SegsTable
     */
    public $Segs;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.segs'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Segs') ? [] : ['className' => SegsTable::class];
        $this->Segs = TableRegistry::getTableLocator()->get('Segs', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Segs);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
