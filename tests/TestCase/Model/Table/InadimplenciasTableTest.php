<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\InadimplenciasTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\InadimplenciasTable Test Case
 */
class InadimplenciasTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\InadimplenciasTable
     */
    public $Inadimplencias;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.inadimplencias',
        'app.clientes'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Inadimplencias') ? [] : ['className' => InadimplenciasTable::class];
        $this->Inadimplencias = TableRegistry::getTableLocator()->get('Inadimplencias', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Inadimplencias);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
