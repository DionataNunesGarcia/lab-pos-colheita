<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\FundamentosBrkTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\FundamentosBrkTable Test Case
 */
class FundamentosBrkTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\FundamentosBrkTable
     */
    public $FundamentosBrk;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.fundamentos_brk',
        'app.fundamentos_brk_pessoas'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('FundamentosBrk') ? [] : ['className' => FundamentosBrkTable::class];
        $this->FundamentosBrk = TableRegistry::getTableLocator()->get('FundamentosBrk', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->FundamentosBrk);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
