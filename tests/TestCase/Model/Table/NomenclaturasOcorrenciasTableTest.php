<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\NomenclaturasOcorrenciasTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\NomenclaturasOcorrenciasTable Test Case
 */
class NomenclaturasOcorrenciasTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\NomenclaturasOcorrenciasTable
     */
    public $NomenclaturasOcorrencias;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.nomenclaturas_ocorrencias',
        'app.nomenclaturas_relatorios',
        'app.ocorrencias'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('NomenclaturasOcorrencias') ? [] : ['className' => NomenclaturasOcorrenciasTable::class];
        $this->NomenclaturasOcorrencias = TableRegistry::getTableLocator()->get('NomenclaturasOcorrencias', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->NomenclaturasOcorrencias);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
