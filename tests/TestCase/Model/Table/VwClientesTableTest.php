<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\VwClientesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\VwClientesTable Test Case
 */
class VwClientesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\VwClientesTable
     */
    public $VwClientes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.vw_clientes',
        'app.pessoa_juridicas',
        'app.vendedores',
        'app.enderecos'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('VwClientes') ? [] : ['className' => VwClientesTable::class];
        $this->VwClientes = TableRegistry::getTableLocator()->get('VwClientes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->VwClientes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
