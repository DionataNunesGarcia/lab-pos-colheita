<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\MetasPessoasTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\MetasPessoasTable Test Case
 */
class MetasPessoasTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\MetasPessoasTable
     */
    public $MetasPessoas;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.metas_pessoas',
        'app.metas',
        'app.supervisores',
        'app.gerentes',
        'app.vendedores'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('MetasPessoas') ? [] : ['className' => MetasPessoasTable::class];
        $this->MetasPessoas = TableRegistry::getTableLocator()->get('MetasPessoas', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->MetasPessoas);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
