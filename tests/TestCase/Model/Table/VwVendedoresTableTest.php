<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\VwVendedoresTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\VwVendedoresTable Test Case
 */
class VwVendedoresTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\VwVendedoresTable
     */
    public $VwVendedores;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.vw_vendedores',
        'app.usuarios',
        'app.pessoa_fisicas',
        'app.supervisors'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('VwVendedores') ? [] : ['className' => VwVendedoresTable::class];
        $this->VwVendedores = TableRegistry::getTableLocator()->get('VwVendedores', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->VwVendedores);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
