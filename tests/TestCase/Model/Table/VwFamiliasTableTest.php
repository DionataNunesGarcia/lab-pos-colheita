<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\VwFamiliasTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\VwFamiliasTable Test Case
 */
class VwFamiliasTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\VwFamiliasTable
     */
    public $VwFamilias;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.vw_familias',
        'app.categorias',
        'app.grupos'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('VwFamilias') ? [] : ['className' => VwFamiliasTable::class];
        $this->VwFamilias = TableRegistry::getTableLocator()->get('VwFamilias', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->VwFamilias);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
