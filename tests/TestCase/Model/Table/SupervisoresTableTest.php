<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SupervisoresTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SupervisoresTable Test Case
 */
class SupervisoresTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SupervisoresTable
     */
    public $Supervisores;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.supervisores',
        'app.gerentes',
        'app.pessoas_fisicas',
        'app.usuarios',
        'app.ranking'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Supervisores') ? [] : ['className' => SupervisoresTable::class];
        $this->Supervisores = TableRegistry::getTableLocator()->get('Supervisores', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Supervisores);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
