<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CevTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CevTable Test Case
 */
class CevTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CevTable
     */
    public $Cev;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.cev',
        'app.clientes'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Cev') ? [] : ['className' => CevTable::class];
        $this->Cev = TableRegistry::getTableLocator()->get('Cev', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Cev);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
