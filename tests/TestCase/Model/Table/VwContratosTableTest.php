<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\VwContratosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\VwContratosTable Test Case
 */
class VwContratosTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\VwContratosTable
     */
    public $VwContratos;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.vw_contratos',
        'app.tipo_acordos',
        'app.clientes'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('VwContratos') ? [] : ['className' => VwContratosTable::class];
        $this->VwContratos = TableRegistry::getTableLocator()->get('VwContratos', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->VwContratos);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
