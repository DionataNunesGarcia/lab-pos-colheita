<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PrazosPedidosHistoricosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PrazosPedidosHistoricosTable Test Case
 */
class PrazosPedidosHistoricosTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\PrazosPedidosHistoricosTable
     */
    public $PrazosPedidosHistoricos;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.prazos_pedidos_historicos',
        'app.pedidos_historicos'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('PrazosPedidosHistoricos') ? [] : ['className' => PrazosPedidosHistoricosTable::class];
        $this->PrazosPedidosHistoricos = TableRegistry::getTableLocator()->get('PrazosPedidosHistoricos', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->PrazosPedidosHistoricos);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
