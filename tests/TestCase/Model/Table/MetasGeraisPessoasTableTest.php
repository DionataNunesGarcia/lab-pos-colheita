<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\MetasGeraisPessoasTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\MetasGeraisPessoasTable Test Case
 */
class MetasGeraisPessoasTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\MetasGeraisPessoasTable
     */
    public $MetasGeraisPessoas;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.metas_gerais_pessoas',
        'app.metas_gerais',
        'app.vendedores',
        'app.gerentes',
        'app.supervisores'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('MetasGeraisPessoas') ? [] : ['className' => MetasGeraisPessoasTable::class];
        $this->MetasGeraisPessoas = TableRegistry::getTableLocator()->get('MetasGeraisPessoas', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->MetasGeraisPessoas);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
