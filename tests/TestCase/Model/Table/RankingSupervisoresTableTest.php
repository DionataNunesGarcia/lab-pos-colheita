<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\RankingSupervisoresTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\RankingSupervisoresTable Test Case
 */
class RankingSupervisoresTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\RankingSupervisoresTable
     */
    public $RankingSupervisores;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.ranking_supervisores',
        'app.supervisores'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('RankingSupervisores') ? [] : ['className' => RankingSupervisoresTable::class];
        $this->RankingSupervisores = TableRegistry::getTableLocator()->get('RankingSupervisores', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->RankingSupervisores);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
