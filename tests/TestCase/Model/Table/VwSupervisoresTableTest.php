<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\VwSupervisoresTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\VwSupervisoresTable Test Case
 */
class VwSupervisoresTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\VwSupervisoresTable
     */
    public $VwSupervisores;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.vw_supervisores',
        'app.gerentes',
        'app.pessoa_fisicas',
        'app.usuarios'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('VwSupervisores') ? [] : ['className' => VwSupervisoresTable::class];
        $this->VwSupervisores = TableRegistry::getTableLocator()->get('VwSupervisores', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->VwSupervisores);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
