<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\VwNiveisTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\VwNiveisTable Test Case
 */
class VwNiveisTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\VwNiveisTable
     */
    public $VwNiveis;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.vw_niveis'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('VwNiveis') ? [] : ['className' => VwNiveisTable::class];
        $this->VwNiveis = TableRegistry::getTableLocator()->get('VwNiveis', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->VwNiveis);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
