<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\MetasGeraisTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\MetasGeraisTable Test Case
 */
class MetasGeraisTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\MetasGeraisTable
     */
    public $MetasGerais;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.metas_gerais'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('MetasGerais') ? [] : ['className' => MetasGeraisTable::class];
        $this->MetasGerais = TableRegistry::getTableLocator()->get('MetasGerais', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->MetasGerais);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
