<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\VendedoresValoresDiariosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\VendedoresValoresDiariosTable Test Case
 */
class VendedoresValoresDiariosTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\VendedoresValoresDiariosTable
     */
    public $VendedoresValoresDiarios;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.vendedores_valores_diarios',
        'app.vendedors'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('VendedoresValoresDiarios') ? [] : ['className' => VendedoresValoresDiariosTable::class];
        $this->VendedoresValoresDiarios = TableRegistry::getTableLocator()->get('VendedoresValoresDiarios', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->VendedoresValoresDiarios);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
