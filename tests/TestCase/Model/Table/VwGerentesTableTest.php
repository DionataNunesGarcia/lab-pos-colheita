<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\VwGerentesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\VwGerentesTable Test Case
 */
class VwGerentesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\VwGerentesTable
     */
    public $VwGerentes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.vw_gerentes',
        'app.usuarios',
        'app.pessoa_fisicas'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('VwGerentes') ? [] : ['className' => VwGerentesTable::class];
        $this->VwGerentes = TableRegistry::getTableLocator()->get('VwGerentes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->VwGerentes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
