<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SiglasTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SiglasTable Test Case
 */
class SiglasTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SiglasTable
     */
    public $Siglas;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.siglas'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Siglas') ? [] : ['className' => SiglasTable::class];
        $this->Siglas = TableRegistry::getTableLocator()->get('Siglas', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Siglas);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
