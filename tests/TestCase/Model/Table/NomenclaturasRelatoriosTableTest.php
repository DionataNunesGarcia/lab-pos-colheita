<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\NomenclaturasRelatoriosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\NomenclaturasRelatoriosTable Test Case
 */
class NomenclaturasRelatoriosTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\NomenclaturasRelatoriosTable
     */
    public $NomenclaturasRelatorios;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.nomenclaturas_relatorios'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('NomenclaturasRelatorios') ? [] : ['className' => NomenclaturasRelatoriosTable::class];
        $this->NomenclaturasRelatorios = TableRegistry::getTableLocator()->get('NomenclaturasRelatorios', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->NomenclaturasRelatorios);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
