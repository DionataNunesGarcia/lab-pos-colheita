<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PrazosPedidosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PrazosPedidosTable Test Case
 */
class PrazosPedidosTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\PrazosPedidosTable
     */
    public $PrazosPedidos;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.prazos_pedidos',
        'app.pedidos'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('PrazosPedidos') ? [] : ['className' => PrazosPedidosTable::class];
        $this->PrazosPedidos = TableRegistry::getTableLocator()->get('PrazosPedidos', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->PrazosPedidos);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
