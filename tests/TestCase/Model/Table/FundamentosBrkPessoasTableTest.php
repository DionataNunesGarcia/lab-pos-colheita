<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\FundamentosBrkPessoasTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\FundamentosBrkPessoasTable Test Case
 */
class FundamentosBrkPessoasTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\FundamentosBrkPessoasTable
     */
    public $FundamentosBrkPessoas;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.fundamentos_brk_pessoas',
        'app.supervisores',
        'app.gerentes',
        'app.vendedores',
        'app.fundamentos_brk'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('FundamentosBrkPessoas') ? [] : ['className' => FundamentosBrkPessoasTable::class];
        $this->FundamentosBrkPessoas = TableRegistry::getTableLocator()->get('FundamentosBrkPessoas', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->FundamentosBrkPessoas);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
