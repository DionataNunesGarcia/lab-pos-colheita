<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CategoriasVarejosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CategoriasVarejosTable Test Case
 */
class CategoriasVarejosTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CategoriasVarejosTable
     */
    public $CategoriasVarejos;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.categorias_varejos'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('CategoriasVarejos') ? [] : ['className' => CategoriasVarejosTable::class];
        $this->CategoriasVarejos = TableRegistry::getTableLocator()->get('CategoriasVarejos', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CategoriasVarejos);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test novo method
     *
     * @return void
     */
    public function testNovo()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buscar method
     *
     * @return void
     */
    public function testBuscar()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test salvar method
     *
     * @return void
     */
    public function testSalvar()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
