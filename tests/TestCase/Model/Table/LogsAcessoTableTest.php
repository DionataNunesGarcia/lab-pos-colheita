<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\LogsAcessoTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\LogsAcessoTable Test Case
 */
class LogsAcessoTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\LogsAcessoTable
     */
    public $LogsAcesso;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.logs_acesso'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('LogsAcesso') ? [] : ['className' => LogsAcessoTable::class];
        $this->LogsAcesso = TableRegistry::getTableLocator()->get('LogsAcesso', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->LogsAcesso);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
