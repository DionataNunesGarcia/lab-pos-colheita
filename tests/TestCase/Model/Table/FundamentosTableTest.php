<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\FundamentosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\FundamentosTable Test Case
 */
class FundamentosTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\FundamentosTable
     */
    public $Fundamentos;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.fundamentos'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Fundamentos') ? [] : ['className' => FundamentosTable::class];
        $this->Fundamentos = TableRegistry::getTableLocator()->get('Fundamentos', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Fundamentos);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
