<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\MetasMacrosGvesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\MetasMacrosGvesTable Test Case
 */
class MetasMacrosGvesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\MetasMacrosGvesTable
     */
    public $MetasMacrosGves;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.metas_macros_gves'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('MetasMacrosGves') ? [] : ['className' => MetasMacrosGvesTable::class];
        $this->MetasMacrosGves = TableRegistry::getTableLocator()->get('MetasMacrosGves', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->MetasMacrosGves);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
