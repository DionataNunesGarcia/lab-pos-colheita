<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\MotivosDevolucoesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\MotivosDevolucoesTable Test Case
 */
class MotivosDevolucoesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\MotivosDevolucoesTable
     */
    public $MotivosDevolucoes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.motivos_devolucoes'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('MotivosDevolucoes') ? [] : ['className' => MotivosDevolucoesTable::class];
        $this->MotivosDevolucoes = TableRegistry::getTableLocator()->get('MotivosDevolucoes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->MotivosDevolucoes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
