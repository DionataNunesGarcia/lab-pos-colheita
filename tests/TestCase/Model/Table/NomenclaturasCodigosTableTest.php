<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\NomenclaturasCodigosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\NomenclaturasCodigosTable Test Case
 */
class NomenclaturasCodigosTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\NomenclaturasCodigosTable
     */
    public $NomenclaturasCodigos;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.nomenclaturas_codigos',
        'app.nomenclaturas_relatorios'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('NomenclaturasCodigos') ? [] : ['className' => NomenclaturasCodigosTable::class];
        $this->NomenclaturasCodigos = TableRegistry::getTableLocator()->get('NomenclaturasCodigos', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->NomenclaturasCodigos);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
