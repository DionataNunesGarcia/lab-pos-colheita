<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CoringasTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CoringasTable Test Case
 */
class CoringasTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CoringasTable
     */
    public $Coringas;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.coringas',
        'app.coringas_codigos'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Coringas') ? [] : ['className' => CoringasTable::class];
        $this->Coringas = TableRegistry::getTableLocator()->get('Coringas', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Coringas);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
