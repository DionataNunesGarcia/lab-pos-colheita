<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\MetasStmTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\MetasStmTable Test Case
 */
class MetasStmTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\MetasStmTable
     */
    public $MetasStm;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.metas_stm'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('MetasStm') ? [] : ['className' => MetasStmTable::class];
        $this->MetasStm = TableRegistry::getTableLocator()->get('MetasStm', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->MetasStm);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
