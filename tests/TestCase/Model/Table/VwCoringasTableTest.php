<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\VwCoringasTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\VwCoringasTable Test Case
 */
class VwCoringasTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\VwCoringasTable
     */
    public $VwCoringas;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.vw_coringas'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('VwCoringas') ? [] : ['className' => VwCoringasTable::class];
        $this->VwCoringas = TableRegistry::getTableLocator()->get('VwCoringas', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->VwCoringas);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
