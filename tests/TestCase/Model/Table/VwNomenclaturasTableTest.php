<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\VwNomenclaturasTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\VwNomenclaturasTable Test Case
 */
class VwNomenclaturasTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\VwNomenclaturasTable
     */
    public $VwNomenclaturas;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.vw_nomenclaturas'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('VwNomenclaturas') ? [] : ['className' => VwNomenclaturasTable::class];
        $this->VwNomenclaturas = TableRegistry::getTableLocator()->get('VwNomenclaturas', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->VwNomenclaturas);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
