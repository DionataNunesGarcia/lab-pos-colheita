<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\RelRemuneracaoTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\RelRemuneracaoTable Test Case
 */
class RelRemuneracaoTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\RelRemuneracaoTable
     */
    public $RelRemuneracao;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.rel_remuneracao'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('RelRemuneracao') ? [] : ['className' => RelRemuneracaoTable::class];
        $this->RelRemuneracao = TableRegistry::getTableLocator()->get('RelRemuneracao', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->RelRemuneracao);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
