<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\VwMetasGeraisTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\VwMetasGeraisTable Test Case
 */
class VwMetasGeraisTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\VwMetasGeraisTable
     */
    public $VwMetasGerais;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.vw_metas_gerais'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('VwMetasGerais') ? [] : ['className' => VwMetasGeraisTable::class];
        $this->VwMetasGerais = TableRegistry::getTableLocator()->get('VwMetasGerais', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->VwMetasGerais);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
