<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CoringasCodigosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CoringasCodigosTable Test Case
 */
class CoringasCodigosTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CoringasCodigosTable
     */
    public $CoringasCodigos;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.coringas_codigos',
        'app.coringas'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('CoringasCodigos') ? [] : ['className' => CoringasCodigosTable::class];
        $this->CoringasCodigos = TableRegistry::getTableLocator()->get('CoringasCodigos', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CoringasCodigos);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
