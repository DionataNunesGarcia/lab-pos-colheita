<?php
namespace App\Test\TestCase\Controller\Admin;

use App\Controller\Admin\SupervisoresController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\Admin\SupervisoresController Test Case
 */
class SupervisoresControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.supervisores',
        'app.gerentes',
        'app.pessoas_fisicas',
        'app.usuarios',
        'app.ranking',
        'app.ranking_supervisores'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
