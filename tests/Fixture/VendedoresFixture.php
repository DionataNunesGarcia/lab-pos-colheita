<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * VendedoresFixture
 *
 */
class VendedoresFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'codigo' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'dp' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'relatorios' => ['type' => 'tinyinteger', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'canal' => ['type' => 'string', 'length' => null, 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'fixo' => ['type' => 'float', 'length' => 10, 'precision' => 2, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => ''],
        'comissao' => ['type' => 'float', 'length' => 10, 'precision' => 2, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => ''],
        'usuario_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'pessoa_fisica_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'supervisor_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'fk_vendedores_usuarios1_idx' => ['type' => 'index', 'columns' => ['usuario_id'], 'length' => []],
            'fk_vendedores_pessoas_fisicas1_idx' => ['type' => 'index', 'columns' => ['pessoa_fisica_id'], 'length' => []],
            'fk_vendedores_supervisores1_idx' => ['type' => 'index', 'columns' => ['supervisor_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'fk_vendedores_pessoas_fisicas1' => ['type' => 'foreign', 'columns' => ['pessoa_fisica_id'], 'references' => ['pessoas_fisicas', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_vendedores_supervisores1' => ['type' => 'foreign', 'columns' => ['supervisor_id'], 'references' => ['supervisores', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_vendedores_usuarios1' => ['type' => 'foreign', 'columns' => ['usuario_id'], 'references' => ['usuarios', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Init method
     *
     * @return void
     */
    public function init()
    {
        $this->records = [
            [
                'id' => 1,
                'codigo' => 1,
                'dp' => 'Lorem ipsum dolor sit amet',
                'relatorios' => 1,
                'canal' => 'Lorem ipsum dolor sit amet',
                'fixo' => 1,
                'comissao' => 1,
                'usuario_id' => 1,
                'pessoa_fisica_id' => 1,
                'supervisor_id' => 1
            ],
        ];
        parent::init();
    }
}
