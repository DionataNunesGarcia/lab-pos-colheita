<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * MetasPessoasFixture
 *
 */
class MetasPessoasFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'metas_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'supervisores_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'gerentes_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'vendedores_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'fk_metas_pessoas_metas1_idx' => ['type' => 'index', 'columns' => ['metas_id'], 'length' => []],
            'fk_metas_pessoas_supervisores1_idx' => ['type' => 'index', 'columns' => ['supervisores_id'], 'length' => []],
            'fk_metas_pessoas_gerentes1_idx' => ['type' => 'index', 'columns' => ['gerentes_id'], 'length' => []],
            'fk_metas_pessoas_vendedores1_idx' => ['type' => 'index', 'columns' => ['vendedores_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'fk_metas_pessoas_gerentes1' => ['type' => 'foreign', 'columns' => ['gerentes_id'], 'references' => ['gerentes', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_metas_pessoas_metas1' => ['type' => 'foreign', 'columns' => ['metas_id'], 'references' => ['metas', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_metas_pessoas_supervisores1' => ['type' => 'foreign', 'columns' => ['supervisores_id'], 'references' => ['supervisores', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_metas_pessoas_vendedores1' => ['type' => 'foreign', 'columns' => ['vendedores_id'], 'references' => ['vendedores', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Init method
     *
     * @return void
     */
    public function init()
    {
        $this->records = [
            [
                'id' => 1,
                'metas_id' => 1,
                'supervisores_id' => 1,
                'gerentes_id' => 1,
                'vendedores_id' => 1
            ],
        ];
        parent::init();
    }
}
