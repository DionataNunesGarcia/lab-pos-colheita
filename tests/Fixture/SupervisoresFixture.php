<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * SupervisoresFixture
 *
 */
class SupervisoresFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'codigo' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'dp' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'ativo' => ['type' => 'boolean', 'length' => null, 'null' => true, 'default' => '1', 'comment' => '', 'precision' => null],
        'relatorios' => ['type' => 'boolean', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'gerente_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'pessoa_fisica_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'usuarios_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'fk_supervisores_gerentes1_idx' => ['type' => 'index', 'columns' => ['gerente_id'], 'length' => []],
            'fk_supervisores_pessoas_fisicas1_idx' => ['type' => 'index', 'columns' => ['pessoa_fisica_id'], 'length' => []],
            'fk_supervisores_usuarios1_idx' => ['type' => 'index', 'columns' => ['usuarios_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'fk_supervisores_gerentes1' => ['type' => 'foreign', 'columns' => ['gerente_id'], 'references' => ['gerentes', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_supervisores_pessoas_fisicas1' => ['type' => 'foreign', 'columns' => ['pessoa_fisica_id'], 'references' => ['pessoas_fisicas', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_supervisores_usuarios1' => ['type' => 'foreign', 'columns' => ['usuarios_id'], 'references' => ['usuarios', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Init method
     *
     * @return void
     */
    public function init()
    {
        $this->records = [
            [
                'id' => 1,
                'codigo' => 1,
                'dp' => 'Lorem ipsum dolor sit amet',
                'ativo' => 1,
                'relatorios' => 1,
                'gerente_id' => 1,
                'pessoa_fisica_id' => 1,
                'usuarios_id' => 1
            ],
        ];
        parent::init();
    }
}
