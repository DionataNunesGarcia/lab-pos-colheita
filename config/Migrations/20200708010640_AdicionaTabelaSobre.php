<?php
use Migrations\AbstractMigration;

class AdicionaTabelaSobre extends AbstractMigration
{
    /**
     *
     */
    public function up()
    {
        $table = $this->table('sobre');
        $table
            ->addColumn('titulo', 'string', [
                'default' => null,
                'null' => false,
                'limit' => 255
            ])
            ->addColumn('sobre', 'text', [
                'default' => null,
                'null' => false,
            ])
            ->addColumn('servicos', 'text', [
                'default' => null,
                'null' => false,
            ])
            ->addColumn('missao', 'text', [
                'default' => null,
                'null' => false,
            ])
            ->addColumn('visao', 'text', [
                'default' => null,
                'null' => false,
            ])
            ->addColumn('valores', 'text', [
                'default' => null,
                'null' => false,
            ])
            ->addColumn('modificado', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->create();
    }

    /**
     *
     */
    public function down()
    {
        $this->dropTable('sobre');
    }
}
