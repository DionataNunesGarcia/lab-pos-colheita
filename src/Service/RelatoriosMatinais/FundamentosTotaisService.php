<?php

namespace App\Service\RelatoriosMatinais;

use App\Service\BaseRelatoriosService;
use App\Service\CalculosGeraisSevice\CalculoClientesService;
use App\Service\RelatoriosCoberturas\FdsVendedoresService;
use Cake\I18n\Time;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;

/**
 * Class FundamentosTotaisService
 * @package App\Service\RelatoriosMatinais
 */
class FundamentosTotaisService extends BaseRelatoriosService
{
    private $vendedoresFrio;

    private $vendedoresAsi;

    private $vendedoresPremium;

    private $vendedor;

    private $totalFinal;

    private $calculoRemuneracao;

    private $fds;

    private $valoresVendedores;

    /**
     * @var array $conditionsTend
     */
    private $conditionsTend = [];

    /**
     * @var array $valoresRefrigeracao
     */
    private $valoresRefrigeracao = [];

    public function __construct()
    {
        parent::__construct();
    }

    public function buscarDados()
    {
        $this->setVendedoresFrio();
        $this->setVendedoresAsi();
        $this->setVendedoresPremium();

        return [
            'lastUpdate' => $this->getLastUpdate(),
            'data' => $this->getData(),
            'vendedoresFrio' => $this->getVendedoresFrio(),
            'vendedoresAsi' => $this->getVendedoresAsi(),
            'vendedoresPremium' => $this->getVendedoresPremium(),
        ];
    }

    /**
     * @return mixed
     */
    public function getVendedoresFrio()
    {
        return $this->vendedoresFrio;
    }

    /**
     *
     */
    public function setVendedoresFrio(): void
    {
        $vendedoresFrio = $this->getVendedores('Frio');

        $this->vendedoresFrio = $vendedoresFrio;
    }

    /**
     * @return mixed
     */
    public function getVendedoresAsi()
    {
        return $this->vendedoresAsi;
    }

    /**
     * @return mixed
     */
    public function getVendedoresPremium()
    {
        return $this->vendedoresPremium;
    }

    /**
     *
     */
    public function setVendedoresAsi(): void
    {
        $vendedoresAsi = $this->getVendedores('ASI');

        $this->vendedoresAsi = $vendedoresAsi;
    }

    /**
     *
     */
    public function setVendedoresPremium(): void
    {
        $vendedoresPremium = $this->getVendedores('Premium');

        $this->vendedoresPremium = $vendedoresPremium;
    }

    private function getVendedores($tipo)
    {
        return TableRegistry::getTableLocator()
            ->get('Vendedores')
            ->find('list', [
                'valueField' => function($vendedor) use ($tipo){
                    $this->setVendedor($vendedor);

                    $this->setFds();
                    $this->setTotalFinal();
                    $this->setValoresFundamentos();

                    switch ($tipo) {
                        case 'Frio':
                            $this->vendedor->valores = $this->getValoresFrio();
                            break;
                        case 'ASI':
                            $this->vendedor->valores = $this->getValoresAsi();
                            break;
                        case 'Premium':
                            $this->vendedor->valores = $this->getValoresPremium();
                            break;
                    }

                    return $this->getVendedor();
                },
                'keyField' => function($q){}
            ])
            ->contain([
                'PessoaFisica'
            ])
            ->where([
                'Vendedores.canal' => $tipo,
                'Vendedores.relatorios' => true,
                'Vendedores.considera_relatorios' => true,
            ])
            ->toArray();
    }

    /**
     * @return mixed
     */
    public function getVendedor()
    {
        return $this->vendedor;
    }

    /**
     * @param mixed $vendedor
     */
    public function setVendedor($vendedor): void
    {
        $this->vendedor = $vendedor;
    }

    /**
     * INICIO VENDEDOR FRIO
     * @return array
     */
    public function getValoresFrio()
    {
        $this->setCalculoRemuneracao();
        $remuneracao = $this->getCalculoRemuneracao();

        $this->setValoresRefrigeracao();
        $refrigeracao = $this->getValoresRefrigeracao();

        $this->setTotalFinal();

        $this->setTotalVolumeFrio($remuneracao);
        $this->setTotalExecucaoFrio($refrigeracao);
        $this->setTotalDistribuicaoFrio();
        $this->setTotalOutrosFrio();

        return [
            'trezSeicMil' => round($remuneracao['trezentosSeicentosMil'], 2),
            'premium' => round($remuneracao['premium'], 2),
            'volumeTotal' => round($remuneracao['volumeTotal'], 2),
            'coringa1' => round($remuneracao['coringa1'], 2),
            'coringa2' => round($remuneracao['coringa2'], 2),
            'refrigeracao' => $refrigeracao['calcFund'],
            'iav' =>  $this->getFundamentosIav(),
            'cobertura' => $this->getFundamentosCobTotal(),
            'fds' => round($this->getFds(), 2),
            'devolucao' => $this->getFundamentosDevolucoes(),
            'trocas' => $this->getFundamentosTrocas(),
            'terceiros' => $this->getFundamentosTerceiros(),
            'totalFinal' => $this->getTotalFinal(),
        ];
    }

    private function setTotalVolumeFrio($remuneracao)
    {
        /* INICIO VOLUME */
        //$remuneracao['3006001000']
        $this->totalFinal += $this->calculaTotalFinal($remuneracao['trezentosSeicentosMil'], 19.25);

        //$remuneracao['3006001000']
        $this->totalFinal += $this->calculaTotalFinal($remuneracao['premium'], 11.00);

        //$remuneracao['volumeTotal']
        $this->totalFinal += $this->calculaTotalFinal($remuneracao['volumeTotal'], 13.75);

        //$remuneracao['coringa1']
        $this->totalFinal += $this->calculaTotalFinal($remuneracao['coringa1'], 5.5);

        //$remuneracao['coringa2']
        $this->totalFinal += $this->calculaTotalFinal($remuneracao['coringa2'], 5.5);
        /* FIM VOLUME */
    }

    private function setTotalExecucaoFrio($refrigeracao)
    {
        /* INICIO EXECUÇÃO */
        //$refrigeracao
        $this->totalFinal += $this->calculaTotalFinal($refrigeracao, 7.5);

        //$row2['iv']
        $this->totalFinal += $this->calculaTotalFinal($this->getFundamentosIav(), 7.5);
        /* FIM EXECUÇÃO */
    }

    private function setTotalDistribuicaoFrio()
    {
        /* INICIO DISTRIBUIÇÃO */
        //$row2['cobertura']
        $this->totalFinal += $this->calculaTotalFinal($this->getFundamentosCobTotal(), 10);

        //FDS
        $this->totalFinal += $this->calculaTotalFinal($this->getFds(), 10);
        /* FIM DISTRIBUIÇÃO */
    }

    private function setTotalOutrosFrio()
    {
        /* INICIO OUTROS */
        //$row2['devolucao']
        $this->totalFinal += $this->calculaTotalFinal($this->getFundamentosDevolucoes(), 2.5);

        //$row2['trocas']
        $this->totalFinal += $this->calculaTotalFinal($this->getFundamentosTrocas(), 2.5);

        //$row2['terceiros']
        $this->totalFinal += $this->calculaTotalFinal($this->getFundamentosTerceiros(), 5);
        /* FIM OUTROS */
    }
    /**
     * FINAL VENDEDOR FRIO
     */

    /**
     * INICIO VENDEDOR ASI
     *
     * @return array
     */
    public function getValoresAsi()
    {
        $this->setCalculoRemuneracao();
        $remuneracao = $this->getCalculoRemuneracao();

        $this->setValoresRefrigeracao();
        $refrigeracao = $this->getValoresRefrigeracao();

        $this->setTotalFinal();

        $this->setTotalVolumeASI($remuneracao);
        $this->setTotalExecucaoASI($refrigeracao);
        $this->setTotalDistribuicaoASI();
        $this->setTotalOutrosASI();

        return [
            'trezSeicMil' => round($remuneracao['trezentosSeicentosMil'], 2),
            'premium' => round($remuneracao['premium'], 2),
            'volumeTotal' => round($remuneracao['volumeTotal'], 2),
            'coringa1' => round($remuneracao['coringa1'], 2),
            'coringa2' => round($remuneracao['coringa2'], 2),
            'refrigeracao' => $refrigeracao['calcFund'],
            'iav' => $this->getFundamentosIav(),
            'cobertura' => $this->getFundamentosCobTotal(),
            'fds' => round($this->getFds(), 2),
            'devolucao' => $this->getFundamentosDevolucoes(),
            'trocas' => $this->getFundamentosTrocas(),
            'terceiros' => $this->getFundamentosTerceiros(),
            'totalFinal' => $this->getTotalFinal(),
        ];
    }

    private function setTotalVolumeASI($remuneracao)
    {
        /* INICIO VOLUME */
        //$remuneracao['3006001000']
        $this->totalFinal += $this->calculaTotalFinal($remuneracao['trezentosSeicentosMil'], 6);

        //$remuneracao['3006001000']
        $this->totalFinal += $this->calculaTotalFinal($remuneracao['premium'], 21);

        //$remuneracao['volumeTotal']
        $this->totalFinal += $this->calculaTotalFinal($remuneracao['volumeTotal'], 21);

        //$remuneracao['coringa1']
        $this->totalFinal += $this->calculaTotalFinal($remuneracao['coringa1'], 6);

        //$remuneracao['coringa2']
        $this->totalFinal += $this->calculaTotalFinal($remuneracao['coringa2'], 6);
        /* FIM VOLUME */
    }

    private function setTotalExecucaoASI($refrigeracao)
    {
        /* INICIO EXECUÇÃO */
        //$refrigeracao
        $this->totalFinal += $this->calculaTotalFinal($refrigeracao, 5);

        //$row2['iv']
        $this->totalFinal += $this->calculaTotalFinal($this->getFundamentosIav(), 5);
        /* FIM EXECUÇÃO */
    }

    private function setTotalDistribuicaoASI()
    {
        /* INICIO DISTRIBUIÇÃO */
        //$row2['cobertura']
        $this->totalFinal += $this->calculaTotalFinal($this->getFundamentosCobTotal(), 14);

        //$realPercentualVendedor
        $this->totalFinal += $this->calculaTotalFinal($this->getFds(), 14);
        /* FIM DISTRIBUIÇÃO */
    }

    private function setTotalOutrosASI()
    {
        /* INICIO OUTROS */
        //['devolucao']
        $this->totalFinal += $this->calculaTotalFinal($this->getFundamentosDevolucoes(), 2.5);

        //['trocas']
        $this->totalFinal += $this->calculaTotalFinal($this->getFundamentosTrocas(), 2.5);

        //['terceiros']
        $this->totalFinal += $this->calculaTotalFinal($this->getFundamentosTerceiros(), 5);
        /* FIM OUTROS */
    }
    /**
     * Final Valores ASI
     */

    /**
     * Inicio Valores Premium
     * @return array
     */
    public function getValoresPremium()
    {
        $this->setCalculoRemuneracao();
        $remuneracao = $this->getCalculoRemuneracao();

        $this->setValoresRefrigeracao();
        $refrigeracao = $this->getValoresRefrigeracao();

        $this->setTotalFinal();

        $this->setTotalVolumePremium($remuneracao);
        $this->setTotalExecucaoPremium($refrigeracao);
        $this->setTotalDistribuicaoPremium();
        $this->setTotalOutrosPremium();

        return [
            'trezSeicMil' => round($remuneracao['trezentosSeicentosMil'], 2),
            'premium' => round($remuneracao['premium'], 2),
            'volumeTotal' => round($remuneracao['volumeTotal'], 2),
            'coringa1' => round($remuneracao['coringa1'], 2),
            'coringa2' => round($remuneracao['coringa2'], 2),
            'refrigeracao' => $refrigeracao['calcFund'],
            'iav' => $this->getFundamentosIav(),
            'cobertura' => $this->getFundamentosCobTotal(),
            'fds' => round($this->getFds(), 2),
            'devolucao' => $this->getFundamentosDevolucoes(),
            'trocas' => $this->getFundamentosTrocas(),
            'terceiros' => $this->getFundamentosTerceiros(),
            'totalFinal' => $this->getTotalFinal(),
        ];
    }

    private function setTotalVolumePremium($remuneracao)
    {
        /* INICIO VOLUME */
        //$remuneracao['3006001000']
        $this->totalFinal += $this->calculaTotalFinal($remuneracao['trezentosSeicentosMil'], 12);

        //$remuneracao['3006001000']
        $this->totalFinal += $this->calculaTotalFinal($remuneracao['premium'], 21);

        //$remuneracao['volumeTotal']
        $this->totalFinal += $this->calculaTotalFinal($remuneracao['volumeTotal'], 15);

        //$remuneracao['coringa1']
        $this->totalFinal += $this->calculaTotalFinal($remuneracao['coringa1'], 6);

        //$remuneracao['coringa2']
        $this->totalFinal += $this->calculaTotalFinal($remuneracao['coringa2'], 6);
        /* FIM VOLUME */
    }

    private function setTotalExecucaoPremium($refrigeracao)
    {
        /* INICIO EXECUÇÃO */
        //$refrigeracao
        $this->totalFinal += $this->calculaTotalFinal($refrigeracao, 5);

        //$row2['iv']
        $this->totalFinal += $this->calculaTotalFinal($this->getFundamentosIav(), 5);
        /* FIM EXECUÇÃO */
    }

    private function setTotalDistribuicaoPremium()
    {
        /* INICIO DISTRIBUIÇÃO */
        //$row2['cobertura']
        $this->totalFinal += $this->calculaTotalFinal($this->getFundamentosCobTotal(), 10);

        //$realPercentualVendedor
        $this->totalFinal += $this->calculaTotalFinal($this->getFds(), 10);
        /* FIM DISTRIBUIÇÃO */
    }

    private function setTotalOutrosPremium()
    {
        /* INICIO OUTROS */
        //$row2['devolucao']
        $this->totalFinal += $this->calculaTotalFinal($this->getFundamentosDevolucoes(), 2.5);

        //$row2['trocas']
        $this->totalFinal += $this->calculaTotalFinal($this->getFundamentosTrocas(), 2.5);

        //$row2['terceiros']
        $this->totalFinal += $this->calculaTotalFinal($this->getFundamentosTerceiros(), 5);
        /* FIM OUTROS */
    }

    /**
     * @return mixed
     */
    public function getCalculoRemuneracao()
    {
        return $this->calculoRemuneracao;
    }

    /**
     *
     */
    public function setCalculoRemuneracao(): void
    {
        $this->calculoRemuneracao = [
            'trezentosSeicentosMil' => $this->calculoTrezentosSeicentosMil(),
            'premium' => $this->calculoPremium(),
            'volumeTotal' => $this->calculoVolumeTotal(),
            'coringa1' => $this->calculCoringa1(),
            'coringa2' => $this->calculCoringa2(),
        ];
    }

    /**
     * @return array
     */
    private function calculoTrezentosSeicentosMil()
    {
        $this->conditionsTend = $this->getCondicoesNomenclaturas('600ml-+-1000ml-+-300ml');
//        $this->conditionsTend = [
//            'Familias.id IN' => [1, 14],
//        ];

        return $this->buscandoValor();
    }

    /**
     * @return array
     */
    private function calculoPremium()
    {
        $this->conditionsTend = $this->getCondicoesNomenclaturas('funda-premium');
//        $this->conditionsTend = [
//            'Produtos.codigo IN' => [
//                903024,900106,900109,900115,900118,900121,900130,
//                900500,902261,901162,902479,902832,902371,902478,902773,902581,900808,
//                900811,900820,900826,900841,900844,901781,902241,902544,902541,903025,
//                900829,900847,900856,900859,900860,900861,902420,902422,900855,902421
//            ],
//        ];

        return $this->buscandoValor();
    }

    /**
     * @return array
     */
    private function calculoVolumeTotal()
    {
        $this->conditionsTend = $this->getCondicoesNomenclaturas('fund-volume-total');
//        $this->conditionsTend = [
//            'Familias.categoria_id IN' => [1,2,3,4,5,6,7,8,9],
//        ];

        return $this->buscandoValor();
    }

    /**
     * @return array
     */
    private function calculCoringa1()
    {
        $this->conditionsTend = $this->getCondicoesNomenclaturas('fund-coringa1');
//        $this->conditionsTend = [
//            'Produtos.marca_id' => 3,
//        ];

        return $this->buscandoValor();
    }

    /**
     * @return array
     */
    private function calculCoringa2()
    {
        $this->conditionsTend = $this->getCondicoesNomenclaturas('fund-coringa2');
//        $this->conditionsTend = [
//            'Familias.id IN' => [1,4],
//        ];

        return $this->buscandoValor();
    }

    /**
     * @param $conditions
     * @return float
     */
    private function buscandoValor() :float
    {
        $valorTendenciaHl = $this->valorTendenciaHl();

        if (empty($valorTendenciaHl) || $valorTendenciaHl == 0) {
            return 0;
        }

        $pedidosTable = TableRegistry::getTableLocator()
            ->get('Pedidos');

        $conditions = $this->conditionsTend;

        $conditions['MetasPessoas.vendedores_id'] = $this->vendedor->id;
        $conditions['Metas.mes_ano'] = $this->getData()->i18nFormat('yyyy-MM-01');
        $conditions['Produtos.considera_relatorios'] = true;

        $query = $pedidosTable
            ->find()
            ->contain([
                'Clientes',
                'Produtos' => [
                    'Metas.MetasPessoas',
                    'Familias.Categorias',
                    'Marcas',
                ],
            ])
            ->where($conditions);

        $valorMetaHl = $query
            ->select([
                'meta_hl' => $query->func()->sum('Familias.hl * Metas.meta_cobertura')
            ])
            ->sumOf('meta_hl');

        if (empty($valorMetaHl)) {
            return 0;
        }

        return ($valorTendenciaHl / $valorMetaHl) * 100;
    }

    private function valorTendenciaHl()
    {
        $dateInicial = $this->getData();
        $dateFinal = $this->getData();

        $this->conditionsTend['Pedidos.data_pedido >='] = $dateInicial->i18nFormat('yyyy-MM-01');
        $this->conditionsTend['Pedidos.data_pedido <='] = $dateFinal->modify('last day of this month')->i18nFormat('yyyy-MM-dd');
        $this->conditionsTend['Clientes.vendedores_id '] = $this->vendedor->id;

        $pedidosTable = TableRegistry::getTableLocator()
            ->get('Pedidos');

        $query = $pedidosTable
            ->find()
            ->contain([
                'Clientes',
                'Produtos' => [
                    'Familias.Categorias',
                    'Marcas',
                ]
            ])
            ->where($this->conditionsTend);

        $valor = $query
            ->select([
                'tend_hl' => $query->func()->sum(
                    "Pedidos.quantidade * Familias.hl"
                ),
            ])
            ->sumOf('tend_hl');

        if ($valor == 0) {
            return 0;
        }
        return ($valor / $this->getTotalDiasUteis()) * $this->getTotalDiasUteis();
    }

    /**
     * @return array
     */
    public function getValoresRefrigeracao(): array
    {
        return $this->valoresRefrigeracao;
    }

    /**
     *
     */
    public function setValoresRefrigeracao(): void
    {
        $valoresRefrigeracao = TableRegistry::getTableLocator()
            ->get('CalculoRefrigeracao')
            ->getValores($this->vendedor->id);

        $this->valoresRefrigeracao = $valoresRefrigeracao;
    }

    /**
     * @return mixed
     */
    public function getTotalFinal()
    {
        return round($this->totalFinal, 2);
    }

    /**
     * @param mixed $totalFinal
     */
    public function setTotalFinal(): void
    {
        $totalFinal = 0;

        $this->totalFinal = $totalFinal;
    }

    private function calculaTotalFinal($valor, $porcentagem)
    {
        $totalFinal = 0;
        if($valor >= 150) {
            $totalFinal = 150 * $porcentagem;

        } else if ($valor >= 80 && $valor < 100) {
            $totalFinal = ($valor * 0.5) * $porcentagem;

        }else if($valor >= 100 && $valor < 150) {
            $totalFinal = ($valor) * $porcentagem;

        } elseif ($valor < 80) {

            $totalFinal = 0;
        }

        if ($totalFinal > 0) {
            return $totalFinal / 100;
        }

        return $totalFinal;
    }


    /**
     * @return array
     */
    private function getValoresVazios()
    {
        return [
            'trezSeicMil' => 0.00,
            'premium' => 0.00,
            'volumeTotal' => 0.00,
            'coringa1' => 0.00,
            'coringa2' => 0.00,
            'refrigeracao' => 0.00,
            'iav' => 0.00,
            'cobertura' => 0.00,
            'fds' => 0.00,
            'devolucao' => 0.00,
            'trocas' => 0.00,
            'terceiros' => 0.00,
            'totalFinal' => 0.00,
        ];
    }

    /**
     * @return mixed
     */
    public function getFds()
    {
        return $this->fds;
    }

    /**
     * @param mixed $fds
     */
    public function setFds(): void
    {
        $service = new FdsVendedoresService();
        $service->setVendedor($this->vendedor);
        $service->setValoresFdsVendedor();

        $valoresFds = $service->getValoresFds();

        $this->fds = $valoresFds['porcentagem'];
    }

    public function getFundamentosTotaisVendedor()
    {
        $this->setFds();
        $this->setTotalFinal();

        switch ($this->vendedor->canal) {
            case 'Frio':
                $this->vendedor->valores = $this->getValoresFrio();
                break;
            case 'ASI':
                $this->vendedor->valores = $this->getValoresAsi();
                break;
            case 'Premium':
                $this->vendedor->valores = $this->getValoresPremium();
                break;
        }

        return $this->vendedor->valores['totalFinal'];
    }

    private function getFundamentosIav()
    {
        return $this->valoresVendedores->fundamentos_refrigeracao;
    }

    private function getFundamentosRefrigeracao()
    {
        return $this->valoresVendedores->fundamentos_iav;
    }

    private function getFundamentosCobTotal()
    {
        return $this->valoresVendedores->fundamentos_cobTotal;
    }

    private function getFundamentosFds()
    {
        return $this->valoresVendedores->fundamentos_fds;
    }

    private function getFundamentosDevolucoes()
    {
        return $this->valoresVendedores->fundamentos_devolucoes;
    }

    private function getFundamentosTrocas()
    {
        return $this->valoresVendedores->fundamentos_trocas;
    }

    private function getFundamentosTerceiros()
    {
        return $this->valoresVendedores->fundamentos_terceiros;
    }

    private function setValoresFundamentos()
    {
        $table = TableRegistry::getTableLocator()
            ->get("VendedoresValoresDiarios");

        $this->valoresVendedores = $table
            ->find()
            ->where([
                'vendedor_id' => $this->vendedor->id,
                'data' => Time::now()
            ])
            ->first();
    }
}
