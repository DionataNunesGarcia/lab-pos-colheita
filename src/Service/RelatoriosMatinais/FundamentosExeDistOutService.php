<?php

namespace App\Service\RelatoriosMatinais;

use App\Service\BaseRelatoriosService;
use App\Service\RelatoriosCoberturas\FdsVendedoresService;
use App\Service\RelatoriosMateriais\MateriaisClientesService;
use Cake\ORM\TableRegistry;

/**
 * Class FundamentosExeDistOutService
 * @package App\Service\RelatoriosMatinais
 */
class FundamentosExeDistOutService extends BaseRelatoriosService
{
    private $vendedoresFrio;
    private $vendedoresAsi;
    private $vendedoresPremium;
    private $vendedor;
    private $metasGerais;
    private $iavAtual;

    private $_vendedoresTable;
    private $_produtosTable;
    private $_pedidosTable;
    private $_metasTable;
    private $_metasGeraisTable;
    private $_clientesTable;
    private $_cevTable;
    private $_iavTable;
    private $_parametrosSistema;
    private $condicoesCobMatTT;

    /**
     * FundamentosExeDistOutService constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->_vendedoresTable = TableRegistry::getTableLocator()
            ->get('Vendedores');

        $this->_produtosTable = TableRegistry::getTableLocator()
            ->get('Produtos');

        $this->_pedidosTable = TableRegistry::getTableLocator()
            ->get('Pedidos');

        $this->_metasTable = TableRegistry::getTableLocator()
            ->get('Metas');

        $this->_metasGeraisTable = TableRegistry::getTableLocator()
            ->get('MetasGerais');

        $this->_parametrosSistema = TableRegistry::getTableLocator()
            ->get('ParametrosSistema')
            ->buscar();

        $this->_clientesTable = TableRegistry::getTableLocator()
            ->get('Clientes');

        $this->_cevTable = TableRegistry::getTableLocator()
            ->get('Cev');

        $this->_iavTable = TableRegistry::getTableLocator()
            ->get('Iav');

        $this->condicoesCobMatTT = $this->getCondicoesNomenclaturas('cob-mat-tt');
    }

    /**
     * @return array
     */
    public function getValores()
    {
        $this->setVendedoresFrio();
        $this->setVendedoresAsi();
        $this->setVendedoresPremium();
        return [
            'lastUpdate' => $this->getLastUpdate(),
            'data' => $this->getData(),
            'vendedoresFrio' => $this->getVendedoresFrio(),
            'vendedoresAsi' => $this->getVendedoresAsi(),
            'vendedoresPremium' => $this->getVendedoresPremium(),
        ];
    }

    private function getVendedores($tipo)
    {
        return $this->_vendedoresTable
            ->find('list', [
                'valueField' => function($vendedor){
                    $this->setVendedor($vendedor);
                    $this->setMetasGerais();
                    $this->setValoresVendedor();

                    return $this->getVendedor();
                },
                'keyField' => function($q){}
            ])
            ->contain([
                'PessoaFisica'
            ])
            ->where([
                'Vendedores.canal' => $tipo,
                'Vendedores.relatorios' => true,
                'Vendedores.considera_relatorios' => true
            ])
            ->toArray();
    }

    /**
     * @return mixed
     */
    public function getVendedoresFrio()
    {
        return $this->vendedoresFrio;
    }

    /**
     *
     */
    public function setVendedoresFrio(): void
    {
        $this->vendedoresFrio = $this->getVendedores('Frio');
    }

    /**
     * @return mixed
     */
    public function getVendedoresAsi()
    {
        return $this->vendedoresAsi;
    }

    /**
     * @return mixed
     */
    public function getVendedoresPremium()
    {
        return $this->vendedoresPremium;
    }

    /**
     *
     */
    public function setVendedoresAsi(): void
    {
        $this->vendedoresAsi = $this->getVendedores('ASI');
    }

    /**
     *
     */
    public function setVendedoresPremium(): void
    {
        $this->vendedoresPremium = $this->getVendedores('Premium');
    }

    /**
     * @param mixed $vendedor
     */
    public function setVendedor($vendedor): void
    {
        $this->vendedor = $vendedor;
    }

    /**
     * @return mixed
     */
    public function getVendedor()
    {
        return $this->vendedor;
    }

    /**
     * @return mixed
     */
    public function getMetasGerais()
    {
        return $this->metasGerais;
    }

    /**
     * @param mixed $metasGerais
     */
    public function setMetasGerais(): void
    {
        $metasGerais = $this->_metasGeraisTable
            ->find()
            ->contain([
                'MetasGeraisPessoas',
            ])
            ->where([
                'MetasGeraisPessoas.vendedores_id' => $this->vendedor->id,
                'MetasGerais.mes_ano' => $this->getData()->i18nFormat('yyyy-MM-01')
            ])
            ->first();

        $this->metasGerais = $metasGerais;
    }

    /**
     *
     */
    public function setValoresVendedor()
    {
        $this->vendedor->valores = [
            'refrigeracao' => $this->getRefrigeracao(),
            'iav' => $this->getIav(),
            'cobTotal' => $this->getCobTotal(),
            'fds' => $this->getFds(),
            'devolucoes' => $this->getDevolucoes(),
            'trocas' => $this->getTrocas(),
            'terceiros' => $this->getTerceiros(),
        ];
    }

    /**
     * Valores Refrigeração
     *
     * @return array
     */
    private function getRefrigeracao()
    {
        $meta = $this->_parametrosSistema->meta_material_pesado_vendedor;
        $real = $this->getRealRefrigeracaoVendedor();

        return [
            'meta' => $meta,
            'real' => $real,
            'porcentagem' => $this->getPorcentagem($meta, $real),
        ];
    }

    /**
     * @return float
     */
    private function getRealRefrigeracaoVendedor()
    {
        /**
         * Criar duas opções no “Valores e Índices” dentro dos Parâmetros do Sistema: “Meta financeira material grande” e “Meta financeira material pequeno”. Valor inteiro, deixaremos neste momento o GDE em R$ 1.26400 e o PEQ em R$ 1.000.
         * Uma vez buscado todos os clientes do vendedor que possuem material de refrigeração, é verificado quantos deles bateram a meta financeira sobre a quantidade anterior e este é o “Real” para apresentar aqui.
         * O cálculo para cada cliente é assim:
         * Cada material grande é multiplicado pelo valor dos parâmetros e o material pequeno também. Logo, um cliente que tem um material grande e um pequeno tem meta financeira de R$ 2.200. Se o cliente comprou (PEDIDOS/VALOR onde ocorrência 1 e GRUPOS 1) R$ 1.815,45 o percentual para aparecer neste campo do real será de 82,5%, logo não contará pois NÃO bateu a meta financeira.
         * Exemplo: 10 clientes possuem material de refrigeração e 6 deles bateram a meta financeira: Real de 60%.
         * O campo %, que é o seguinte a este, vai fazer =( Real / Meta ) * 100, neste exemplo ficando em Meta de 70% e Real de 60% com Percentual Final de 85,7%.
         */
        $service = new MateriaisClientesService();
        $real = $service->getRealRefrigeracaoExeDistOut($this->getVendedor()->id);

        return round($real, 2);
    }

    /**
     * Valores IAV
     *
     * @return array
     */
    public function getIav()
    {
        $this->setIavAtual();
        $meta = $this->iavAtual->visit ?? 0;
        $real = $this->iavAtual->totped ?? 0;
        return [
            'meta' => $meta,
            'real' => $real,
            'porcentagem' => round($this->getPorcentagem($meta, $real), 1),
        ];
    }

    public function setIavAtual()
    {
        $this->iavAtual = $this->_iavTable
            ->find()
            ->where([
                'vendedor_id' => $this->vendedor->id,
                'mes_ano' => $this->getData()->i18nFormat('yyyy-MM-01'),
            ])
            ->first();
    }

    /**
     * Valores COB TOTAL
     *
     * @return array
     */
    private function getCobTotal()
    {
        $meta = $this->getMetaCobTotal();
        $real = $this->getRealCobTotal();
        return [
            'meta' => $meta,
            'real' => $real,
            'porcentagem' => round($this->getPorcentagem($meta, $real), 1),
        ];
    }

    /**
     * @return int
     */
    private function getMetaCobTotal()
    {
        if (empty($this->metasGerais)) {
            return 0;
        }

        return $this->metasGerais->meta_total;
    }

    /**
     * @return int|null
     */
    private function getRealCobTotal()
    {
        $condicoes = $this->condicoesCobMatTT;

        $condicoes['Clientes.vendedores_id'] = $this->vendedor->id;
        $condicoes['Pedidos.data_pedido >='] = $this->getData()->i18nFormat('yyyy-MM-01');
        $condicoes['Pedidos.data_pedido <='] = $this->getData()
            ->modify('last day of this month')
            ->i18nFormat('yyyy-MM-dd');

        $condicoes['Produtos.considera_relatorios'] = true;

        return $this->_pedidosTable
            ->find()
            ->contain([
                'Clientes',
                'Produtos' => [
                    'Marcas',
                    'Familias.Categorias',
                ]
            ])
            ->where($condicoes)
            ->group('Pedidos.cliente_id')
            ->count();
    }

    /**
     * Valores FDS
     *
     * @return array
     */
    private function getFds()
    {
        $meta = $this->getMetaFds();
        $real = $this->getRealFds();

        return [
            'meta' => $meta,
            'real' => $real,
            'porcentagem' => round($this->getPorcentagem($meta, $real), 1),
        ];
    }

    /**
     * @return int
     */
    private function getMetaFds()
    {
        if (empty($this->getMetasGerais())) {
            return 0;
        }

        return intval($this->getMetasGerais()->demp);
    }

    /**
     * @return int
     */
    private function getRealFds()
    {
        $fdsVendedoresService = new FdsVendedoresService();

         return $fdsVendedoresService->getFdsVendedor($this->vendedor);
    }

    /**
     * Valores FDS
     *
     * @return array
     */
    private function getDevolucoes()
    {
        $meta = $this->getMetaDevolucoes();
        $real = $this->getRealDevolucoes();

        return [
            'meta' => $meta,
            'real' => round($real, 1),
            'porcentagem' => round($this->getPorcentagem($meta, $real), 1),
        ];
    }

    /**
     * @return int
     */
    private function getMetaDevolucoes()
    {
        if (empty($this->metasGerais)) {
            return 0;
        }

        return floatval($this->metasGerais->meta_dev);
    }

    /**
     * @return int
     */
    private function getRealDevolucoes()
    {
        $condicoes['Clientes.vendedores_id'] = $this->vendedor->id;

        $condicoes['Pedidos.data_pedido >='] = $this->getData()->i18nFormat('yyyy-MM-01');
        $condicoes['Pedidos.data_pedido <='] = $this->getData()
            ->modify('last day of this month')
            ->i18nFormat('yyyy-MM-dd');

        $condicoes['Pedidos.ocorrencia IN'] = [1];

        $query = $this->_pedidosTable
            ->find()
            ->contain([
                'Clientes',
            ])
            ->where($condicoes)
            ->group('Pedidos.cliente_id');

        return $query
            ->select([
                'total' => $query->func()->sum('Pedidos.devolucao * Pedidos.preco_tabela')
            ])
            ->sumOf('total');
    }

    /**
     * Valores Trocas
     *
     * @return array
     */
    private function getTrocas()
    {
        $meta = $this->getMetaTrocas();
        $real = $this->getRealTrocas();

        return [
            'meta' => $meta,
            'real' => round($real, 1),
            'porcentagem' => round($this->getPorcentagem($meta, $real), 1),
        ];
    }

    /**
     * @return int
     */
    private function getMetaTrocas()
    {
        if (empty($this->metasGerais)) {
            return 0;
        }

        return floatval($this->metasGerais->meta_finan_troca);
    }

    /**
     * @return int
     */
    private function getRealTrocas()
    {
        $condicoes['Clientes.vendedores_id'] = $this->vendedor->id;

        $condicoes['Pedidos.data_pedido >='] = $this->getData()->i18nFormat('yyyy-MM-01');
        $condicoes['Pedidos.data_pedido <='] = $this->getData()
            ->modify('last day of this month')
            ->i18nFormat('yyyy-MM-dd');

        $condicoes['Pedidos.ocorrencia IN'] = [3];

        $query = $this->_pedidosTable
            ->find()
            ->contain([
                'Clientes',
            ])
            ->where($condicoes)
            ->group('Pedidos.cliente_id');

        return $query
            ->select([
                'total' => $query->func()->sum('Pedidos.devolucao * Pedidos.preco_tabela')
            ])
            ->sumOf('total');
    }

    /**
     * Valores Terceiros
     *
     * @return array
     */
    private function getTerceiros()
    {
        $meta = $this->getMetaTerceiros();
        $real = $this->getRealTerceiros();

        return [
            'meta' => $meta,
            'real' => round($real, 1),
            'porcentagem' => round($this->getPorcentagem($meta, $real), 1),
        ];
    }

    /**
     * @return int
     */
    private function getMetaTerceiros()
    {
        if (empty($this->metasGerais)) {
            return 0;
        }

        return $this->metasGerais->meta_finan_terc;
    }

    /**
     * @return int
     */
    private function getRealTerceiros()
    {
        $condicoes['Clientes.vendedores_id'] = $this->vendedor->id;

        $condicoes['Pedidos.data_pedido >='] = $this->getData()->i18nFormat('yyyy-MM-01');
        $condicoes['Pedidos.data_pedido <='] = $this->getData()
            ->modify('last day of this month')
            ->i18nFormat('yyyy-MM-dd');

        $condicoes['Familias.grupo_id IN'] = [2];
        $condicoes['Produtos.considera_relatorios'] = true;

        $query = $this->_pedidosTable
            ->find()
            ->contain([
                'Clientes',
                'Produtos.Familias',
            ])
            ->where($condicoes)
            ->group('Pedidos.cliente_id');

        return $query
            ->select([
                'total' => $query->func()->sum('Pedidos.valor')
            ])
            ->sumOf('total');
    }
}
