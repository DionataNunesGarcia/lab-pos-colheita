<?php

namespace App\Service\RelatoriosCoberturas;

use App\Model\Entity\Vendedore;
use App\Service\BaseRelatoriosService;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;

class FdsClientesService extends BaseRelatoriosService
{
    private $_clientesTable;
    private $_pedidosTable;
    private $_pedidosHistoricosTable;
    private $_vendedoresTable;

    private $fds;

    /**
     * @var int $vendedorId
     */
    private $vendedorId;

    /**
     * @var Vendedore $vendedor
     */
    private $vendedor;

    /**
     * @var $pastas
     */
    private $pastas;

    /**
     * @var int
     */
    private $clienteId;

    /**
     * @var string
     */
    private $canal;

    /**
     * @var array
     */
    private $clientes;

    /**
     * @var $mesAno
     */
    private $mesAno;

    public function __construct()
    {
        parent::__construct();

        $this->_clientesTable = TableRegistry::getTableLocator()
            ->get("Clientes");

        $this->_pedidosTable = TableRegistry::getTableLocator()
            ->get("Pedidos");

        $this->_pedidosHistoricosTable = TableRegistry::getTableLocator()
            ->get("PedidosHistoricos");

        $this->_vendedoresTable = TableRegistry::getTableLocator()
            ->get("Vendedores");

    }

    public function getValores()
    {
        $this->setVendedor();
        $this->setClientes();

        return [
            'lastUpdate' => $this->getLastUpdate(),
            'vendedor' => $this->getVendedor(),
            'data' => $this->getMesAno(),
            'clientes' => $this->getClientes(),
        ];
    }

    /**
     * @return mixed
     */
    public function getClientes()
    {
        return $this->clientes;
    }

    /**
     *
     */
    public function setClientes(): void
    {
        $clientes = $this->_clientesTable
            ->find('list', [
                'keyField' => function($cliente){},
                'valueField' => function($cliente) {

                    $this->setClienteId($cliente->id);
                    $this->setCanal($cliente->categoriaVarejo->demp->nome);

                    $cliente->canal_fds = $cliente->categoriaVarejo->demp->nome;
                    $cliente->codigo_str = "{$cliente->regiao}-{$cliente->numero}";

                    $this->setFds();
                    $cliente->fds = $this->fdsPorCanal();
                    return $cliente;
                },
            ])
            ->contain([
                'PessoasJuridicas',
                'CategoriasVarejos.Demps',
            ])
            ->where($this->getCondicoes())
            ->order([
                'Demps.nome' => 'ASC',
                'PessoasJuridicas.nome_fantasia' => 'ASC',
            ])
            ->toArray();

        $this->clientes = $clientes;
    }

    /**
     * @return array
     */
    private function getCondicoes()
    {
        $conditions['Clientes.vendedores_id'] = $this->getVendedorId();
        if(!empty($this->getPastas())) {
            $conditions['Clientes.pasta IN'] = $this->getPastas();
        }

        return $conditions;
    }

    /**
     *
     */
    public function setVendedor(): void
    {
        $vendedor = $this->_vendedoresTable
            ->find()
            ->contain([
                'PessoaFisica'
            ])
            ->where([
                'Vendedores.id' => $this->getVendedorId(),
            ])
            ->first();

        $this->vendedor = $vendedor;
    }

    /**
     * @return int
     */
    public function getVendedorId(): int
    {
        return $this->vendedorId;
    }

    /**
     * @param int $vendedorId
     */
    public function setVendedorId(int $vendedorId): void
    {
        $this->vendedorId = $vendedorId;
    }

    /**
     * @return mixed
     */
    public function getVendedor()
    {
        return $this->vendedor;
    }

    /**
     * @return mixed
     */
    public function getPastas()
    {
        return $this->pastas;
    }

    /**
     * @param mixed $pastas
     */
    public function setPastas($pastas): void
    {
        $this->pastas = explode(',', $pastas);
    }

    /**
     * @return int
     */
    public function getClienteId(): int
    {
        return $this->clienteId;
    }

    /**
     * @param int $clienteId
     */
    public function setClienteId(int $clienteId): void
    {
        $this->clienteId = $clienteId;
    }

    /**
     * @return string
     */
    public function getCanal(): string
    {
        return $this->canal;
    }

    /**
     * @param string $canal
     */
    public function setCanal(string $canal): void
    {
        $this->canal = $canal;
    }

    /**
     * @return \stdClass
     */
    private function fdsPorCanal()
    {
        switch ($this->getCanal()) {
            case 'ASI 1-4':
                return $this->fdsAsi14();
            case 'BAR A-B':
                return $this->fdsBarAB();
            case 'BAR C-D':
                return $this->fdsBarCD();
            case 'CHOPERIA':
                return $this->fdsChoperia();
            case 'REST-LANCH':
                return $this->fdsRestLanch();
            case 'CONVENIENCIA':
                return $this->fdsConveniencia();
            case 'TRAD':
                return $this->fdsTrad();
            case 'ADEGA':
                return $this->fdsAdega();
        }
    }

    /**
     * @return int
     */
    public function fdsPorCliente()
    {
        switch ($this->getCanal()) {
            case 'ASI 1-4':
                return $this->totalAsi14();
            case 'BAR A-B':
                return $this->totalBarAB();
            case 'BAR C-D':
                return $this->totalBarCD();
            case 'CHOPERIA':
                return $this->totalChoperia();
            case 'REST-LANCH':
                return $this->totalRestLanch();
            case 'CONVENIENCIA':
                return $this->totalConveniencia();
            case 'TRAD':
                return $this->totalTrad();
            case 'ADEGA':
                return $this->totalAdega();
        }
    }

    /**
     * @return int
     */
    private function totalAsi14()
    {
        if ($this->fdsPorSlug('fds-s-hero-eisen') == 0) {
            return 0;
        }
        if ($this->fdsPorSlug('fds-eisen-355') == 0) {
            return 0;
        }
        if ($this->fdsPorSlug('fds-eisen-350') == 0) {
            return 0;
        }
        if ($this->fdsPorSlug('fds-dev-355') == 0) {
            return 0;
        }
        if ($this->fdsPorSlug('fds-dev-473') == 0) {
            return 0;
        }
        if ($this->fdsPorSlug('fds-schin-473') == 0) {
            return 0;
        }
        if ($this->fdsPorSlug('fds-refri-2l') == 0) {
            return 0;
        }
        if ($this->fdsPorSlug('fds-dreher') == 0) {
            return 0;
        }
        return 1;
    }

    /**
     * @return int
     */
    private function totalBarAB()
    {
        if ($this->fdsPorSlug('fds-s-hero-eisen') == 0) {
            return 0;
        }
        if ($this->fdsPorSlug('fds-eisen-355') == 0) {
            return 0;
        }
        if ($this->fdsPorSlug('fds-eisen-600') == 0) {
            return 0;
        }
        if ($this->fdsPorSlug('fds-dev-600') == 0) {
            return 0;
        }
        return 1;
    }

    /**
     * @return int
     */
    private function totalBarCD()
    {
        if ($this->fdsPorSlug('fds-eisen-355') == 0) {
            return 0;
        }
        if ($this->fdsPorSlug('fds-dev-600') == 0) {
            return 0;
        }
        if ($this->fdsPorSlug('fds-schin-600') == 0) {
            return 0;
        }
        if ($this->fdsPorSlug('fds-refri-2l') == 0) {
            return 0;
        }
        if ($this->fdsPorSlug('fds-dreher') == 0) {
            return 0;
        }

        return 1;
    }

    /**
     * @return int
     */
    private function totalChoperia()
    {
        if ($this->fdsPorSlug('fds-eisen-355') == 0) {
            return 0;
        }
        if ($this->fdsPorSlug('fds-eisen-draft') == 0) {
            return 0;
        }
        return 1;
    }

    /**
     * @return int
     */
    private function totalRestLanch()
    {
        if ($this->fdsPorSlug('fds-eisen-355') == 0) {
            return 0;
        }
        if ($this->fdsPorSlug('fds-dev-600') == 0) {
            return 0;
        }
        if ($this->fdsPorSlug('fds-schin-473') == 0) {
            return 0;
        }
        if ($this->fdsPorSlug('fds-itubaina-355') == 0) {
            return 0;
        }
        if ($this->fdsPorSlug('fds-skinka') == 0) {
            return 0;
        }

        return 1;
    }

    /**
     * @return int
     */
    private function totalConveniencia()
    {
        if ($this->fdsPorSlug('fds-s-hero-eisen') == 0) {
            return 0;
        }
        if ($this->fdsPorSlug('fds-eisen-355') == 0) {
            return 0;
        }
        if ($this->fdsPorSlug('fds-schin-473') == 0) {
            return 0;
        }
        if ($this->fdsPorSlug('fds-skinka') == 0) {
            return 0;
        }
        if ($this->fdsPorSlug('fds-eisen-350') == 0) {
            return 0;
        }
        if ($this->fdsPorSlug('fds-dev-473') == 0) {
            return 0;
        }
        if ($this->fdsPorSlug('fds-s-hero-baden') == 0) {
            return 0;
        }
        return 1;
    }

    /**
     * @return int
     */
    private function totalTrad()
    {

        if ($this->fdsPorSlug('fds-eisen-350') == 0) {
            return 0;
        }
        if ($this->fdsPorSlug('fds-dev-473') == 0) {
            return 0;
        }
        if ($this->fdsPorSlug('fds-schin-473') == 0) {
            return 0;
        }
        if ($this->fdsPorSlug('fds-skinka') == 0) {
            return 0;
        }
        if ($this->fdsPorSlug('fds-refri-2l') == 0) {
            return 0;
        }
        return 1;
    }

    /**
     * @return int
     */
    private function totalAdega()
    {
        if ($this->fdsPorSlug('fds-eisen-600') == 0) {
            return 0;
        }
        if ($this->fdsPorSlug('fds-dev-600') == 0) {
            return 0;
        }
        if ($this->fdsPorSlug('fds-dev-473') == 0) {
            return 0;
        }
        if ($this->fdsPorSlug('fds-schin-473') == 0) {
            return 0;
        }
        if ($this->fdsPorSlug('fds-schin-1l') == 0) {
            return 0;
        }
        if ($this->fdsPorSlug('fds-refri-2l') == 0) {
            return 0;
        }
        if ($this->fdsPorSlug('fds-dreher') == 0) {
            return 0;
        }
        return 1;
    }

    /**
     * @param array $condicoes
     * @return int|null
     */
    private function getPedidos(array $condicoes)
    {
        $table = $this->_pedidosTable;
        $model = 'Pedidos';
        if ($this->getMesAno() !== Time::now()->i18nFormat('yyyy-MM-01')) {
            $table = $this->_pedidosHistoricosTable;
            $model = 'PedidosHistoricos';
        }

        $condicoes[$model . '.ocorrencia IN'] = [1, 2, 4];
        $condicoes[$model . '.cliente_id'] = $this->getClienteId();

        $condicoes[$model . '.data_pedido >='] = $this->getMesAno();
        $condicoes[$model . '.data_pedido <='] = $this->getFimMesAno();

        if (!isset($condicoes['quantidade >'])) {
            $condicoes[$model.'.quantidade >='] = 1;
        }

        return $table
            ->find()
            ->contain([
                'Produtos' => [
                    'Familias.Categorias',
                    'Marcas',
                ],
            ])
            ->where($condicoes)
            ->group($model .'.cliente_id')
            ->count();
    }

    /**
     * @return \stdClass
     */
    private function fdsBarAB()
    {
        $barAB = new \stdClass();

        $barAB->heroEisen = $this->fds->heroEisen;
        $barAB->eisen355 = $this->fds->eisen355;
        $barAB->eisen600 = $this->fds->eisen600;
        $barAB->dev600 = $this->fds->dev600;

        $total = (
            $barAB->heroEisen +
            $barAB->eisen355 +
            $barAB->eisen600 +
            $barAB->dev600
        );

        $barAB->porcentagem = round(($total / 4) * 100);

        return $barAB;
    }

    /**
     * @return \stdClass
     */
    private function fdsBarCD()
    {
        $barCd = new \stdClass();

        $barCd->eisen355 = $this->fds->eisen355;
        $barCd->dev600 = $this->fds->dev600;
        $barCd->schin600 = $this->fds->schin600;
        $barCd->refri2l = $this->fds->refri2l;
        $barCd->dreher = $this->fds->dreher;

        $total = (
            $barCd->eisen355 +
            $barCd->dev600 +
            $barCd->schin600 +
            $barCd->refri2l +
            $barCd->dreher
        );

        $barCd->porcentagem = round(($total / 5) * 100);

        return $barCd;
    }

    /**
     * @return \stdClass
     */
    private function fdsChoperia()
    {
        $choperia = new \stdClass();

        $choperia->eisen355 = $this->fds->eisen355;
        $choperia->eisenDraft = $this->fds->eisenDraft;

        $total = (
            $choperia->eisen355 +
            $choperia->eisenDraft
        );

        $choperia->porcentagem = round(($total / 2) * 100);

        return $choperia;
    }

    /**
     * @return \stdClass
     */
    private function fdsRestLanch()
    {
        $restLanch = new \stdClass();

        $restLanch->eisen355 = $this->fds->eisen355;
        $restLanch->dev600 = $this->fds->dev600;
        $restLanch->schin473 = $this->fds->schin473;
        $restLanch->itubaina355 = $this->fds->itubaina355;
        $restLanch->skinka = $this->fds->skinka;

        $total = (
            $restLanch->eisen355 +
            $restLanch->dev600 +
            $restLanch->itubaina355 +
            $restLanch->skinka +
            $restLanch->schin473
        );

        $restLanch->porcentagem = round(($total / 5) * 100);

        return $restLanch;
    }

    /**
     * @return \stdClass
     */
    private function fdsTrad()
    {
        $trad = new \stdClass();

        $trad->eisen350 = $this->fds->eisen350;
        $trad->dev473 = $this->fds->dev473;
        $trad->schin473 = $this->fds->schin473;
        $trad->skinka = $this->fds->skinka;
        $trad->refri2l = $this->fds->refri2l;

        $total = (
            $trad->refri2l +
            $trad->schin473 +
            $trad->skinka +
            $trad->eisen350 +
            $trad->dev473
        );

        $trad->porcentagem = round(($total / 5) * 100);

        return $trad;
    }

    /**
     * @return \stdClass
     */
    private function fdsAsi14()
    {
        $asi114 = new \stdClass();

        $asi114->heroEisen = $this->fds->heroEisen;
        $asi114->eisen355 = $this->fds->eisen355;
        $asi114->eisen350 = $this->fds->eisen350;
        $asi114->dev355 = $this->fds->dev355;
        $asi114->dev473 = $this->fds->dev473;
        $asi114->schin473 = $this->fds->schin473;
        $asi114->refri2l = $this->fds->refri2l;
        $asi114->dreher = $this->fds->dreher;

        $total = (
            $asi114->heroEisen +
            $asi114->eisen355 +
            $asi114->eisen350 +
            $asi114->dev355 +
            $asi114->dev473 +
            $asi114->schin473 +
            $asi114->refri2l +
            $asi114->dreher
        );

        $asi114->porcentagem = round(($total / 8) * 100);

        return $asi114;
    }

    /**
     * @return \stdClass
     */
    private function fdsConveniencia()
    {
        $conveniencia = new \stdClass();

        $conveniencia->heroEisen = $this->fds->heroEisen;
        $conveniencia->eisen355 = $this->fds->eisen355;
        $conveniencia->schin473 = $this->fds->schin473;
        $conveniencia->skinka = $this->fds->skinka;
        $conveniencia->eisen350 = $this->fds->eisen350;
        $conveniencia->dev473 = $this->fds->dev473;
        $conveniencia->heroBaden = $this->fds->heroBaden;

        $total = (
            $conveniencia->heroEisen +
            $conveniencia->eisen355 +
            $conveniencia->schin473 +
            $conveniencia->skinka +
            $conveniencia->eisen350 +
            $conveniencia->dev473 +
            $conveniencia->heroBaden
        );

        $conveniencia->porcentagem = round(($total / 7) * 100);

        return $conveniencia;
    }

    /**
     * @return \stdClass
     */
    private function fdsAdega()
    {
        $adega = new \stdClass();

        $adega->eisen600 = $this->fds->eisen600;
        $adega->dev600 = $this->fds->dev600;
        $adega->dev473 = $this->fds->dev473;
        $adega->schin473 = $this->fds->schin473;
        $adega->schin1l = $this->fds->schin1l;
        $adega->refri2l = $this->fds->refri2l;
        $adega->dreher = $this->fds->dreher;

        $total = (
            $adega->eisen600 +
            $adega->dev600 +
            $adega->dev473 +
            $adega->schin473 +
            $adega->schin1l +
            $adega->refri2l +
            $adega->dreher
        );

        $adega->porcentagem = round(($total / 7) * 100);

        return $adega;
    }

    /**
     *
     */
    private function fdsPorSlug($slug)
    {
        $condicoes = $this->getCondicoesNomenclaturas($slug);

        $total = $this->getPedidos($condicoes);

        return ($total == 0) ? 0 : 1;
    }

    /**
     * @return mixed
     */
    public function getFds()
    {
        return $this->fds;
    }

    /**
     * @param mixed $fds
     */
    public function setFds(): void
    {
        $fds = new \stdClass();

        $fds->heroEisen = $this->fdsPorSlug('fds-s-hero-eisen');
        $fds->eisen355 = $this->fdsPorSlug('fds-eisen-355');
        $fds->eisen600 = $this->fdsPorSlug('fds-eisen-600');
        $fds->schin600 = $this->fdsPorSlug('fds-schin-600');
        $fds->dev600 = $this->fdsPorSlug('fds-dev-600');
        $fds->refri2l = $this->fdsPorSlug('fds-refri-2l');
        $fds->dreher = $this->fdsPorSlug('fds-dreher');
        $fds->schin473 = $this->fdsPorSlug('fds-schin-473');
        $fds->itubaina355 = $this->fdsPorSlug('fds-itubaina-355');
        $fds->skinka = $this->fdsPorSlug('fds-skinka');
        $fds->eisen350 = $this->fdsPorSlug('fds-eisen-350');
        $fds->dev473 = $this->fdsPorSlug('fds-dev-473');
        $fds->dev355 = $this->fdsPorSlug('fds-dev-355');
        $fds->heroBaden = $this->fdsPorSlug('fds-s-hero-baden');
        $fds->schin1l = $this->fdsPorSlug('fds-schin-1l');
        $fds->eisenDraft = $this->fdsPorSlug('fds-eisen-draft');

        $this->fds = $fds;
    }

    /**
     * @return mixed
     */
    public function getMesAno()
    {
        return $this->mesAno;
    }

    /**
     * @param $mesAno
     */
    public function setMesAno($mesAno = null, $formatar = true): void
    {
        $this->mesAno = Time::now()->i18nFormat('yyyy-MM-01');
        if (!empty($mesAno)) {
            $mesAno = ($formatar) ? mesAnoToDate($mesAno) : $mesAno;

            $this->mesAno = $mesAno;
        }
    }

    /**
     * @return mixed
     */
    public function getFimMesAno()
    {
        return date("Y-m-t", strtotime($this->mesAno));
    }
}
