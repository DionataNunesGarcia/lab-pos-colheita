<?php

namespace App\Service\RelatoriosCoberturas;

use App\Service\BaseRelatoriosService;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;

class FdsVendedoresService extends BaseRelatoriosService
{
    private $_vendedoresTable;
    private $_metasGeraisTable;
    private $_clientesTable;
    private $_fdsClientesService;
    private $_coberturasGeraisService;
    private $vendedores;
    private $vendedor;

    /**
     * @var array
     */
    private $clientes;

    public function __construct()
    {
        parent::__construct();

        $this->_vendedoresTable = TableRegistry::getTableLocator()
            ->get("Vendedores");

        $this->_metasGeraisTable = TableRegistry::getTableLocator()
            ->get("MetasGerais");

        $this->_clientesTable = TableRegistry::getTableLocator()
            ->get("Clientes");

        $this->_fdsClientesService = new FdsClientesService();

        $this->_coberturasGeraisService = new CoberturasGeraisService();
    }

    public function getValores()
    {
        $this->setVendedores();

        return [
            'lastUpdate' => $this->getLastUpdate(),
            'data' => $this->getData(),
            'vendedores' => $this->getVendedores(),
        ];
    }

    /**
     * @return mixed
     */
    public function getVendedores()
    {
        return $this->vendedores;
    }

    /**
     *
     */
    public function setVendedores(): void
    {
        $vendedores = $this->_vendedoresTable
            ->find('list', [
                'keyField' => function($q){},
                'valueField' => function($q){
                    $this->setVendedor($q);
                    $this->setValoresFdsVendedor();

                    return $this->getVendedor();
                },
            ])
            ->contain([
                'PessoaFisica',
            ])
            ->where([
                'Vendedores.relatorios' => true,
                'Vendedores.considera_relatorios' => true
            ])
            ->orderAsc('codigo')
            ->toArray();

        $this->vendedores = $vendedores;
    }

    /**
     * @return mixed
     */
    public function getVendedor()
    {
        return $this->vendedor;
    }

    /**
     * @param mixed $vendedor
     */
    public function setVendedor($vendedor): void
    {
        $this->vendedor = $vendedor;
    }

    /**
     *
     */
    public function setValoresFdsVendedor()
    {
        $this->vendedor->fds = $this->getTodosValoresFds();

        $clientes = $this->_clientesTable
            ->find()
            ->contain([
                'CategoriasVarejos.Demps',
            ])
            ->where([
                'vendedores_id' => $this->vendedor->id
            ])
            ->toArray();

        foreach ($clientes as $cliente) {
            $canal = $cliente->categoriaVarejo->demp->nome;
            if ($canal == 'outros') {
                continue;
            }
            $this->_fdsClientesService->setClienteId($cliente->id);
            $this->_fdsClientesService->setCanal($canal);
            $this->_fdsClientesService->setMesAno();

            $key = $this->getKeyArray($canal);
            if (array_key_exists($key, $this->vendedor->fds)) {
                $this->vendedor->fds[$key] += $this->_fdsClientesService->fdsPorCliente();
            }
        }

        $this->setOutrosValores();
    }

    /**
     * @return mixed
     */
    public function getValoresFds()
    {
        return $this->getVendedor()->fds;
    }

    private function getTodosValoresFds()
    {
        return [
            'as14' => 0,
            'barAB' => 0,
            'barCD' => 0,
            'choperia' => 0,
            'barRestLanch' => 0,
            'conveniencia' => 0,
            'trad' => 0,
            'adega' => 0,
            'meta' => 0,
            'real' => 0,
            'porcentagem' => 0,
            'cob_meta' => 0,
            'cob_real' => 0,
            'cob_porcentagem' => 0,
            'tendencia' => 0,
            'realFds' => 0,
        ];
    }

    private function getKeyArray($canal)
    {
        switch ($canal) {
            case 'ASI 1-4':
                return 'as14';
            case 'BAR A-B':
                return 'barAB';
            case 'BAR C-D':
                return 'barCD';
            case 'CHOPERIA':
                return 'choperia';
            case 'REST-LANCH':
                return 'barRestLanch';
            case 'CONVENIENCIA':
                return 'conveniencia';
            case 'TRAD':
                return 'trad';
            case 'ADEGA':
                return 'adega';
        }
    }

    /**
     * Carrega os outros valores que compoem o FDS
     */
    private function setOutrosValores()
    {
        $this->setValoresMetas();
        $this->calculaRealFds();
        $this->calculaPorcentagem();

        $this->setValoresCob();
    }

    /**
     * Carrega os valores da meta
     */
    private function setValoresMetas()
    {
        $mesAno = $this->getData()->i18nFormat('yyyy-MM-01');

        $metasGerais = $this->_metasGeraisTable
            ->find()
            ->contain([
                'MetasGeraisPessoas',
            ])
            ->where([
                'MetasGerais.mes_ano' => $mesAno,
                'MetasGeraisPessoas.vendedores_id' => $this->vendedor->id,
            ])
            ->first();

        if (!empty($metasGerais)) {
            $this->vendedor->fds['meta'] = $metasGerais->meta_total;
            $this->vendedor->fds['real'] = $metasGerais->demp;
        }
    }

    /**
     * Calcula o total de FDS
     */
    private function calculaRealFds()
    {
        $this->vendedor->fds['realFds'] = $this->vendedor->fds['as14']
            + $this->vendedor->fds['barAB']
            + $this->vendedor->fds['barCD']
            + $this->vendedor->fds['choperia']
            + $this->vendedor->fds['barRestLanch']
            + $this->vendedor->fds['conveniencia']
            + $this->vendedor->fds['trad']
            + $this->vendedor->fds['adega'];
    }

    private function calculaPorcentagem()
    {
        if ($this->vendedor->fds['realFds'] != 0 && $this->vendedor->fds['meta']) {
            $porcentagem = ($this->vendedor->fds['realFds'] / $this->vendedor->fds['meta']) * 100;
            $this->vendedor->fds['porcentagem'] = round($porcentagem);
        }
    }

    private function setValoresCob()
    {
        $totalCob = $this->_coberturasGeraisService->getValorCobTodosProdutos($this->vendedor->id);
        $this->vendedor->fds['cob_real'] = $totalCob;

        $this->setMetaRealCob($totalCob);
    }

    private function setMetaRealCob($totalCob)
    {
        $totalUteis = $this->getTotalDiasUteis();
        $totalUteisMes = $this->getTotalDiasUteisMes();

        // se for zero, não executa nenhum dos dois cálculos
        if ($totalCob == 0) {
            return;
        }

        // se o total de dias uteis for igual a zero, não executa o valor de Cob Meta
        if ($totalUteis != 0) {
            $this->vendedor->fds['cob_meta'] = round(($totalCob / $totalUteis) * $totalUteisMes);
        }

        // se a meta não for zero, faz o cálculo do cob Real
        if ($this->vendedor->fds['meta'] != 0) {
            $this->vendedor->fds['cob_real'] = round(($totalCob / $this->vendedor->fds['meta']) * 100);
        }
    }

    public function getFdsVendedor($vendedor)
    {
        $this->setVendedor($vendedor);
        $this->setValoresFdsVendedor();

        return $this->vendedor->fds['realFds'];
    }

    public function getTotalClientesSucessoVendedor($vendedorId, $mesAno)
    {
        $clientes = $this->_clientesTable
            ->find()
            ->contain([
                'CategoriasVarejos.Demps',
            ])
            ->where([
                'vendedores_id' => $vendedorId
            ])
            ->toArray();

        $total = 0;
        foreach ($clientes as $cliente) {
            $canal = $cliente->categoriaVarejo->demp->nome;
            if ($canal == 'outros') {
                continue;
            }
            $this->_fdsClientesService->setClienteId($cliente->id);
            $this->_fdsClientesService->setCanal($canal);
            $this->_fdsClientesService->setMesAno($mesAno, false);

            $total += $this->_fdsClientesService->fdsPorCliente();
        }

        return $total;
    }
}
