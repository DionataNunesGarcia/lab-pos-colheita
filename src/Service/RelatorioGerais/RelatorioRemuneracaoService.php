<?php

namespace App\Service\RelatorioGerais;

use App\Service\BaseRelatoriosService;
use App\Service\RelatoriosMatinais\FundamentosTotaisService;
use Cake\ORM\TableRegistry;

/**
 * Class RelatorioRemuneracaoService
 * @package App\Service\RelatorioGerais
 */
class RelatorioRemuneracaoService extends BaseRelatoriosService
{
    private $vendedores;
    private $vendedor;

    private $_vendedoresTable;
    private $_pedidosTable;

    public function __construct()
    {
        parent::__construct();

        $this->_vendedoresTable = TableRegistry::getTableLocator()->get('Vendedores');
        $this->_pedidosTable = TableRegistry::getTableLocator()->get('Pedidos');
    }

    /**
     * @return array
     */
    public function getValores()
    {
        $this->setVendedores();

        return [
            'lastUpdate' => $this->getLastUpdate(),
            'data' => $this->getData(),
            'vendedores' => $this->getVendedores(),
        ];
    }

    /**
     * @return mixed
     */
    public function getVendedores()
    {
        return $this->vendedores;
    }

    /**
     *
     */
    public function setVendedores(): void
    {
        ini_set('memory_limit','128M');
        $vendedores = $this->_vendedoresTable
            ->find('list', [
                'valueField' => function($vendedor){
                    $this->setVendedor($vendedor);
                    $this->setValoresVendedor();

                    return $this->getVendedor();
                },
                'keyField' => function($q){}
            ])
            ->contain([
                'PessoaFisica'
            ])
            ->where([
                'Vendedores.relatorios' => true,
                'Vendedores.considera_relatorios' => true
            ])
            ->toArray();


        $this->vendedores = $vendedores;
    }

    /**
     * @return mixed
     */
    public function getVendedor()
    {
        return $this->vendedor;
    }

    /**
     * @param mixed $vendedor
     */
    public function setVendedor($vendedor): void
    {
        $this->vendedor = $vendedor;
    }

    private function setValoresVendedor()
    {
        //Valor da Comissão Ret: Valor decimal resultante da multiplicação das duas colunas anteriores (Comissão Retornável * ROB Retornável)
        $this->vendedor->rob_ret = $this->getRobRetrabalho();
        $this->vendedor->vlr_ret = $this->vendedor->rob_ret * $this->vendedor->comissao_ret;

        //Valor da Comissão Ter: Valor decimal resultante da multiplicação das duas colunas anteriores
        // (Comissão Terceiros * ROB Terceiros)
        $this->vendedor->rob_ter = $this->getRobTerceiros();
        $this->vendedor->valor_ter = $this->vendedor->com_ter * $this->vendedor->rob_ter;
        //colocar filtro por datas

        //Fundamentos: Traz o percentual final do Relatório “Fundamentos TT”
        $service = new FundamentosTotaisService();
        $service->setVendedor($this->vendedor);

        //Valor Variável: Valor decimal resultante da multiplicação das duas colunas anteriores (Variável Fundamentos * Fundamentos)
        $this->vendedor->fundamentos_totais = $service->getFundamentosTotaisVendedor();
        $this->vendedor->valor_var = $this->vendedor->variavel * $this->vendedor->fundamentos_totais;

        //Salário: Número decimal que soma os 3 valores (Valor da Comissão Ret + Valor da Comissão Ter + Valor Variável)
        $this->vendedor->salario = $this->vendedor->vlr_ret * $this->vendedor->valor_ter * $this->vendedor->valor_var;
    }

    private function getRobRetrabalho()
    {
        $data = $this->getData();

        $condicoes['Pedidos.data_pedido >='] = $data->i18nFormat('yyyy-MM-01');
        $condicoes['Pedidos.data_pedido <='] = $data->modify('last day of this month')->i18nFormat('yyyy-MM-dd');
        $condicoes['Pedidos.ocorrencia IN'] = [1];
        $condicoes['Familias.grupo_id IN'] = [2];
        $condicoes['Clientes.vendedores_id'] = $this->vendedor->id;

        $query = $this->_pedidosTable
            ->find()
            ->contain([
                'Clientes',
                'Produtos.Familias',
            ])
            ->where($condicoes);

        return $query
            ->select([
                'total' => $query->func()->sum('Pedidos.valor')
            ])
            ->sumOf('total');
    }

    private function getRobTerceiros()
    {
        $data = $this->getData();

        $condicoes['Pedidos.data_pedido >='] = $data->i18nFormat('yyyy-MM-01');
        $condicoes['Pedidos.data_pedido <='] = $data->modify('last day of this month')->i18nFormat('yyyy-MM-dd');
        $condicoes['Pedidos.ocorrencia IN'] = [1];
        $condicoes['Produtos.familia_id IN'] = [1, 2, 4];
        $condicoes['Clientes.vendedores_id'] = $this->vendedor->id;

        $query = $this->_pedidosTable
            ->find()
            ->contain([
                'Clientes',
                'Produtos',
            ])
            ->where($condicoes);

        return $query
            ->select([
                'total' => $query->func()->sum('Pedidos.valor')
            ])
            ->sumOf('total');
    }
}
