<?php
namespace App\Service\RelatoriosMateriais;

use App\Service\BaseRelatoriosService;
use Cake\ORM\TableRegistry;

class RefrigeracaoVendedoresService extends BaseRelatoriosService
{
    private $vendedores = [];
    private $vendedor;
    private $valoresRefrigeracao;

    public function __construct()
    {
        parent::__construct();
    }

    public function getValores()
    {
        $this->setVendedores();

        return [
            'lastUpdate' => $this->getLastUpdate(),
            'data' => $this->getData(),
            'vendedores' => $this->getVendedores(),
        ];
    }

    /**
     * @return mixed
     */
    public function getVendedores()
    {
        return $this->vendedores;
    }

    /**
     *
     */
    public function setVendedores(): void
    {
        $vendedoresTable = TableRegistry::getTableLocator()
            ->get("Vendedores");

        $data = $this->getData();

        $vendedores = $vendedoresTable
            ->find('list', [
                'keyField' => function($q){},
                'valueField' => function($q){

                    $this->setVendedor($q);
                    $this->setValoresRefrigeracao($q);

                    $q->valores = $this->getValoresRefrigeracao();

                    return $q;
                },
            ])
            ->contain([
                'PessoaFisica',
            ])
            ->where([
                'Vendedores.relatorios' => true,
                'Vendedores.considera_relatorios' => true
            ])
            ->toArray();

        $this->vendedores = $vendedores;
    }

    /**
     * @param mixed $vendedor
     */
    public function setVendedor($vendedor): void
    {
        $this->vendedor = $vendedor;
    }

    /**
     * @return array
     */
    public function getValoresRefrigeracao(): array
    {
        return $this->valoresRefrigeracao;
    }

    /**
     *
     */
    public function setValoresRefrigeracao(): void
    {
        $valoresRefrigeracao = TableRegistry::getTableLocator()
            ->get('CalculoRefrigeracao')
            ->getValores($this->vendedor->id);

        $this->valoresRefrigeracao = $valoresRefrigeracao;
    }
}
