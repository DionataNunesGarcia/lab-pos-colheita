<?php

namespace App\Service;

use Cake\I18n\Time;
use Cake\ORM\TableRegistry;

class ExportarService extends BaseService
{
    public function __construct()
    {
        parent::__construct();
    }

    private $_table;

    private $nameFile;

    private $header;

    private $extensao = 'xls';

    public function exportReport($name, $valuesHeader, $valuesBody)
    {
        $this->setInicioTable();

        $this->setNameFile($name);

        $this->setHeader($valuesHeader);

        $this->setBody($valuesBody);

        $this->setFimTable();

        $this->setHeaders();

        print $this->_table;

        exit();
    }

    public function exportCsv($name, $valuesHeader, $valuesBody)
    {
        $time = Time::now()
        ->i18nFormat('yyyy-MM-dd-HHmmss');

        header('Content-Type: text/csv; charset=utf-8');
        header("Content-Disposition: attachment; filename={$time} - {$name}.csv");

        $output = fopen("php://output", "w");
        fputcsv($output, $valuesHeader, ';');

        foreach ($valuesBody as $row) {
            fputcsv($output, $row, ';');
        }
        fclose($output);
    }

    /**
     * @return mixed
     */
    public function getNameFile()
    {
        return $this->nameFile;
    }

    /**
     * @param string $name
     */
    public function setNameFile(string $name): void
    {
        $time = Time::now()
            ->i18nFormat('yyyy-MM-dd-HHmmss');

        $nameFile = "$time _ $name";

        $this->nameFile = $nameFile;
    }

    /**
     * @return mixed
     */
    public function getTable()
    {
        return $this->_table;
    }

    /**
     * @return string
     */
    public function getExtensao(): string
    {
        return $this->extensao;
    }

    /**
     * @param string $extensao
     */
    public function setExtensao(string $extensao): void
    {
        $this->extensao = $extensao;
    }

    /**
     *
     */
    public function setInicioTable(): void
    {
        $table = '<table cellpadding="0" cellspacing="0" id="sheet0" style="border:2px solid #000;padding:3px">';

        $this->_table = $table;
    }

    /**
     *
     */
    public function setFimTable(): void
    {
        $this->_table .= '</tbody>';
        $this->_table .= '</table>';

        $this->_table = str_replace("\r", "", $this->_table);
    }

    /**
     * @return mixed
     */
    public function getHeader()
    {
        return $this->header;
    }

    /**
     * @param array $array
     */
    public function setHeader(array $array): void
    {
        //create o HEADER XLS
//        $header = '<thead border="1">';
        $header = '<tr>';

        foreach ($array as $key => $value) {
            $header .= '<td border="1" align="center" style="font-weight:bold; font-size: 16px; border:1px solid #000; padding:3px; width: auto !important;">' . trim(strip_tags(correct_encoding($value))). '</td>';
        }

        $header .= '</tr>';
//        $header .= '</thead>';

        $this->_table .= $header;
    }

    private function setBody($valuesBody)
    {
        $body = '<tbody>';
        foreach ($valuesBody as $key => $entity) {
            $body .= '<tr>';
            foreach ($entity as $key => $value) {
                if (!isset($value) OR ( $value == "")) {
                    $value = "";
                } else {
                    $value = str_replace('"', '""', strip_tags(correct_encoding($value)));
                }
                $body .= '<td style="font-size: 16px; border:1px solid #000; padding:3px; width: auto !important;">' . trim($value) . '</td>';
            }
            $body .= '</tr>';
        }

        $this->_table .= $body;
    }

    private function setHeaders()
    {

        // Configurações header para forçar o download
        header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
        header("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
        header("Cache-Control: no-cache, must-revalidate");
        header("Pragma: no-cache");
        header("Content-type: application/x-msexcel");
        header("Content-Disposition: attachment; filename=\"{$this->getNameFile()}\".{$this->getExtensao()}");
        header("Content-Description: PHP Generated Data");

//        header("Cache-Control: cache, must-revalidate");
//        header("Content-type: application/x-msdownload");
//        header("Content-type: application/vnd.ms-excel");
//        header("Content-Disposition: attachment; filename={$this->getNameFile()}.xls");
//        header("Pragma: no-cache");
//        header("Expires: 0");
    }
}
