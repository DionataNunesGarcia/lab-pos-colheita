<?php


namespace App\Service\Exportar;


use App\Service\ExportarService;
use Cake\ORM\TableRegistry;

class ExportarArquivoMetasGeraisService extends ExportarService
{
    private $_vendedoresTable;

    private $vendedores;

    private $valoresHeader;

    private $valoresBody;

    public function __construct()
    {
        parent::__construct();
        $this->_vendedoresTable = TableRegistry::getTableLocator()
            ->get('Vendedores');
    }


    public function gerarArquivo()
    {
        $this->setVendedores();

        $this->setValoresBody();

        $this->setValoresHeader();

        $nome = "Padrão - Metas Gerais";

        $this->setExtensao('csv');
        $this->exportCsv($nome, $this->getValoresHeader(), $this->getValoresBody());
    }

    /**
     * @return mixed
     */
    public function getVendedores()
    {
        return $this->vendedores;
    }

    /**
     *
     */
    public function setVendedores(): void
    {
        $vendedores = $this->_vendedoresTable
            ->find()
            ->contain([
                'PessoaFisica'
            ])
            ->where([
                'relatorios' => true
            ])
            ->toArray();

        $this->vendedores = $vendedores;
    }

    private function setValoresBody()
    {
        $valoresBody = [];
        foreach ($this->getVendedores() as $vendedor) {
            $valoresBody[] =[
                'cd' => $vendedor->codigo,
                'pessoa' => $vendedor->pessoaFisica->nome,
                'Meta_ROB_HEI' => '',
                'Meta_ROB_3' => '',
                'Meta_Dev' => '',
                'M_Troca' => '',
                'C_Total' => '',
                'C_Cerveja' => '',
                'C_600_300_1000' => '',
                'C_Retornavel' => '',
                'C_300ml_R' => '',
                'C_600ml_R' => '',
                'C_1000ml_R' => '',
                'C_Premium' => '',
                'C_Eisenbahn' => '',
                'C_Eisen_600_R' => '',
                'C_Devassa' => '',
                'C_Schin' => '',
                'C_Schin_600' => '',
                'Dev_600_R' => '',
                'C_N_Alcoolico' => '',
                'FDS' => '',
                'ITEM1' => '',
                'ITEM2' => '',
                'ITEM3' => '',
                'ITEM4' => '',
                'ITEM5' => ''
            ];
        }

        $this->valoresBody = $valoresBody;
    }

    private function getValoresBody()
    {
        return $this->valoresBody;
    }

    private function setValoresHeader()
    {
        $this->valoresHeader = [
            'Cd',
            'Pessoa',
            'Meta ROB HEI',
            'Meta ROB 3',
            'Meta Dev',
            'M Troca',
            'C Total',
            'C Cerveja',
            'C 600+300+1000',
            'C Retornavel',
            'C 300ml R',
            'C 600ml R',
            'C 1000ml R',
            'C Premium',
            'C Eisenbahn',
            'C Eisen 600 R',
            'C Devassa',
            'C Schin',
            'C Schin 600',
            'Dev 600 R',
            'C N Alcoolico',
            'FDS',
            'ITEM1',
            'ITEM2',
            'ITEM3',
            'ITEM4',
            'ITEM5'
        ];
    }

    public function getValoresHeader()
    {
        return $this->valoresHeader;
    }
}
