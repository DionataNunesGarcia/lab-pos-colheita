<?php
$placeholder = !empty($userSession['id']) ? 'Só preencher se for alterar' : '';
?>
<?= $this->Form->create(null, ['action' => 'perfil', 'enctype' => 'multipart/form-data', 'id' => 'form-perfil', 'data-auth' => 'false']) ?>
<div class="box">
    <?= $this->element('admin/titulo-box', ['titulo' => '<i class="fa fa-drivers-license-o"></i> Dados Pessoais']) ?>
    <div class="box-body">
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 text-center">
            <div class="well profile">
                <figure>
                    <?php
                    if (empty($userSession['imagem'])) {
                        echo $this->Form->file('imagem_upload', ['class' => 'upload_crop', 'multiple' => false, 'accept' => 'image/*', 'label' => 'Imagem']);
                    } else {
                        ?>
                        <div class="col-md-12 no-padding">
                            <?= $this->Html->image('../' . $userSession['imagem'], ['class' => 'img-circle img-responsive img-usuario img-thumbnail']); ?>
                        </div>
                        <div class="col-md-12 text-rigth">
                            <?=
                            $this->Html->link("<i class='fa fa-trash'></i> Excluir Imagem", ['action' => 'excluir_imagem_perfil', $userSession['id']], [
                                "alt" => "Imagem",
                                'data-auth' => 'false',
                                'escape' => false,
                                'confirm' => __('Tem certeza de que deseja excluir o arquivo?'),
                                'class' => 'btn btn-danger btn-xs'
                            ]);
                            ?>
                        </div>
                    <?php } ?>
                    <div id="uploaded_image"></div>
                </figure>
                <h2>
                    <?= $userSession['usuario'] ?>
                </h2>
                <hr/>
                <div class="text-left">
                    <p>
                        <strong>Nível: </strong>
                        <?= $userSession['niveis']['nome'] ?>
                    </p>
                    <p>
                        <strong>
                            Membro desde:
                        </strong>
                        <?= dateToData($userSession['criado']) ?>
                    </p>
                </div>
            </div>
        </div>
        <div class="col-lg-9 col-md-9 col-sm-6 col-xs-12 no-padding">
            <fieldset>
                <div class="form-group col-md-4">
                    <?= $this->Form->control('usuario', [
                        'value' => $userSession['usuario'],
                        'required' => true
                    ]); ?>
                </div>
                <div class="form-group col-md-4">
                    <?= $this->Form->control('senha', ['type' => 'password', 'required' => false, 'value' => '', 'placeholder' => $placeholder]); ?>
                </div>
                <div class="form-group col-md-4">
                    <?= $this->Form->control('confimar_senha', ['type' => 'password', 'value' => '', 'required' => false, 'placeholder' => $placeholder]); ?>
                </div>
            </fieldset>
        </div>
    </div>
    <div class="box-footer text-rigth">
        <?= $this->Form->button('Salvar <i class="fa fa-save"></i>', ['class' => 'btn btn-primary pull-rigth', 'escape' => false]);?>
    </div>
</div>
<?= $this->Form->end() ?>

<?= $this->element('admin/imagem-crop-modal') ?>

<script>
    $(document).ready(function () {
        $('#form-perfil').submit(function(){
            if ($('#senha').val() !== $('#confimar-senha').val()) {
                alert('Os campos de senhas não conferem, verifique e tente novamente.');
                return false;
            }
        });
    });

    // Depois de carregar a tela
    // apaga os campos de senha que o navegador preenche automaticamente
    $(window).on('load', function () {
        setTimeout(function(){
            $('#confimar-senha').val('');
            $('#senha').val('');
        }, 300);
    });
</script>
