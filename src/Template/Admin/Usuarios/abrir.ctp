<?php
$placeholder = !empty($entidade->id) ? 'Só preencher se for alterar' : '';
$required = empty($entidade->id) ? true : false;
?>
<?= $this->Form->create($entidade, ['action' => 'salvar', 'enctype' => 'multipart/form-data', 'id' => 'form-usuario']) ?>
<div class="box">
    <?= $this->element('admin/titulo-box', ['titulo' => '<i class="fa fa-list-alt"></i> Cadastro']) ?>
    <div class="box-body">
        <fieldset>
            <?= $this->Form->hidden('id') ?>
            <div class="form-group col-md-4">
                <?= $this->Form->control('usuario'); ?>
            </div>
            <!--            <div class="form-group col-md-3">
            <?= $this->Form->control('email'); ?>
                        </div>-->
            <div class="form-group col-md-4">
                <?= $this->Form->control('senha', ['type' => 'password', 'value' => '', 'placeholder' => $placeholder, 'required' => $required]); ?>
            </div>
            <div class="form-group col-md-4">
                <?= $this->Form->control('confirmar_senha', ['type' => 'password', 'value' => '', 'placeholder' => $placeholder, 'required' => $required]); ?>
            </div>
            <div class="form-group col-md-4 radio-booton">
                <label class=" no-padding" for="ativo">Ativo</label>
                <div class="radio">                   
                    <?= $this->Form->radio('ativo', ['1' => 'Sim', '0' => 'Não'], ['label' => 'Ativo']); ?>
                </div>
            </div>
            <div class="form-group col-md-4">
                <?=
                $this->element('admin/select2', [
                    'controller' => 'niveis',
                    'name' => 'niveis_id',
                    'label' => 'Níveis',
                    'multiple' => false,
                    'required' => true,
                    'value' => $entidade->niveis_id,
                ])
                ?>
            </div>
            <div class="form-group col-md-4">
                <?php
                if (empty($entidade->imagem)) {
                    echo $this->Form->file('imagem_upload', ['class' => 'upload_crop', 'multiple' => false, 'accept' => 'image/*', 'label' => 'Imagem']);
                } else {
                    ?>
                    <div class="col-md-8 no-padding">
                        <strong>Imagem</strong><br/>
                        <?= $this->Html->image('../' . $entidade->imagem, ['class' => 'img-responsive img-usuario img-thumbnail']); ?>
                    </div>
                    <div class="col-md-4 text-rigth">
                        <?=
                        $this->Html->link("<i class='fa fa-trash'></i> Excluir", ['action' => 'excluir_imagem', $entidade->id], [
                            "alt" => "Imagem",
                            'escape' => false,
                            'confirm' => __('Tem certeza de que deseja excluir o arquivo?'),
                            'class' => 'btn btn-xs btn-danger tex'
                        ]);
                        ?>
                    </div>
                <?php } ?>
                <div id="uploaded_image"></div>
            </div>
        </fieldset>
    </div>
    <div class="box-footer">
        <?= $this->element('admin/botoes-abrir', ['id' => $entidade->id]) ?>
    </div>
</div>
<?= $this->Form->end() ?>
<?= $this->element('admin/imagem-crop-modal') ?>
