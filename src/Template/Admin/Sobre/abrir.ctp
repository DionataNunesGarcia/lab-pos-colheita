<?= $this->Form->create($entidade, ['action' => 'salvar', 'enctype' => 'multipart/form-data']) ?>
<div class="box">
    <?= $this->element('admin/titulo-box', ['titulo' => 'Dados Sistema', 'collapse' => false]) ?>
    <div class="box-body">
        <?= $this->Form->hidden('id') ?>
        <div class="row">
            <div class="form-group col-md-6">
                <?= $this->Form->control('titulo'); ?>
            </div>
            <div class="form-group col-md-12">
                <?= $this->Form->control('sobre', ['type' => 'textarea', 'class' => 'ckeditor', 'label' => 'Sobre']); ?>
            </div>
            <div class="form-group col-md-12">
                <?= $this->Form->control('servicos', ['type' => 'textarea', 'class' => 'ckeditor', 'label' => 'Serviços']); ?>
            </div>
            <div class="form-group col-md-12">
                <?= $this->Form->control('missao', ['type' => 'textarea', 'class' => 'ckeditor', 'label' => 'Missão']); ?>
            </div>
            <div class="form-group col-md-12">
                <?= $this->Form->control('visao', ['type' => 'textarea', 'class' => 'ckeditor', 'label' => 'Visão']); ?>
            </div>
            <div class="form-group col-md-12">
                <?= $this->Form->control('valores', ['type' => 'textarea', 'class' => 'ckeditor', 'label' => 'Valores']); ?>
            </div>
        </div>
    </div>
    <div class="box-footer">
        <?= $this->Form->button('<i class="fa fa-save"></i> Salvar', ['class' => 'btn btn-primary', 'escape' => false]); ?>
    </div>
</div>
<?= $this->Form->end() ?>
