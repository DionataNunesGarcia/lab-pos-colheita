<?= $this->Form->create(null, ['type' => 'get']); ?>
<div class="box">
    <?= $this->element('admin/titulo-box', ['titulo' => '<i class="fa fa-filter"></i> Filtrar']) ?>
    <div class="box-body">
        <div class="col-md-6">  
            <?=
            $this->Form->input('texto', ['class' => 'form-control', 'label' => false,
                'placeholder' => 'Pesquse por Texto',
                'autofocus' => true,
                'value' => $this->request->query('texto')]);
            ?>
        </div>
    </div>
    <div class="box-footer">
        <div class="pull-right">
            <?= $this->element('admin/pesquisar/botoes-filtro') ?>
        </div>
    </div>
</div>
<?= $this->Form->end(); ?>
<!-- Default box -->
<div class="box">        
    <?= $this->element('admin/titulo-box', ['titulo' => '<i class="fa fa-table"></i> Registros']) ?>
    <div class="box-body">            
        <?= $this->element('admin/pesquisar/botoes') ?>
        <div class="table-responsive no-padding">
            <table cellpadding="0" cellspacing="0" class="table table-striped">
                <thead>
                    <tr>
                        <th scope="col"><?= $this->Paginator->sort('titulo') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('assunto') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('criado') ?></th>
                        <th scope="col" class="actions"><?= __('Actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($entidade as $item): ?>
                        <tr>
                            <td><?= h($item->titulo) ?></td>
                            <td><?= h($item->assunto) ?></td>
                            <td><?= dateToDataTime($item->criado) ?></td>
                            <td class="actions">
                                <?=
                                $this->Html->link(__('<i class="fa fa-eye"></i>'), [
                                    'action' => 'mensagem', $item->id, '_full' => true
                                    ], [
                                    'class' => 'btn btn-primary btn-xs',
                                    'escape' => false,
                                    'data-auth' => 'false',
                                    'data-open-modal-id' => 'visualizar_mensagem'
                                ]);
                                ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <?= $this->element('admin/pesquisar/paginacao') ?>
        </div>
    </div>
</div>
<div class="modal fade" id="visualizar_log" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true"></div>