<?= $this->Form->create($entidade, ['action' => 'salvar', 'id' => 'nomenclaturas']) ?>
<div class="box">
    <?= $this->element('admin/titulo-box', ['titulo' => 'Cadastro']) ?>
    <div class="box-body" data-url-ajax="<?=
    $this->Url->build([
        'prefix' => 'admin',
        'action' => 'mostrarCodigosTipos',
        '_full' => true
    ]);
    ?>">
        <fieldset>
            <div class="col-md-12 no-padding">
                <div class="form-group col-md-4">
                    <?=
                    $this->Form->control('nome', [
                        'title' => __('Nome da especifico para identificar em qual relatório está sendo usado')
                    ]);
                    ?>
                </div>
                <div class="form-group col-md-4">
                    <label for="tipo">Tipo</label>
                    <?=
                    $this->Form->select('tipo', tipoNomenclaturas(), [
                        'id' => 'tipo',
                        'title' => __('Escolha qual o tipo de relação será usada esse registro'),
                    ]);
                    ?>
                </div>
                <div class="form-group col-md-4">
                    <?=
                    $this->element('admin/select2', [
                        'controller' => 'Ocorrencias',
                        'name' => 'ocorrencias',
                        'label' => 'Ocorrências',
                        'multiple' => true,
                        'required' => true,
                        'value' => $entidade->ocorrencias,
                    ]);
                    ?>
                </div>
            </div>
            <div class="col-md-12 no-padding">
                <div class="form-group col-md-4">
                    <label for="volume">Volume</label>
                    <?=
                    $this->Form->select('volume', volumeNomenclaturas(), [
                        'value' => $entidade->volume,
                        'id' => 'volume'
                    ]);
                    ?>
                </div>
                <div class="form-group col-md-4" id="tipo-cobertura-hide">
                    <label for="tipo-cobertura">Tipo Cobertura</label>
                    <?=
                    $this->Form->select('tipo_cobertura', tipoCobertura(), [
                        'value' => $entidade->tipo_cobertura,
                        'empty' => '-- Selecione um tipo --',
                        'id' => 'tipo-cobertura',
                    ]);
                    ?>
                </div>
            </div>
            <div class="col-md-12">
                <hr/>
                <strong>Selecione os filtros para os tipos selecionados</strong>
                <div id="mostrar-codigos">
                </div>
            </div>
            <?= $this->Form->hidden('id') ?>
            <?php
            if (!empty($entidade->id)) {

                $idsSalvos = [];
                foreach($entidade->codigos as $key => $codigo){
                    $idsSalvos[$key] = $key;
                }

                echo $this->Form->hidden('ids_salvos', [
                    'value' => json_encode($idsSalvos, JSON_FORCE_OBJECT)
                ]);
                echo $this->Form->hidden('tipo_salvo', [
                    'value' => $entidade->tipo
                ]);

            }
            ?>
        </fieldset>
    </div>
    <div class="box-footer">
        <?= $this->element('admin/botoes-abrir', ['id' => @$entidade->id]) ?>
    </div>
</div>
<?= $this->Form->end() ?>
