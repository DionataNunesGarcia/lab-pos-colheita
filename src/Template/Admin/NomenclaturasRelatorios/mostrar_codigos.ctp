<div class="">
    <div class="form-group">
        <?=
        $this->Form->select('codigos', $retorno, [
            'multiple' => true,
            'class' => 'selectTipo select-2-'. slug($dados['tipo']),
            'hiddenField' => false,
            'label' => $dados['tipo'],
            'value' => $dados['idsSalvos'],
        ]);
        ?>
    </div>
</div>
<script>
    var dlb2 = new DualListbox('.selectTipo', {
        availableTitle:'Selecionar ',
        selectedTitle: '<?= h($dados['tipo'])?>' + ' Selecionados',
        addButtonText: '<i class="fa fa-angle-right">',
        removeButtonText: '<i class="fa fa-angle-left">',
        addAllButtonText: '<i class="fa fa-angle-double-right">',
        removeAllButtonText: '<i class="fa fa-angle-double-left">',
        searchPlaceholder: 'Pesquise',
    });
</script>