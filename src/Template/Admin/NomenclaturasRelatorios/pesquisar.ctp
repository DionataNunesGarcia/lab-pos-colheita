<?= $this->Form->create(null, ['type' => 'get']); ?>
<div class="box">
    <?= $this->element('admin/titulo-box', ['titulo' => '<i class="fa fa-filter"></i> Filtrar']) ?>
    <div class="box-body">

        <div class="col-md-4">    
            <?=
            $this->Form->input('nome', ['class' => 'form-control', 'label' => false,
                'placeholder' => 'Pesquise por Nome',
                'autofocus' => true,
                'value' => $this->request->query('nome')]);
            ?>
        </div>
    </div>
    <div class="box-footer">
        <div class="pull-right">
            <?= $this->element('admin/pesquisar/botoes-filtro') ?>
        </div>
    </div>
</div>
<?= $this->Form->end(); ?>
<!-- Default box -->
<div class="box">        
    <?= $this->element('admin/titulo-box', ['titulo' => '<i class="fa fa-table"></i> Registros']) ?>
    <div class="box-body">            
        <?= $this->element('admin/pesquisar/botoes') ?>
        <div class="table-responsive no-padding">
            <table cellpadding="0" cellspacing="0" class="table table-striped table-bordered table-hover table-condensed">
                <thead>
                    <tr>
                        <th><?= $this->Form->checkbox('select-all', ['hiddenField' => false]); ?></th>
                        <th><?= $this->Paginator->sort('nome') ?></th>
                        <th><?= $this->Paginator->sort('volume') ?></th>
                        <th><?= $this->Paginator->sort('tipo') ?></th>
                        <th><?= $this->Paginator->sort('codigos', 'Códigos Selecionados') ?></th>
                        <th class="actions"><?= __('Actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($entidade as $item) {
                        ?>
                        <tr>
                            <td><?= $this->Form->checkbox('id', ['hiddenField' => false, 'value' => $item->id]); ?></td>
                            <td><?= h($item->nome) ?></td>  
                            <td><?= h($item->volume) ?></td>  
                            <td><?= h($item->tipo) ?></td>  
                            <td><?= limitarTexto($item->codigos, 50) ?></td>
                            <td class="actions">
                                <?= $this->element('admin/pesquisar/botoes-acoes', ['id' => $item->id, 'deletavel' => false]) ?>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
            <?= $this->element('admin/pesquisar/paginacao') ?>
        </div>
    </div>
</div>