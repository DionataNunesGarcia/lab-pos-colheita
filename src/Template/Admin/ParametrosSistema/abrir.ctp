<?= $this->Form->create($entidade, ['action' => 'salvar', 'enctype' => 'multipart/form-data']) ?>
<div class="box">
    <?= $this->element('admin/titulo-box', ['titulo' => 'Dados Sistema', 'collapse' => false]) ?>
    <div class="box-body">
        <?= $this->Form->hidden('id') ?>
        <div class="row">
            <div class="form-group col-md-6">
                <?= $this->Form->control('razao_social'); ?>
            </div>
            <div class="form-group col-md-6">
                <?= $this->Form->control('nome_fantasia'); ?>
            </div>
            <div class="form-group col-md-6">
                <?= $this->Form->control('cnpj_cpf', ['value' => cpfCnpj($entidade->cnpj_cpf), 'label' => 'CNPJ/CPF']); ?>
            </div>
            <div class="form-group col-md-6">
                <?=
                $this->element('admin/selectTags', [
                    'nome' => 'emails',
                    'label' => 'E-mails Sistemas',
                    'value' => $entidade->emails
                ]);
                ?>
            </div>
        </div>
    </div>
    <?= $this->element('admin/titulo-box', ['titulo' => 'Configurações Gerais', 'collapse' => false]) ?>
    <div class="box-body">
        <div class="row">
            <div class="form-group col-md-4 radio-booton">
                <label class="no-padding" for="gerar_log">Gerar Logs</label>
                <div class="radio">
                    <?= $this->Form->radio('gerar_log', ['1' => 'Sim', '0' => 'Não']); ?>
                </div>
            </div>
        </div>
    </div>
    <?= $this->element('admin/titulo-box', ['titulo' => 'Valores e Indíces', 'collapse' => false]) ?>
    <div class="box-body">
        <div class="row">
            <div class="form-group col-md-4">
                <?=
                $this->Form->control('indice_meta_fundamento', [
                    'label' => 'Indíce Meta Fundamento',
                    'value' => valueToValor($entidade->indice_meta_fundamento),
                    'class' => 'nf2',
                    'type' => 'text'
                ]);
                ?>
            </div>
            <div class="form-group col-md-4">
                <?=
                $this->Form->control('salario_base_vendedor_menos_3_anos', [
                    'label' => 'Salário Base Vendedor MENOS de 3 ANOS',
                    'value' => valueToValor($entidade->salario_base_vendedor_menos_3_anos),
                    'class' => 'nf2',
                    'type' => 'text'
                ]);
                ?>
            </div>
            <div class="form-group col-md-4">
                <?=
                $this->Form->control('salario_base_vendedor_mais_3_anos', [
                    'label' => 'Salário Base Vendedor MAIS de 3 ANOS',
                    'value' => valueToValor($entidade->salario_base_vendedor_mais_3_anos),
                    'class' => 'nf2',
                    'type' => 'text'
                ]);
                ?>
            </div>
            <div class="form-group col-md-4">
                <?=
                $this->Form->control('comissao_vendedor_menos_1_ano', [
                    'label' => 'Comissão Vendedor MENOS de 1 ANO',
                    'value' => valueToValor($entidade->comissao_vendedor_menos_1_ano),
                    'class' => 'nf2',
                    'type' => 'text'
                ]);
                ?>
            </div>
            <div class="form-group col-md-4">
                <?=
                $this->Form->control('comissao_vendedor_mais_1_ano', [
                    'label' => 'Comissão Vendedor MAIS de 1 ANO',
                    'value' => valueToValor($entidade->comissao_vendedor_mais_1_ano),
                    'class' => 'nf2',
                    'type' => 'text'
                ]);
                ?>
            </div>
            <div class="form-group col-md-4">
                <?=
                $this->Form->control('meta_material_pesado_vendedor', [
                    'label' => 'Meta de Material Pesado Vendedor',
                    'value' => valueToValor($entidade->meta_material_pesado_vendedor),
                    'class' => 'nf2',
                    'type' => 'text'
                ]);
                ?>
            </div>
            <div class="form-group col-md-4">
                <?=
                $this->Form->control('meta_financeira_material_grande', [
                    'label' => 'Meta Financeira Material Grande',
                    'value' => soNumeros($entidade->meta_financeira_material_grande),
                    'class' => 'integer',
                    'type' => 'text'
                ]);
                ?>
            </div>
            <div class="form-group col-md-4">
                <?=
                $this->Form->control('meta_financeira_material_pequeno', [
                    'label' => 'Meta Financeira Material Pequeno',
                    'value' => soNumeros($entidade->meta_financeira_material_pequeno),
                    'class' => 'integer',
                    'type' => 'text'
                ]);
                ?>
            </div>
        </div>
    </div>
    <div class="box-footer">
        <?= $this->Form->button('<i class="fa fa-save"></i> Salvar', ['class' => 'btn btn-primary', 'escape' => false]); ?>
    </div>
</div>
<?= $this->Form->end() ?>
