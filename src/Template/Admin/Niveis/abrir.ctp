<?= $this->Form->create($entidade, ['action' => 'salvar']) ?>
<div class="box">
    <?= $this->element('admin/titulo-box', ['titulo' => 'Cadastro']) ?>
    <div class="box-body">
        <fieldset>
            <?= $this->Form->hidden('id') ?>
            <?= $this->Form->hidden('ativo',['value' => 1]); ?>
            <div class="form-group col-md-6">
                <?= $this->Form->control('nome'); ?>
            </div>
        </fieldset>
    </div>
    <?php if (!empty($entidade->id)) { ?>
        <?= $this->element('admin/titulo-box', ['titulo' => 'Permissões', 'collapse' => false]) ?>
        <div class="box-body">
            <div class="panel-group col-md-12" id="accordion">   
                <?= $this->Form->hidden('prefix', ['value' => $this->request['prefix']]); ?>
                <?php foreach ($listaPermissoes as $key => $controller) { ?>
                    <div class="panel panel-default seleciona-perfis" id="<?= strtolower(key($controller)) ?>">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse-<?= strtolower(key($controller)) ?>">
                                    <strong>
                                        <?= translateController(key($controller)) ?>                                                
                                    </strong>
                                </a>
                            </h4>
                        </div>
                        <div id="collapse-<?= strtolower(key($controller)) ?>" class="panel-collapse collapse">
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <?= $this->Form->hidden('controller[]', ['value' => key($controller)]); ?>
                                    <?= $this->Form->checkbox('selecionar-todos', ['hiddenField' => false, 'id' => strtolower(key($controller)), 'data-controller' => 'collapse-' . strtolower(key($controller))]); ?>
                                    <label for="<?= strtolower(key($controller)) ?>">Selecionar Todos</label>
                                </div>
                                <div class="col-md-12">
                                    <?php
                                    foreach ($controller[key($controller)] as $k => $action) {
                                        $idAction = key($controller) . '-' . $action . '-' . $k;

                                        $actionStr = key($controller) . '.' . $action;
                                        $checked = false;
                                        if (array_search($actionStr, array_column($permissoes, 'action_str')) !== false) {
                                            $checked = true;
                                        }
                                        ?>
                                        <div class="col-md-3">
                                            <?=
                                            $this->Form->checkbox('action[' . key($controller) . '][]', array('div' => false,
                                                'hiddenField' => false,
                                                'label' => false,
                                                'id' => $idAction,
                                                'value' => $action,
                                                'checked' => $checked
                                            ));
                                            ?>
                                            <label for="<?= $idAction ?>">
                                                <?= translateActions($action) ?>
                                            </label>
                                        </div>                                                
                                    <?php } ?> 
                                </div>   
                            </div>
                        </div>
                    </div>
                <?php } ?>  
            </div>
        </div>
    <?php } ?>  
    <div class="box-footer">                    
        <?= $this->element('admin/botoes-abrir', ['id' => @$entidade->id]) ?>
    </div>
</div>
<?= $this->Form->end() ?>
