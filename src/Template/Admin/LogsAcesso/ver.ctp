<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\LogsAcesso $logsAcesso
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Logs Acesso'), ['action' => 'edit', $logsAcesso->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Logs Acesso'), ['action' => 'delete', $logsAcesso->id], ['confirm' => __('Are you sure you want to delete # {0}?', $logsAcesso->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Logs Acesso'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Logs Acesso'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="logsAcesso view large-9 medium-8 columns content">
    <h3><?= h($logsAcesso->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Ip') ?></th>
            <td><?= h($logsAcesso->ip) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Usuario') ?></th>
            <td><?= h($logsAcesso->usuario) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($logsAcesso->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Data Hora') ?></th>
            <td><?= h($logsAcesso->data_hora) ?></td>
        </tr>
    </table>
</div>
