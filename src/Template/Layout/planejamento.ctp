<!DOCTYPE html>
<html>
    <head>
        <title>Planejamento Matinal - Sao Rafael</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

        <style>
            body { line-height: 1; }
            body { color: #535353; background: #fff; font-family: 'OpenSansRegular', Arial, Geneva, sans-serif; font-size: 12px; margin: auto; text-align: center; }
            /* tables still need 'cellspacing="0"' in the markup */
            table { border-collapse: collapse; border-spacing: 0; }
            #ant			{ float: left; margin: 15px 15px 40px 0; font-size: 12px; color: #fff; background: #090; text-decoration: none; padding: 10px; }
            #ant:hover		{ background: #096; }
            .table 			{ clear: both; float: left; margin: 10px 0 0 10px; width: 900px; }
            .table td		{ border: solid 1px gray; padding: 5px; text-align: left; }

            /*CSS para impressão*/

            @media print {
                @page{
                    size: landscape;
                }

                #top 			{ display: none; }
                #col_left 		{ display: none; }
                #bts			{ display: none; }
                #print			{ display: none; }
                #view h2		{ float: left; clear: both; text-align: left; font-family: 'OpenSansBold'; font-size: 14px; }
                #view p			{ clear: both; float: left; text-align: left; color: #909090; margin: -10px 0 0px 0; font-size: 12px; }
                .table 			{ clear: both; float: left; margin: 0px 0 0 10px; width: 900px; font-size: 10px; }
                .table td		{ border: solid 1px #000; padding: 1px; text-align: left; font-size: 10px; font-family: arial; }
            }
            #logo {
                width: 310px;
                height: 50px;
                display: block;
            }
            #logo h1 {
                font-size: 23px;
                margin-top: 25px;
            }
            #logo img {
                margin: 5px 0 0 10px;
            }
            .td-vertical {
                min-width: 15px !important;
                height: 90px;
                text-align: left;
                border: solid 1px gray;
                position: relative;
                font-size: 11px;
            }
            .vertical-text {
                margin-left: -68px;
                margin-top: -15px;
                position: absolute;
                width: 148px;
                transform: rotate(-90deg);
                -webkit-transform: rotate(-90deg);
                -moz-transform: rotate(-90deg);
                -o-transform: rotate(-90deg);
                -ms-transform: rotate(-90deg);
                letter-spacing: 0.3px;
            }
            .vertical-text2 {
                margin-top: -35px !important;
            }
            .text-center {
                text-align: center !important;
            }
            .text-right {
                text-align: right !important;
            }
            .green {
                background: #060;
                color: #fff !important;
            }
            .cinder {
                background: #d8e4bc;
                color: #000 !important;
            }
            .border-left {
                border-left: 2.5px solid #000 !important;
            }
            .border-top {
                border-top: 2.5px solid #000 !important;
            }
            .border-right:  {
                border-right: 2.5px solid #000 !important;
            }
            .border-bottom {
                border-bottom: 2.5px solid #000 !important;
            }
            .no-bord {
                border: transparent !important;
            }
            .bold {
                font-weight: 900 !important;
            }
            thead {
                font-weight: 900 !important;
            }
            .canal {
                color: #fff !important;
                background-color: #000 !important;
            }
            .dados-vendedor {
                max-width: 300px;
            }
            .dados-vendedor {
                max-width: 300px;
            }
            .foco-dia td {
                line-height: 25px;
            }
            body {
                font-weight: bold !important;
                width: 2000px;
            }
            .primeira-coluna {
                width: 200px;
            }
            .segunda-coluna {
                width: 200px;
                float: left;
                margin: -125px 5px;
            }
            .terceira-coluna {
                 float: left;
                 margin: -125px 10px 0px 110px;
             }
            .terceira-coluna table{
                width: 180px;
            }
            .terceira-coluna table thead td{
                font-size: 12px !important;
            }
            .quarta-coluna {
                float: left;
                margin: -125px 0px 0px -5px;
            }
            .table-refrigeracao{
                min-width: 405px;
            }

            .quinta-coluna {
                float: left;
                margin: -125px 0px 0px -5px;
            }

            .sexta-coluna {
                float: left;
                margin: -125px 0px 0px -5px;
            }
            .sexta-coluna td.valores {
                padding: 2px;
                line-height: 15px;
            }
            .bold {
                font-weight: 900 !important;
            }
            .valores {
                background: #d8e4bc;
                color: #0c0c0c;
            }
            .parte-superior {
                height: 310px !important;
            }
            .fundamentos-superior .td-vertical {
                min-width: 10px !important;
                height: 35px;
                font-size: 10px;
            }
            .fundamentos-superior .vertical-text {
                margin-left: -65px;
                margin-top: -60px;
            }
        </style>

        <?= $this->fetch('meta') ?>
        <?= $this->fetch('css') ?>
        <?= $this->fetch('script') ?>

    </head>
    <body>

        <?= $this->fetch('content') ?>

    </body>

</html>
