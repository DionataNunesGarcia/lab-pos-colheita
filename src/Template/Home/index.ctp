<?= $this->element('casamento/convite') ?>

<?= $this->element('casamento/slides') ?>

<?= $this->element('casamento/casal') ?>

<?php if (count($this->sobre->Testimonial()->toArray()) > 0) { ?>
    <div class="important-people">
        <div class="container">
            <div class="important-people-head text-center">
                <h3>Pessoas Mais Importantes</h3>
            </div>
            <div class="important-people-grids">
                <?= $this->element('casamento/testemunhos') ?>
            </div>
        </div>
    </div>
<?php } ?>
<!-- content-section-starts -->
<?php if (!empty($this->sobre->galeria())) { ?>
    <?= $this->element('casamento/galeria') ?>
<?php } ?>
<!-- content-section-ends -->