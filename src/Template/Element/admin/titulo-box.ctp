<div class="box-header with-border">
    <h3 class="box-title">
        <?php
        if (isset($titulo)) {
            echo $titulo;
        }else{
             echo $this->fetch('title');
        }
        ?>
    </h3>
    <?php if (!isset($collapse) || $collapse) { ?>
        <div class="box-tools pull-right">        
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fa fa-minus"></i>
            </button>
        </div>
    <?php } ?>
</div>