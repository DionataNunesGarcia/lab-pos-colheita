<?php

$url = $this->Url->build([
    "controller" => $controller,
    "action" => "autocomplete",
    "_full" => true,
//    "?" => ["foo" => "bar"],
//    "#" => "first",
        ]);

if (!isset($label)) {
    $label = $name;
}

if (!isset($placeholder)) {
    $placeholder = null;
}

if (!isset($multiple)) {
    $multiple = true;
}

if (!isset($required)) {
    $required = false;
}

if (empty($value)) {
    $values = 0;
} else if (is_array($value)) {
    $values = implode(',',$value);
} else {
    $values = $value;
}

if ($label) {
    echo $this->Form->label(ucfirst($label));
}

//verifica se multiplo, default true
if (!isset($disabled)) {
    $disabled = '';
}

if (!isset($clear) || !$required) {
    $clear = false;
}

echo $this->Form->select($name, null, [
    'multiple' => $multiple,
    'class' => 'select2-ajax',
    'hiddenField' => false,
    'label' => $label,
    'data-ajax--url' => $url,
    'required' => $required,
    'data-placeholder' => $placeholder,
    'data-allow-clear' => true,
    'data-val' => $values,
    $disabled
]);
