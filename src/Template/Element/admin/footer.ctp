<?php use Cake\Core\Configure; ?>
<footer class="main-footer">
    <div class="pull-right hidden-xs">
        Desenvolvido por:
        <b>Diônata Nunes Garcia</b>
    </div>
    <strong>Copyright &copy; 
    <?= date('Y') ?> 
        <a href="<?= Configure::read('Cliente.link') ?>">
        <?= Configure::read('Cliente.nome') ?>
        </a>.
    </strong> 
    Todos os direitos reservados
</footer>