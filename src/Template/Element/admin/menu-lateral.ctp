<style>
    .treeview-menu li.disabled:hover, .treeview-menu li.disabled a:hover {
        cursor: no-drop;
    }
</style>
<aside class="main-sidebar">
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <?php
                $imagem = 'user-default.png';
                if (!empty($userSession['imagem']) && file_exists(WWW_ROOT . $userSession['imagem'])) {
                    $imagem = '../' . $userSession['imagem'];
                }
                ?>
                <?= $this->Html->image($imagem, ['class' => 'img-circle']); ?>
            </div>
            <div class="pull-left info">
                <p>
                    <?= ucfirst($userSession['usuario']); ?>
                </p>
            </div>
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <!-- Cadastros -->
            <li class=" treeview">
                <a href="#">
                    <i class="fa fa-drivers-license"></i>
                    <span>
                        <?= __('Cadastros') ?>
                    </span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Sobre', 'action' => 'abrir'], true); ?>" title="Sobre" data-placement="right">
                            <i class="fa fa-circle-o"></i>
                            <?= __('Sobre') ?>
                        </a>
                    </li>
                </ul>
            </li>

            <!--Administração-->
            <li class=" treeview">
                <a href="#">
                    <i class="fa fa-gears"></i> <span>
                        <?= __('Administração') ?>
                    </span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="<?= $this->Url->build(['controller' => 'Usuarios', 'action' => 'pesquisar'], true); ?>" data-placement="right">
                            <i class="fa fa-users"></i>
                            <?= __('Usuários') ?>
                        </a>
                    </li>
                    <li>
                        <a href="<?= $this->Url->build(['controller' => 'niveis', 'action' => 'pesquisar'], true); ?>" data-placement="right">
                            <i class="fa fa-bar-chart-o"></i>
                            <?= __('Níveis Permissões') ?>
                        </a>
                    </li>
                    <li>
                        <a href="<?= $this->Url->build(['controller' => 'ParametrosSistema', 'action' => 'abrir'], true); ?>" data-placement="right">
                            <i class="fa fa-gear"></i>
                            <?= __('Parâmetros do Sistema') ?>
                        </a>
                    </li>
                    <li>
                        <a href="<?= $this->Url->build(['controller' => 'LogsAlteracoes', 'action' => 'pesquisar'], true); ?>" data-placement="right">
                            <i class="fa fa-exclamation-triangle"></i>
                            <?= __('Logs Alterações') ?>
                        </a>
                    </li>
                    <li>
                        <a href="<?= $this->Url->build(['controller' => 'LogsAcesso', 'action' => 'pesquisar'], true); ?>" data-placement="right">
                            <i class="fa fa-check"></i>
                            <?= __('Logs Acessos') ?>
                        </a>
                    </li>
                    <li>
                        <a href="<?= $this->Url->build(['controller' => 'Mensagens', 'action' => 'escreverMensagem'], true); ?>" data-placement="right">
                            <i class="fa fa-envelope-o"></i>
                            <?= __('Enviar Mensagens') ?>
                        </a>
                    </li>
                    <li>
                        <a href="<?= $this->Url->build(['controller' => 'usuarios', 'action' => 'logout'], true); ?>" data-auth="false" data-placement="right">
                            <i class="fa fa-sign-out"></i>
                            <?= __('Sair') ?>
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
