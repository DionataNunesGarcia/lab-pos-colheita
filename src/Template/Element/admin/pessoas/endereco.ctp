<?= $this->Form->hidden('endereco_id', ['value' => @$entidade->id]); ?>
<div class="box-body">
    <div class="form-group col-md-3">
        <?=
        $this->Form->control('cep', [
            'label' => 'CEP',
            'class' => 'cep',
            'value' => cep(@$entidade->cep)
        ]);
        ?>
    </div>
    <div class="form-group col-md-4">
        <?=
        $this->Form->control('rua', [
            'label' => 'Rua',
            'value' => @$entidade->rua
        ]);
        ?>
    </div>
    <div class="form-group col-md-2">
        <?=
        $this->Form->control('numero', [
            'label' => 'Nº',
            'value' => @$entidade->numero
        ]);
        ?>
    </div>
    <div class="form-group col-md-3">
        <?=
        $this->Form->control('complemento', [
            'label' => 'Complemento',
            'value' => @$entidade->complemento
        ]);
        ?>
    </div>
    <div class="form-group col-md-3">
        <?=
        $this->Form->control('bairro', [
            'label' => 'Bairro',
            'value' => @$entidade->bairro
        ]);
        ?>
    </div>
    <div class="form-group col-md-4">
        <?=
        $this->Form->control('cidade', [
            'label' => 'Cidade',
            'value' => @$entidade->cidade
        ]);
        ?>
    </div>
    <div class="form-group col-md-2">
        <?=
        $this->Form->control('uf', [
            'label' => 'UF',
            'maxlength' => 2,
            'value' => @$entidade->uf
        ]);
        ?>
    </div>
</div>
