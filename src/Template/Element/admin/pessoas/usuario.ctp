<?php
$placeholder = !empty($entidade->id) ? 'Só preencher se for alterar' : '';
$required = empty($entidade->id) ? true : false;
?>
<?= $this->Form->hidden('usuario_id', ['value' => $entidade->id]); ?>
<div class="box-body">
    <div class="form-group col-md-4">
        <?= $this->Form->control('usuario', ['label' => 'Usuário', 'value' => $entidade->usuario, 'required' => true]); ?>
    </div>
    <div class="form-group col-md-4">
        <?=
        $this->Form->control('senha', [
            'label' => 'Senha',
            'type' => 'password',
            'value' => '',
            'placeholder' => $placeholder,
            'required' => $required,
        ]);
        ?>
    </div>
    <div class="form-group col-md-4">
        <?=
        $this->Form->control('confirmar_senha', [
            'label' => 'Confirmar Senha',
            'type' => 'password',
            'value' => '',
            'placeholder' => $placeholder,
            'required' => $required,
        ]);
        ?>
    </div>
    <div class="form-group col-md-4">
        <?=
        $this->element('admin/select2', [
            'controller' => 'niveis',
            'name' => 'niveis_id',
            'label' => 'Níveis',
            'multiple' => false,
            'required' => true,
            'value' => $entidade->niveis_id,
            'placeholder' => 'Pesquise o Nível de Permissão'
        ])
        ?>
    </div>
    <div class="form-group col-md-4 radio-booton">
        <label class=" no-padding" for="ativo">Ativo</label>
        <div class="radio">
            <?=
            $this->Form->radio('ativo', ['1' => 'Sim', '0' => 'Não'], [
                'label' => 'Ativo',
                'value' => !empty($entidade->ativo) ? $entidade->ativo : 1,
                'required' => true,
            ]);
            ?>
        </div>
    </div>
    <div class="form-group col-md-4">
        <?php
        if (empty($entidade->imagem)) {
            echo $this->Form->control('imagem_upload', [
                'type' => 'file',
                'class' => 'upload_crop',
                'multiple' => false,
                'accept' => 'image/*',
                'label' => 'Imagem'
            ]);
        } else {
            ?>
            <div class="col-md-4 no-padding">
                <strong>Imagem</strong>
                <?= $this->Html->image('../' . $entidade->imagem, ['class' => 'img-responsive img-usuario img-thumbnail']); ?>
            </div>
            <div class="col-md-2 text-rigth">
                <?=
                $this->Html->link("<i class='fa fa-trash'></i> Excluir", ['controller' => 'Usuarios', 'action' => 'excluir_imagem', $entidade->id, '_full' => true], [
                    "alt" => "Imagem",
                    'escape' => false,
                    'confirm' => __('Tem certeza de que deseja excluir o arquivo?'),
                    'class' => 'btn btn-xs btn-danger tex'
                ]);
                ?>
            </div>
        <?php } ?>
        <div id="uploaded_image"></div>
    </div>
</div>
<?= $this->element('admin/imagem-crop-modal') ?>
