<?= $this->Form->hidden('pessoa_fisica_id', ['value' => $entidade->id]); ?>
<div class="row">
    <div class="form-group col-md-4">
        <?=
        $this->Form->control('nome', [
            'required' => true,
            'label' => 'Nome',
            'value' => $entidade->nome
        ]);
        ?>
    </div>
    <div class="form-group col-md-4">
        <?=
        $this->Form->control('data_nascimento', [
            'label' => 'Data Nascimento',
            'class' => 'datepicker',
            'value' => dateToData($entidade->data_nascimento)
        ]);
        ?>
    </div>
    <div class="form-group col-md-4">
        <?=
        $this->Form->control('email', [
            'label' => 'E-mail',
            'value' => $entidade->email
        ]);
        ?>
    </div>
    <div class="form-group col-md-4">
        <?=
        $this->Form->control('cpf', [
            'label' => 'CPF',
            'class' => 'cpf',
            'value' => cpf($entidade->cpf)
        ]);
        ?>
    </div>
    <div class="form-group col-md-4">
        <?=
        $this->Form->control('rg', [
            'label' => 'RG',
            'value' => $entidade->rg
        ]);
        ?>
    </div>
    <div class="form-group col-md-4 radio-booton">
        <label class=" no-padding" for="sexo">Sexo</label>
        <div class="radio">
            <?=
            $this->Form->radio('sexo', sexo(), [
                'required' => true,
                'label' => 'Sexo',
                'value' => $entidade->sexo
            ]);
            ?>
        </div>
    </div>
</div>
