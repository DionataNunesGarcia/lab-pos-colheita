<div class="paginator text-center no-print">
    <ul class="pagination">
        <?php
        $controller = isset($controller) ? $controller : $this->request->getParam('controller');
        $action = isset($action) ? $action : $this->request->getParam('action');
        $search = isset($search) ? $search : $this->request->getParam('?');
        $pass = '';

        if (!empty($this->request->getParam('pass'))){
            foreach ($this->request->getParam('pass') as $item) {
                $pass .= $pass !== '' ? '/' : '';
                $pass .= $item;
            }
        }

        if (isset($search['page'])){
            unset($search['page']);
        }

        $this->Paginator->options([
            'url' => [
                'controller' => $controller,
                'action' => $action,
                $pass,
                '?' => $search,
                '_full' => true,
            ]
        ]);
        ?>
        <?= $this->Paginator->first('<i class="fa fa-angle-double-left" aria-hidden="true"></i> ', ['escape' => false]) ?>
        <?= $this->Paginator->prev('<i class="fa fa-angle-left" aria-hidden="true"></i>  ', ['escape' => false]) ?>
        <?= $this->Paginator->numbers() ?>
        <?= $this->Paginator->next('<i class="fa fa-angle-right" aria-hidden="true"></i>  ', ['escape' => false]) ?>
        <?= $this->Paginator->last('<i class="fa fa-angle-double-right" aria-hidden="true"></i> ', ['escape' => false]) ?>
    </ul>
    <p><?= $this->Paginator->counter(['format' => __('Página {{page}} de {{pages}}, mostrando {{current}} registro(s) do total de {{count}}.')]) ?></p>
</div>
