<?php
echo $this->Html->link(__('<i class="fa fa-folder-open-o"></i>'), ['action' => 'abrir',$id, '_full' => true], [
    'class' => 'btn btn-primary btn-xs',
    'escape' => false
]);

if (!isset($deletavel)) {
    $deletavel = true;
}
if ($deletavel) {
    echo $this->Html->link(__('<i class="fa fa-trash-o"></i>'), ['action' => 'excluir',$id , '_full' => true], [
        'class' => 'btn btn-danger btn-xs',
        'escape' => false,
        'confirm' => __('Você tem certeza que deseja excluir esse registro?', $id),
    ]);
    ?> 
<?php } ?>