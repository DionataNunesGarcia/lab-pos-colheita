<?php

/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     3.0.0
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */

namespace App\Shell;

use Cake\Console\ConsoleOptionParser;
use Cake\Console\Shell;
use Cake\Log\Log;
use Psy\Shell as PsyShell;
use Cake\I18n\Time;
use Cake\Console\TaskRegistry;

/**
 * Simple console wrapper around Psy\Shell.
 */
class ImportacoesDiariasShell extends Shell {

    public $tasks = [
        'DiariasClientes',
        'DiariasIav',
        'DiariasPedidos',
        'DiariasPesquisas',
        'DiariasInadimplencias',
        'DiariasCev',
    ];

    /**
     * Start the shell and interactive console.
     *
     * @return int|null
     */
    public function main() {

        $this->out('Olá, para importar tabelas execute um dos comandos a seguir');
        $this->hr();
        $this->out('clientes arquivoId');
        $this->out('cev arquivoId');
        $this->out('iav arquivoId');
        $this->out('pedidos arquivoId');
        $this->out('pesquisas arquivoId');
        $this->out('inadimplencias arquivoId');
        $this->out('atualizarRankingVendedores');

    }

    public function clientes($arquivoId) {
        $this->DiariasClientes->importar($arquivoId);
    }

    public function pesquisas($arquivoId) {
        $this->DiariasPesquisas->importar($arquivoId);
    }

    public function iav($arquivoId) {
        $this->DiariasIav->importar($arquivoId);
    }

    public function cev($arquivoId) {
        $this->DiariasCev->importar($arquivoId);
    }

    public function pedidos($arquivoId) {
        $this->DiariasPedidos->importar($arquivoId);
    }

    public function atualizarRankingVendedores() {
        $this->DiariasPedidos->atualizarRankingVendedores();
    }

    public function atualizarValoresVendedores() {
        $this->DiariasPedidos->atualizarValoresVendedores();
    }

    public function inadimplencias($arquivoId) {
        $this->DiariasInadimplencias->importar($arquivoId);
    }
}
