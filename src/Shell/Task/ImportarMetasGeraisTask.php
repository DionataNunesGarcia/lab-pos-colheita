<?php

namespace App\Shell\Task;

use Cake\Console\Shell;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;

/**
 * Simple console wrapper around Psy\Shell.
 */
class ImportarMetasGeraisTask extends Shell {

    /**
     * Start the shell and interactive console.
     *
     * @return int|null
     */
    public function main() {
        $this->hr();
        $this->out('Importação Dias Uteis');
        $inicio = Time::now();
        $this->out('Comecando as: ' . $inicio);
        $this->hr();
        $this->out();
        $this->out();

        $this->deletarTodos();
        $this->migrar();

    }

    public function migrar() {
        $table = TableRegistry::get('MetasGerais');
        $produtosTable = TableRegistry::get('Produtos');
        $entidades = $this->listarOld();

        $count = 0;
        foreach ($entidades as $item) {
            $count++;
            if (($count % 100) == 0) {
                $this->out('Meta total '. $count . ' - ID ' . $item['id'] . ', mes ano meta ' . $item['ano'] . '-' . $item['mes'] . ' meta finan '. $item['meta_finan_brk']);
            }

            $tipoPessoaId = $table->buscarPessoaCargo($item['pessoa']);
            if(empty($tipoPessoaId)){
                continue;
            }

            $entidade = $table->novo();

            if ($item['ano'] == '' && $item['mes'] == '') {
                $item['ano'] = date('Y');
                $item['mes'] = date('m');
            }

            $entidade->meta_rob_hei = $item['meta_finan_brk'];
            $entidade->meta_rob_3 = $item['meta_finan_terc'];
            $entidade->meta_dev = $item['meta_dev'];
            $entidade->meta_finan_troca = $item['meta_finan_troca'];
            $entidade->meta_cerveja = $item['meta_cob_cerveja'];
            $entidade->meta_total = $item['meta_cob_tt'];
            $entidade->meta_300_600_1000 = $item['meta_cob_m1'];
            $entidade->meta_retornavel = $item['meta_cob_ret'];
            $entidade->meta_nao_alcoolico = $item['meta_cob_nalc'];
            $entidade->meta_eisenbahn_600_r = $item['meta_cob_eisen600'];
            $entidade->meta_premium = $item['meta_cob_esp'];
            $entidade->meta_devassa = $item['meta_cob_devassa'];
            $entidade->meta_600_r = $item['meta_cob_seiscentos'];
            $entidade->meta_devassa_600_r = $item['meta_cob_devassa_seiscentos'];
            $entidade->fds = $item['demp'];
            $entidade->mes_ano = $item['ano'] . '-' . $item['mes'] . '-01';

            $save = $table->save($entidade);
            if (!$save) {
                debug($entidade);
                die;
            }

            $metasGeraisPessoas = TableRegistry::get('MetasGeraisPessoas');
            $metaPessoa = $metasGeraisPessoas->novo();
            $metaPessoa->meta_geral_id = $entidade->id;

            switch ($tipoPessoaId['tipo']){
                case 'v':
                    $metaPessoa->vendedores_id = $tipoPessoaId['id'];
                    break;
                case 'g':
                    $metaPessoa->gerentes_id = $tipoPessoaId['id'];
                    break;
                case 's':
                    $metaPessoa->supervisores_id = $tipoPessoaId['id'];
                    break;
            }

            $saveMetasPessoas = $metasGeraisPessoas->save($metaPessoa);

            if (!$saveMetasPessoas) {
                debug($metasGeraisPessoas);
                die;
            }
        }
        $this->hr('TOTAL IMPORTADO ' . $count);
        $this->hr();
    }

    public function listarOld() {

        $conn = ConnectionManager::get('bd_old');
        return $conn->execute("select * from sao_rafael.metas_gerais where ano >= 2019")->fetchAll('assoc');
    }

    public function deletarTodos() {

        $this->out('Deletando os dados');
        TableRegistry::get('MetasGeraisPessoas')->deleteAll(['id !=' => 0]);
        TableRegistry::get('MetasGerais')->deleteAll(['id !=' => 0]);
        $this->hr();

        $conn = ConnectionManager::get('default');
        $conn->execute("ALTER TABLE metas_gerais_pessoas AUTO_INCREMENT = 1; ");
        $conn->execute("ALTER TABLE metas_gerais AUTO_INCREMENT = 1; ");
    }
}
