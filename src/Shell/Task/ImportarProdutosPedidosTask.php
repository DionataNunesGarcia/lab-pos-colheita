<?php

namespace App\Shell\Task;

use Cake\Console\Shell;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;

/**
 * Simple console wrapper around Psy\Shell.
 */
class ImportarProdutosPedidosTask extends Shell {

    /**
     * Start the shell and interactive console.
     *
     * @return int|null
     */
    public function main() {
        $this->hr();
        $this->out('Importação Tabelas Produtos e Pedidos');
        $inicio = Time::now();
        $this->out('Comecando as: ' . $inicio);
        $this->hr();
        $this->out();
        $this->out();

        $this->migrarProdutos();

    }

    public function migrarProdutos() {
        $tabela = 'Produtos';
        $this->out("Deletando os dados " . $tabela);

        $table = TableRegistry::get($tabela);

        $table->deleteAll(['id !=' => 0]);
        $conn = ConnectionManager::get('default');

        $conn->execute("ALTER TABLE $tabela AUTO_INCREMENT = 1; ");

        $this->hr();

        $dados = $this->listarTabela('produtos');

        $count = 0;

        foreach ($dados as $item) {
            $count++;
            if (($count % 100) == 0)  {
                $this->out("total $count - $tabela " . $item['nome']);
            }
            try{

                $familia = TableRegistry::get('Familias')
                    ->find()
                    ->where([
                        'id' => $item['id_familia']
                    ])
                    ->first();

                if (empty($familia)) {
                    $this->warn("familia nao encontrada com o id " . $item['id_familia']);
                    continue;
                }
                $marca = TableRegistry::get('Marcas')
                    ->find()
                    ->where([
                        'id' => $item['id_marca']
                    ])
                    ->first();

                if (empty($marca)) {
                    $this->warn("marca nao encontrada com o id " . $item['marca_id']);
                    continue;
                }
                $fundamento = TableRegistry::get('Fundamentos')
                    ->find()
                    ->where([
                        'id' => $item['id_fundamento']
                    ])
                    ->first();

                $fundamentoId = '';
                if (!empty($fundamento)) {
                    $fundamentoId = $fundamento->id;
                }

                $entidade = $table->newEntity();
                $entidade->codigo = $item['codigo'];
                $entidade->nome = $item['nome'];
                $entidade->frete = $item['frete'];
                $entidade->carreto = $item['carreto'];
                $entidade->fob = $item['fob'];
                $entidade->cmv = $item['cmv'];
                $entidade->estoque = $item['estoque'] == 'S';
                $entidade->ps_taxa = $item['ps_taxa'];
                $entidade->ps_valor = $item['ps_valor'];
                $entidade->preco_liquido = $item['preco_liquido'];
                $entidade->ps_compra_taxa = $item['ps_compra_taxa'];
                $entidade->ps_compra_valor = $item['ps_compra_valor'];
                $entidade->marca_id = $marca->id;
                $entidade->familia_id = $familia->id;
                $entidade->fundamento_id = $fundamentoId;

                if ($entidade->nome == 'CERV EISEN PILS 0,60LGFA RT 24UN CX AMAR') {

                    continue;
                }
                $save = $table->save($entidade);

                if (!$save) {
                    debug($item);
                    debug($entidade);
                    throw new \Exception(__("marca nao encontrada com o id " . $item['marca_id']));
                }


            }catch (\Exception $ex) {

                debug($ex);
                die;
            }
        }

        $this->out("total de $count ");
    }

    public function listarTabela($tabela) {
        $conn = ConnectionManager::get('bd_old');
        return $conn->execute("select * from sao_rafael.$tabela order by 1")->fetchAll('assoc');
    }

    public function idContrato($numero) {
        return TableRegistry::get('Clientes')->buscarPorNumero($numero);
    }

    public function idVendedores($codigo) {
        $vendendorId = TableRegistry::get('Vendedores')->findByCodigo($codigo, false);
        if (!empty($vendendorId)) {
            return $vendendorId;
        }
        $supervisorId = TableRegistry::get('Supervisores')->findByCodigo($codigo, false);
        if (!empty($supervisorId)) {
            return $supervisorId;
        }
        $gerenteId = TableRegistry::get('Gerentes')->findByCodigo($codigo, false);
        if (!empty($gerenteId)) {
            return $gerenteId;
        }
    }

    public function idCliente($numero) {
        return TableRegistry::get('Clientes')->buscarPorNumero($numero);
    }

}
