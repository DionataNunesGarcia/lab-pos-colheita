<?php

namespace App\Shell\Task;

use Cake\Console\Shell;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;

/**
 * Simple console wrapper around Psy\Shell.
 */
class ImportarPedidosTask extends Shell {

    /**
     * Start the shell and interactive console.
     *
     * @return int|null
     */
    public function main() {
        $this->hr();
        $this->out('Importação Pedidos');
        $inicio = Time::now();
        $this->out('Comecando as: ' . $inicio);
        $this->hr();
        $this->out();
        $this->out();

        $conn = ConnectionManager::get('default');
        $conn->begin();

        $this->deletarTodos();
        $this->migrar();

        $conn->commit();
    }

    public function migrar() {
        $table = TableRegistry::get('Pedidos');
        $clientesTable = TableRegistry::get('Clientes');
        $produtosTable = TableRegistry::get('Produtos');

        $entidades = $this->listarOld();

        $count = 0;
        foreach ($entidades as $item) {
            $count++;
            if(($count % 100) == 0) {
                $this->out('pedido ' . $item['pedido']);
            }

            $clienteId = $clientesTable->buscarPorNumero($item['cliente'], 'codigo');
            if(empty($clienteId)){
                $this->warn('cliente não encontrado');
                continue;
            }

            $produtoId = $produtosTable->buscarPorNumero($item['produto']);
            if(empty($produtoId)){
                $this->warn('produto não encontrado');
                continue;
            }

            $entidade = $table->novo();

            $entidade->pedido = $item['pedido'];
            $entidade->seq = $item['seq'];
            $entidade->cliente_id = $clienteId;
            $entidade->pagamento_id = $item['tipo_pagamento'];
            $entidade->produtos_id = $produtoId;
            $entidade->placa = $item['placa'];
            $entidade->cod_cancelamento = $item['codigo_cancelamento'];
            $entidade->cod_devolucao = $item['codigo_devolucao'];
            $entidade->data_pedido = $item['data_pedido'];
            $entidade->quantidade = $item['qtde_ved'];
            $entidade->preco = $item['prec_tab'];
            $entidade->preco_tabela = $item['prec_tab'];
            $entidade->devolucao = $item['devolucao'];
            $entidade->nfe = $item['nfe'];
            $entidade->ocorrencia = $item['ocorrencia'];
            $entidade->valor = $item['valor'];
            $entidade->tabela = $item['tabela'];

            $save = $table->save($entidade);
            if (!$save) {
                debug($entidade);
                die;
            }

            //Prazo 1
            if(intval($item['prazo1']) !== 0){
                $this->salvarPrazo($item['prazo1'], $entidade->id, 1);
            }

            //Prazo 2
            if(intval($item['prazo2']) !== 0){
                $this->salvarPrazo($item['prazo2'], $entidade->id, 2);
            }

            //Prazo 3
            if(intval($item['prazo3']) !== 0){
                $this->salvarPrazo($item['prazo3'], $entidade->id, 3);
            }

            //Prazo 4
            if(intval($item['prazo4']) !== 0){
                $this->salvarPrazo($item['prazo4'], $entidade->id, 4);
            }

            //Prazo 5
            if(intval($item['prazo5']) !== 0){
                $this->salvarPrazo($item['prazo5'], $entidade->id, 5);
            }
        }
        $this->hr();
    }

    public function listarOld() {

        $conn = ConnectionManager::get('bd_old');
        return $conn->execute("select * from sao_rafael.pedidos")->fetchAll('assoc');
    }

    public function deletarTodos() {

        $this->out('Deletando os dados');
        TableRegistry::get('PrazosPedidos')->deleteAll(['id !=' => 0]);
        TableRegistry::get('pedidos')->deleteAll(['id !=' => 0]);
        $this->hr();

        $conn = ConnectionManager::get('default');
        $conn->execute("ALTER TABLE prazos_pedidos AUTO_INCREMENT = 1; ");
        $conn->execute("ALTER TABLE pedidos AUTO_INCREMENT = 1; ");
    }

    public function salvarPrazo($prazo, $pedidoId, $numero) {
        $prazosPedidosTable = TableRegistry::get('PrazosPedidos');
        $entidade = $prazosPedidosTable->novo();
        $entidade->numero = $numero;
        $entidade->valor = $prazo;
        $entidade->pedidos_id = $pedidoId;
        //Save
        $prazosPedidosTable->save($entidade);
    }
}
