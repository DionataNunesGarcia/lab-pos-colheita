<?php

namespace App\Shell\Task;

use Cake\Console\Shell;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;

/**
 * Simple console wrapper around Psy\Shell.
 */
class ImportarSimplesTask extends Shell {

    /**
     * Start the shell and interactive console.
     *
     * @return int|null
     */
    public function main() {
        $this->hr();
        $this->out('Importação Tabelas Simples');
        $inicio = Time::now();
        $this->out('Comecando as: ' . $inicio);
        $this->hr();
        $this->out();
        $this->out();

        $conn = ConnectionManager::get('default');
//        $conn->begin();

        $arraySimples = [
            'canal' => 'Canais',
            'categorias' => 'Categorias',
            'cod_status' => 'codStatus',
            'demp' => 'Demps',
            'fundamentos' => 'Fundamentos',
            'grupos' => 'Grupos',
            'marcas' => 'Marcas',
            'pagamentos' => 'Pagamentos',
        ];

        //deletando exenciais
        $this->deletarTodos();

        $this->migrarMotivosDevolucoes();
        $this->migrarSiglas();

        foreach($arraySimples as $tabelaAntiga => $tabela ){
            $this->migrarTabela($tabela, $tabelaAntiga);
        }

        $this->migrarFamilias();
        $this->migrarCategoriasVarejos();

        $this->migrarContratos();
        $this->migrarInadimplencias();
        $this->migrarMetasStm();
        $this->migrarRanking();
        $this->migrarRankingSupervisores();

        $conn->commit();
    }

    public function migrarTabela($tabela, $tabelaAntiga) {
        $this->out("Deletando os dados " . $tabela);

        $table = TableRegistry::get($tabela);

        $table->deleteAll(['id !=' => 0]);
        $conn = ConnectionManager::get('default');

        if($tabela == 'codStatus'){
            $tabela = 'cod_status';
        }
        $conn->execute("ALTER TABLE $tabela AUTO_INCREMENT = 1; ");

        $this->hr();

        $dados = $this->listarTabela($tabelaAntiga);

        foreach ($dados as $item) {
            $this->out("$tabela " . $item['nome']);

            $entidade = $table->newEntity();
            $entidade->id = $item['id'];
            $entidade->nome = $item['nome'];
            $table->save($entidade);
        }
    }

    public function migrarMotivosDevolucoes() {
        $tabela = 'MotivosDevolucoes';
        $this->out("Deletando os dados" . $tabela);
        $table = TableRegistry::get($tabela);

        $table->deleteAll(['id !=' => 0]);
        $conn = ConnectionManager::get('default');

        $conn->execute("ALTER TABLE motivos_devolucoes AUTO_INCREMENT = 1; ");

        $this->hr();

        $dados = $this->listarTabela('motivos_devolucoes');

        foreach ($dados as $item) {
            $this->out("$tabela " . $item['codigo']);

            if($item['codigo'] == ''){
                continue;
            }
            $entidade = $table->newEntity();
            $entidade->codigo = $item['codigo'];
            $entidade->motivo = $item['motivo'];
            $entidade->penalisa = $item['penalisa'] == 's' ? 1 : 0;

            $table->save($entidade);
        }
    }

    public function migrarSiglas() {
        $tabela = 'Siglas';
        $this->out("Deletando os dados" . $tabela);
        $table = TableRegistry::get($tabela);

        $table->deleteAll(['id !=' => 0]);
        $conn = ConnectionManager::get('default');

        $conn->execute("ALTER TABLE siglas AUTO_INCREMENT = 1; ");

        $this->hr();

        $dados = $this->listarTabela('siglas');

        foreach ($dados as $item) {
            $this->out("$tabela " . $item['nome']);

            $entidade = $table->newEntity();
            $entidade->sigla = $item['sigla'];
            $entidade->fator = $item['fator'];
            $entidade->nome = $item['nome'];
            $table->save($entidade);
        }
    }

    public function migrarCategoriasVarejos() {
        $tabela = 'CategoriasVarejos';
        $this->out("Deletando os dados" . $tabela);
        $table = TableRegistry::get($tabela);

        $table->deleteAll(['id !=' => 0]);
        $conn = ConnectionManager::get('default');

        $conn->execute("ALTER TABLE categorias_varejos AUTO_INCREMENT = 1; ");

        $this->hr();

        $dados = $this->listarTabela('categoria_varejo');

        foreach ($dados as $item) {
            $this->out("$tabela " . $item['nome']);

            $entidade = $table->newEntity();
            $entidade->nome = $item['nome'];
            $entidade->codigo = $item['codigo'];
            $entidade->canal_id = $item['id_canal'];
            $entidade->demp_id = $item['id_demp'];
            $table->save($entidade);
        }
    }

    public function migrarContratos() {
        $tabela = 'Contratos';
        $this->out("Deletando os dados" . $tabela);
        $table = TableRegistry::get($tabela);

        $table->deleteAll(['id !=' => 0]);
        $conn = ConnectionManager::get('default');

        $conn->execute("ALTER TABLE contratos AUTO_INCREMENT = 1; ");

        $this->hr();

        $dados = $this->listarTabela('contratos');

        foreach ($dados as $item) {
            $this->out("$tabela " . $item['valor']);

            $entidade = $table->newEntity();

            $idCliente = $this->idCliente($item['cliente']);
            if(!$idCliente){
                $this->warn("Erro ao encontrar o cliente com numero " . $item['cliente'] . " ");
                continue;
            }

            $entidade->percentual = $item['percentual'];
            $entidade->valor = $item['valor'];
            $entidade->inicio = $item['inicio'];
            $entidade->final = $item['final'];
            $entidade->criado = date('Y-m-d');
            $entidade->cliente_id = $idCliente;
            $entidade->tipo_acordo_id = $this->idTipoAcordo($item['tipo']);

            $save = $table->save($entidade);

            if(!$save){
                debug($entidade);
                throw new \Exception("Erro ao salvar os $tabela");
            }
        }
    }

    public function migrarInadimplencias() {
        $tabela = 'Inadimplencias';
        $this->out("Deletando os dados" . $tabela);
        $table = TableRegistry::get($tabela);

        $table->deleteAll(['id !=' => 0]);
        $conn = ConnectionManager::get('default');

        $conn->execute("ALTER TABLE inadimplencias AUTO_INCREMENT = 1; ");

        $this->hr();

        $dados = $this->listarTabela('inadimplencia');

        foreach ($dados as $item) {
            $this->out("$tabela " . $item['titulo']);

            $entidade = $table->newEntity();

            $idCliente = $this->idCliente($item['cliente']);
            if(!$idCliente){
                $this->warn("Erro ao encontrar o cliente com numero " . $item['cliente'] . " ");
                continue;
            }

            $entidade->emissao = $item['data_emissao'];
            $entidade->titulo = $item['titulo'];
            $entidade->valor = $item['valor'];
            $entidade->vencimento = $item['data_vencimento'];
            $entidade->tipo = $item['tipo'];
            $entidade->clientes_id = $idCliente;

            $save = $table->save($entidade);

            if(!$save){
                debug($entidade);
                throw new \Exception("Erro ao salvar os $tabela");
            }
        }
    }

    public function migrarFamilias() {
        $tabela = 'Familias';
        $this->out("Deletando os dados" . $tabela);

        $table = TableRegistry::get($tabela);
        TableRegistry::get('Produtos')->deleteAll(['id !=' => 0]);

        $table->deleteAll(['id !=' => 0]);
        $conn = ConnectionManager::get('default');

        $conn->execute("ALTER TABLE produtos AUTO_INCREMENT = 1; ");
        $conn->execute("ALTER TABLE familias AUTO_INCREMENT = 1; ");

        $this->hr();

        $dados = $this->listarTabela('familias');

        foreach ($dados as $item) {
            $this->out("$tabela " . $item['nome']);

            $entidade = $table->newEntity();

            $entidade->id = $item['id'];
            $entidade->nome = $item['nome'];
            $entidade->hl = $item['hl'];
            $entidade->quantidade = $item['quantidade'];
            $entidade->peso = $item['peso'];
            $entidade->relatorios = $item['relatorios'] == 's' ? 1 : 0;
            $entidade->categoria_id = $item['id_categoria'];
            $entidade->grupo_id = $item['id_grupo'];

            $save = $table->save($entidade);

            if(!$save){
                debug($entidade);
                throw new \Exception("Erro ao salvar os $tabela");
            }
        }
    }

    public function migrarMetasStm() {
        $tabela = 'MetasStm';
        $this->out("Deletando os dados" . $tabela);
        $table = TableRegistry::get($tabela);

        $table->deleteAll(['id !=' => 0]);
        $conn = ConnectionManager::get('default');

        $conn->execute("ALTER TABLE metas_stm AUTO_INCREMENT = 1; ");

        $this->hr();

        $dados = $this->listarTabela('metas_stm');

        foreach ($dados as $item) {
            $this->out("$tabela " . $item['m_eventos'] . ' - ' . $item['r_eventos']);

            $mesAno = $item['ano'] . '-' . $item['mes'] . '-01';

            $entidade = $table->newEntity();
            $entidade->m_eventos = $item['m_eventos'];
            $entidade->r_eventos = $item['r_eventos'];
            $entidade->cob_especial = $item['cob_especial'];
            $entidade->cob_baden = $item['cob_baden'];
            $entidade->cob_devassa = $item['cob_devassa'];
            $entidade->mes_ano = $mesAno;

            $save = $table->save($entidade);

            if(!$save){
                debug($entidade);
                throw new \Exception("Erro ao salvar os $tabela");
            }
        }
    }

    public function migrarRankingSupervisores() {
        $tabela = 'RankingSupervisores';
        $this->out("Deletando os dados" . $tabela);
        $table = TableRegistry::get($tabela);

        $table->deleteAll(['id !=' => 0]);
        $conn = ConnectionManager::get('default');
        $conn->execute("ALTER TABLE ranking_supervisores AUTO_INCREMENT = 1; ");
        $this->hr();
        $dados = $this->listarTabela('ranking_sv');

        foreach ($dados as $item) {
            $this->out("$tabela " . $item['nome']);

            if($item['ano'] == '' && $item['mes'] == ''){
                $item['ano'] = date('Y');
                $item['mes'] = date('m');
            }

            $mesAno = $item['ano'] . '-' . $item['mes'] . '-01';
            $vendedor = explode(' -', $item['nome']);

            $entidade = $table->newEntity();
            $entidade->tendencia = $item['seiscentos'];
            $entidade->cob_total = $item['cob_total'];
            $entidade->cob_devassa = $item['cob_devassa'];
            $entidade->cob_esp = $item['cob_esp'];
            $entidade->cob_tendencia = $item['cob_seiscentos'];
            $entidade->pontos = $item['pontos'];
            $entidade->mes_ano = $mesAno;
            $entidade->supervisores_id = $this->idVendedores($vendedor[0]);

            if(!$table->save($entidade)){
                debug($entidade);
                throw new \Exception("Erro ao salvar os $tabela");
            }
        }
    }

    public function migrarRanking() {
        $tabela = 'Ranking';
        $this->out("Deletando os dados" . $tabela);
        $table = TableRegistry::get($tabela);

        $table->deleteAll(['id !=' => 0]);
        $conn = ConnectionManager::get('default');
        $conn->execute("ALTER TABLE ranking AUTO_INCREMENT = 1; ");
        $this->hr();
        $dados = $this->listarTabela('ranking');

        foreach ($dados as $item) {
            $this->out("$tabela " . $item['vendedor']);

            if (empty($item['mes']) || empty($item['ano'])) {
                $this->warn("Não foi encontrado o mes e o ano " . $item['vendedor'] . " ");
                continue;
            }

            $mesAno = $item['ano'] . '-' . $item['mes'] . '-01';

            $vendedor = explode(' -', $item['vendedor']);
            $vendedorId = $this->idVendedores($vendedor[0]);
            if (empty($vendedorId)) {
                $this->warn("Não foi encontrado o vendedor " . $item['vendedor'] . " ");
                continue;
            }

            $entidade = $table->newEntity();

            $entidade->tendencia = $item['seiscentos'];
            $entidade->cob_total = $item['cob_total'];
            $entidade->cob_devassa = $item['cob_devassa'];
            $entidade->cob_esp = $item['cob_esp'];
            $entidade->cob_tendencia = $item['cob_seiscentos'];
            $entidade->pontos = $item['pontos'];
            $entidade->mes_ano = $mesAno;
            $entidade->vendedores_id = $vendedorId;

            $save = $table->save($entidade);

            if(!$save){
                debug($item);
                debug($entidade);
                throw new \Exception("Erro ao salvar os $tabela");
            }
        }
    }

    public function listarTabela($tabela) {
        $conn = ConnectionManager::get('bd_old');
        return $conn->execute("select * from sao_rafael.$tabela order by 1")->fetchAll('assoc');
    }

    public function idCliente($numero) {
        return TableRegistry::get('Clientes')->buscarPorNumero($numero);
    }

    public function idVendedores($codigo) {
        $vendendorId = TableRegistry::get('Vendedores')->findByCodigo($codigo, false);
        if(!empty($vendendorId)){
            return $vendendorId;
        }
        $supervisorId = TableRegistry::get('Supervisores')->findByCodigo($codigo, false);
        if(!empty($supervisorId)){
            return $supervisorId;
        }
        $gerenteId= TableRegistry::get('Gerentes')->findByCodigo($codigo, false);
        if(!empty($gerenteId)){
            return $gerenteId;
        }
    }

    public function idTipoAcordo($nome) {
        if($nome == ''){
            return null;
        }
        $tipoTable = TableRegistry::get('TiposAcordos');
        $entidade = $tipoTable->find()->where(['nome' => $nome])->first();
        if(empty($entidade)){
            $entidade = $tipoTable->novo();
            $entidade->nome = $nome;
            $tipoTable->save($entidade);
        }
        return $entidade->id;
    }

    public function deletarTodos()
    {
        $this->out('Deletando os dados');
        TableRegistry::get('Precos')->deleteAll(['id !=' => 0]);
        TableRegistry::get('Metas')->deleteAll(['id !=' => 0]);
        TableRegistry::get('Produtos')->deleteAll(['id !=' => 0]);
        TableRegistry::get('CategoriasVarejos')->deleteAll(['id !=' => 0]);
        TableRegistry::get('Familias')->deleteAll(['id !=' => 0]);
        $this->hr();

        $conn = ConnectionManager::get('default');
        $conn->execute(
            "ALTER TABLE precos AUTO_INCREMENT = 1; " .
            "ALTER TABLE metas AUTO_INCREMENT = 1; " .
            "ALTER TABLE produtos AUTO_INCREMENT = 1; " .
            "ALTER TABLE familias AUTO_INCREMENT = 1; " .
            "ALTER TABLE categorias_varejos AUTO_INCREMENT = 1; "
        );
    }
}
