<?php
namespace App\Shell\Task;

use Cake\Console\Shell;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;

/**
 * VerificarImportacoes shell task.
 */
class VerificarImportacoesTask extends Shell
{
    private $arquivosTable;

    public function initialize()
    {
        parent::initialize();

        $this->arquivosTable = TableRegistry::getTableLocator()
            ->get('Arquivos');

    }
    /**
     * Manage the available sub-commands along with their arguments and help
     *
     * @see http://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     * @return \Cake\Console\ConsoleOptionParser
     */
    public function getOptionParser()
    {
        $parser = parent::getOptionParser();

        return $parser;
    }

    /**
     * main() method.
     *
     * @return bool|int|null Success or error code.
     */
    public function main()
    {
        $arquivos = $this->getArquivosNaoImportados();

        foreach ($arquivos as $arquivo) {
            $this->importarDados($arquivo);
        }
    }

    /**
     * @return mixed
     */
    private function getArquivosNaoImportados()
    {
        $naoImportados = $this->arquivosTable
            ->find()
            ->where([
                'importado' => false,
                'importando' => false,
            ])
            ->toArray();

        if (!empty($naoImportados)) {
            return $naoImportados;
        }

        $importandoParados = $this->arquivosTable
            ->find()
            ->where([
                'importando' => true,
                'importado' => false,
                'inicio_importacao <=' => Time::now()->modify('-10 minutes'),
                'tentativa_importacao <=' => 10
            ])
            ->toArray();

        return $importandoParados;
    }

    /**
     * @param $arquivo
     */
    private function importarDados($arquivo)
    {
        $retorno = [
            'mensagem' => ''
        ];

        switch ($arquivo->tipo) {
            case 'Cev':
                $retorno = TableRegistry::getTableLocator()
                    ->get('Cev')
                    ->importarArquivo($arquivo->id);
                    break;
            case 'Clientes':
                $retorno = TableRegistry::getTableLocator()
                    ->get('Clientes')
                    ->importarArquivo($arquivo->id);
                break;
            case 'Iav':
                $retorno = TableRegistry::getTableLocator()
                    ->get('Iav')
                    ->importarArquivo($arquivo->id);
                    break;
            case 'Inadimplencias':
                $retorno = TableRegistry::getTableLocator()
                    ->get('Inadimplencias')
                    ->importarArquivo($arquivo->id);
                    break;
            case 'Metas':
                $mesAno = $arquivo->mais_parametros;
                $retorno = TableRegistry::getTableLocator()
                    ->get('Metas')
                    ->importarArquivo($arquivo->id, $mesAno);
                    break;
            case 'MetasGerais':
                $mesAno = $arquivo->mais_parametros;
                $retorno = TableRegistry::getTableLocator()
                    ->get('MetasGerais')
                    ->importarArquivo($arquivo->id, $mesAno);
                    break;
            case 'Pedidos':
                $retorno = TableRegistry::getTableLocator()
                    ->get('Pedidos')
                    ->importarArquivo($arquivo->id);
                break;
            case 'PedidosHistoricos':
                $retorno = TableRegistry::getTableLocator()
                    ->get('PedidosHistoricos')
                    ->importarArquivo($arquivo->id);
                break;
            case 'Pesquisas':
                $retorno = TableRegistry::getTableLocator()
                    ->get('Pesquisas')
                    ->importarArquivo($arquivo->id);
                break;
            default:
                $this->out('Tabela nao configurada ' . $arquivo->tipo);
                $this->out('Arquivo id ' . $arquivo->id);

        }

        if($retorno['mensagem'] !== ''){
            $this->out('Mensagem Erro ' . $retorno['mensagem']);
            $this->out('Arquivo importado as executada as ' . Time::now());
            $this->hr();
            $this->hr();
            $this->hr();
        }
        $this->hr();
    }
}
