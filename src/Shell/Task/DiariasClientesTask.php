<?php

namespace App\Shell\Task;

use Cake\Console\Shell;
use Cake\I18n\Time;
use Cake\ORM\Locator\TableLocator;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;

/**
 * Simple console wrapper around Psy\Shell.
 */
class DiariasClientesTask extends Shell {

    /**
     * Start the shell and interactive console.
     *
     * @return int|null
     */
    public function main() {

    }

    /**
     * Start the shell and interactive console.
     *
     * @return int|null
     */
    public function importar($arquivoId) {
        $this->hr();
        $this->out('Importacao de Clientes');
        $inicio = Time::now();
        $this->out('Comecando as: ' . $inicio);
        $this->hr();
        $this->out();
        $this->out();
        $this->out('arquivo id ' . $arquivoId);

        $conn = ConnectionManager::get('default');
        $conn->begin();

        //Importa o arquivo de Pesquisa
        $retorno = TableRegistry::getTableLocator()
            ->get('Clientes')
            ->importarArquivo($arquivoId);

        if($retorno['mensagem'] !== ''){
            $this->out('Mensagem Erro ' . $retorno['mensagem']);
        }
        $conn->commit();

        $fim = Time::now();
        $dateInterval = $inicio->diff($fim);
        $this->out('Terminando as: ' . $fim);
        $this->out('Tempo total de: ' . "{$dateInterval->h} hora(s), {$dateInterval->m} minuto(s) e {$dateInterval->s} segundo(s)");
    }
}
