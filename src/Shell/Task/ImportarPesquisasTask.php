<?php

namespace App\Shell\Task;

use Cake\Console\Shell;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;

/**
 * Simple console wrapper around Psy\Shell.
 */
class ImportarPesquisasTask extends Shell {

    /**
     * Start the shell and interactive console.
     *
     * @return int|null
     */
    public function main() {
        $this->hr();
        $this->out('Importação Pesquisas');
        $inicio = Time::now();
        $this->out('Comecando as: ' . $inicio);
        $this->hr();
        $this->out();
        $this->out();

        $conn = ConnectionManager::get('default');
        $conn->begin();

        $this->deletarTodos();
        $this->migrar();

        $conn->commit();
    }

    public function migrar() {
        $table = TableRegistry::get('Pesquisas');
        $clientesTable = TableRegistry::get('Clientes');

        $entidades = $this->listarOld();

        $count = 0;
        foreach ($entidades as $item) {
            $count++;
            if (($count % 100) == 0) {
                $this->out('pesquisa total '. $count . ' - pesquisa mes ano meta ' . $item['versao']);
            }

            $clienteId = $clientesTable->buscarPorNumero($item['cliente']);
            if(empty($clienteId)){
                continue;
            }

            $entidade = $table->novo();

            $entidade->versao = $item['versao'];
            $entidade->motivo = $item['motivo'];
            $entidade->clientes_id = $clienteId;
            $entidade->data = $item['data'];
            $entidade->quantidade = $item['quantidade'];
            $entidade->pvv = $item['pvv'];
            $entidade->pvc = $item['pvc'];

            $save = $table->save($entidade);
            if (!$save) {
                debug($entidade);
                die;
            }
        }
        $this->hr();
    }

    public function listarOld() {

        $conn = ConnectionManager::get('bd_old');
        return $conn->execute("select * from sao_rafael.pesquisa")->fetchAll('assoc');
    }

    public function deletarTodos() {

        $this->out('Deletando os dados');
        TableRegistry::get('Pesquisas')->deleteAll(['id !=' => 0]);
        $this->hr();

        $conn = ConnectionManager::get('default');
        $conn->execute("ALTER TABLE pesquisas AUTO_INCREMENT = 1; ");
    }
}
