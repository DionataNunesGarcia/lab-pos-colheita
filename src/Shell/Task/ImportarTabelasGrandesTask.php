<?php

namespace App\Shell\Task;

use Cake\Console\Shell;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;

/**
 * Simple console wrapper around Psy\Shell.
 */
class ImportarTabelasGrandesTask extends Shell {

    /**
     * Start the shell and interactive console.
     *
     * @return int|null
     */
    public function main() {
        $this->hr();
        $this->out('Importação Tabelas Grandes');
        $inicio = Time::now();
        $this->out('Comecando as: ' . $inicio);
        $this->hr();
        $this->out();
        $this->out();

        $conn = ConnectionManager::get('default');
        $conn->begin();
        $this->migrarIav();
        $this->migrarCev();
        $this->migrarFundamentosStm();

        $conn->commit();
    }

    public function migrarIav() {
        $tabela = 'iav';
        $this->out("Deletando os dados " . $tabela);

        $table = TableRegistry::get($tabela);

        $table->deleteAll(['id !=' => 0]);
        $conn = ConnectionManager::get('default');

        $conn->execute("ALTER TABLE $tabela AUTO_INCREMENT = 1; ");

        $this->hr();

        $dados = $this->listarTabela('iav');

        $count = 0;
        foreach ($dados as $item) {
            $count++;

            if($item['ano'] == '' && $item['mes'] == ''){
                $item['ano'] = date('Y');
                $item['mes'] = date('m');
            }
            $mesAno = $item['ano'] . '-' . $item['mes'] . '-01';

            if (($count % 100) == 0)  {
                $this->out("Total $count - $tabela {$item['pasta']} | {$mesAno}" );
            }

            if ($item['vendedor'] == '') {
                continue;
            } else {
                $vendedorId = $this->idVendedores($item['vendedor']);
                if (empty($vendedorId)) {
                    continue;
                }
            }

            $entidade = $table->newEntity();
            $entidade->pasta = $item['pasta'];
            $entidade->totcli = $item['totcli'];
            $entidade->totcomp = $item['totcomp'];
            $entidade->cob = valorToValue($item['cob']);
            $entidade->totvis = $item['totvis'];
            $entidade->totped = $item['totped'];
            $entidade->pos = valorToValue($item['pos']);
            $entidade->visit = $item['visit'];
            $entidade->iav = valorToValue($item['iav']);
            $entidade->mes_ano = $mesAno;
            $entidade->vendedor_id = $vendedorId;

            $save = $table->save($entidade);

            if (!$save) {
                debug($entidade);
            }
        }
    }

    public function migrarCev() {
        $tabela = 'Cev';
        $this->out("Deletando os dados " . $tabela);

        $table = TableRegistry::get($tabela);

        $table->deleteAll(['id !=' => 0]);
        $conn = ConnectionManager::get('default');

        $conn->execute("ALTER TABLE $tabela AUTO_INCREMENT = 1; ");

        $this->hr();

        $dados = $this->listarTabela('cev');

        $count = 0;
        foreach ($dados as $item) {
            $count++;
            if (($count % 100) == 0)  {
                $this->out("Total $count - $tabela " . $item['numero']);
            }

            if ($item['cliente'] == '') {
                continue;
            } else {
                $clienteId = $this->idCliente($item['cliente']);
                if (empty($clienteId)) {
                    $this->out("ERRO -> CEV sem cliente encontrado " . $item['numero']);
                    continue;
                }
            }

            $entidade = $table->newEntity();
            $entidade->numero = $item['numero'];
            $entidade->tendencia = $item['seiscentos'];
            $entidade->litro = $item['litro'];
            $entidade->mesa_plastico = $item['mesa_plast'];
            $entidade->vago1 = $item['vago1'];
            $entidade->ref_peq = $item['ref_peq'];
            $entidade->ref_grande = $item['ref_gde'];
            $entidade->visa = $item['visa'];
            $entidade->freezer = $item['freezer'];
            $entidade->mesa_mad = $item['mesa_mad'];
            $entidade->data_emissao = $item['data_emissao'];
            $entidade->data_vencimento = $item['data_vencimento'];
            $entidade->clientes_id = $clienteId;

            $save = $table->save($entidade);

            if (!$save) {
                debug($entidade);
                die;
            }
        }
    }

    public function migrarFundamentosStm() {
        $tabela = 'FundamentosStm';
        $this->out("Deletando os dados " . $tabela);
        $table = TableRegistry::get($tabela);

        $table->deleteAll(['id !=' => 0]);
        $conn = ConnectionManager::get('default');
        $conn->execute("ALTER TABLE fundamentos_stm AUTO_INCREMENT = 1; ");
        $this->hr();

        $dados = $this->listarTabela('fundamentos_stm');

        foreach ($dados as $item) {

            if($item['ano'] == '' && $item['mes'] == ''){
                $item['ano'] = date('Y');
                $item['mes'] = date('m');
            }
            $mesAno = $item['ano'] . '-' . $item['mes'] . '-01';

            $this->out("$tabela " . $mesAno);

            $entidade = $table->newEntity();
            $entidade->especiais = $item['especiais'];
            $entidade->eisen = $item['eisen'];
            $entidade->total_volume = $item['total_volume'];
            $entidade->refrigeracao = $item['refrigeracao'];
            $entidade->execucao = $item['execucao'];
            $entidade->giro = $item['giro'];
            $entidade->total_execucao = $item['total_execucao'];
            $entidade->cob_especial = $item['cob_especial'];
            $entidade->cob_baden = $item['cob_baden'];
            $entidade->cob_devassa = $item['cob_devassa'];
            $entidade->demp = $item['demp'];
            $entidade->total_distribuicao = $item['total_distribuicao'];
            $entidade->total_fundamentos = $item['total_fundamentos'];
            $entidade->mes_ano = $mesAno;

            $save = $table->save($entidade);

            if (!$save) {
                debug($item);
                debug($entidade);
                die;
            }
        }
    }

    public function listarTabela($tabela) {
        $conn = ConnectionManager::get('bd_old');
        return $conn->execute("select * from sao_rafael.$tabela order by 1")->fetchAll('assoc');
    }

    public function idContrato($numero) {
        return TableRegistry::get('Clientes')->buscarPorNumero($numero);
    }

    public function idVendedores($codigo) {
        $vendendorId = TableRegistry::get('Vendedores')->findByCodigo($codigo, false);
        if (!empty($vendendorId)) {
            return $vendendorId;
        }
        $supervisorId = TableRegistry::get('Supervisores')->findByCodigo($codigo, false);
        if (!empty($supervisorId)) {
            return $supervisorId;
        }
        $gerenteId = TableRegistry::get('Gerentes')->findByCodigo($codigo, false);
        if (!empty($gerenteId)) {
            return $gerenteId;
        }
    }

    public function idCliente($numero) {
        return TableRegistry::get('Clientes')->buscarPorNumero($numero, 'codigo');
    }
}
