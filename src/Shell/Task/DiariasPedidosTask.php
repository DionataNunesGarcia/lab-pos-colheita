<?php

namespace App\Shell\Task;

use App\Service\AtualizarRanking\AtualizarRankingVendedoresService;
use App\Service\AtualizarValores\AtualizarValoresVendedoresService;
use Cake\Console\Shell;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;

/**
 * Simple console wrapper around Psy\Shell.
 */
class DiariasPedidosTask extends Shell {

    /**
     * Start the shell and interactive console.
     *
     * @return int|null
     */
    public function main() {

    }

    /**
     * Start the shell and interactive console.
     *
     * @return int|null
     */
    public function importar($arquivoId) {
        $this->hr();
        $this->out('Importacao Diaria de Pedidos');
        $inicio = Time::now();
        $this->out('Comecando as: ' . $inicio);
        $this->hr();
        $this->out();
        $this->out();
        $this->out('arquivo id ' . $arquivoId);

        //Importa o arquivo de Pesquisa
        $retorno = TableRegistry::get('Pedidos')->importarArquivo($arquivoId);

        if($retorno['mensagem'] !== ''){
            $this->out('Mensagem Erro ' . $retorno['mensagem']);
        }

        $this->atualizarRankingVendedores();
        $this->atualizarValoresVendedores();

        $fim = Time::now();
        $dateInterval = $inicio->diff($fim);
        $this->out('Terminando as: ' . $fim);
        $this->out('Tempo total de: ' . "{$dateInterval->h} hora(s), {$dateInterval->m} minuto(s) e {$dateInterval->s} segundo(s)");
    }

    public function atualizarRankingVendedores()
    {
        $service = new AtualizarRankingVendedoresService();

        $service->atualizaRankingVendedores();
    }

    public function atualizarValoresVendedores()
    {
        $service = new AtualizarValoresVendedoresService();

        $service->atualizaValoresVendedores();
    }

    /**
     * Start the shell and interactive console.
     *
     * @return int|null
     */
    public function importarPedidosHistorico($arquivoId) {
        $this->hr();
        $this->out('Importacao Mensal de Historico de Pedidos');
        $inicio = Time::now();
        $this->out('Comecando as: ' . $inicio);
        $this->hr();
        $this->out();
        $this->out();
        $this->out('arquivo id ' . $arquivoId);

        //Importa o arquivo de Pesquisa
        $retorno = TableRegistry::get('PedidosHistoricos')->importarArquivo($arquivoId);

        if($retorno['mensagem'] !== ''){
            $this->out('Mensagem Erro ' . $retorno['mensagem']);
        }

        $this->atualizarRankingVendedores();

        $fim = Time::now();
        $dateInterval = $inicio->diff($fim);
        $this->out('Terminando as: ' . $fim);
        $this->out('Tempo total de: ' . "{$dateInterval->h} hora(s), {$dateInterval->m} minuto(s) e {$dateInterval->s} segundo(s)");
    }
}
