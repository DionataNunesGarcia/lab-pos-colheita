<?php

namespace App\Shell\Task;

use Cake\Console\Shell;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;

/**
 * Simple console wrapper around Psy\Shell.
 */
class ImportarPrecosTask extends Shell {

    /**
     * Start the shell and interactive console.
     *
     * @return int|null
     */
    public function main() {
        $this->hr();
        $this->out('Importação Precos');
        $inicio = Time::now();
        $this->out('Comecando as: ' . $inicio);
        $this->hr();
        $this->out();
        $this->out();

        $conn = ConnectionManager::get('default');
        $conn->begin();

        $this->deletarTodos();
        $this->migrar();

        $conn->commit();
    }

    public function migrar() {
        $table = TableRegistry::get('Precos');
        $produtosTable = TableRegistry::get('Produtos');
        $entidades = $this->listarOld();

        foreach ($entidades as $item) {
            $this->out('Preco Produto ' . $item['produto']);

            $produtoId = $produtosTable->buscarPorNumero($item['produto']);
            if(empty($produtoId)){
                continue;
            }
            $entidade = $table->novo();
            $entidade->preco = $item['preco'];
            $entidade->tabela = $item['tabela'];
            $entidade->produtos_id = $produtoId;

            $save = $table->save($entidade);
            if (!$save) {
                debug($entidade);
                die;
            }
        }
        $this->hr();
    }

    public function listarOld() {

        $conn = ConnectionManager::get('bd_old');
        return $conn->execute("
            select  * from sao_rafael.precos
        ")->fetchAll('assoc');
    }

    public function deletarTodos() {

        $this->out('Deletando os dados');
        TableRegistry::get('Precos')->deleteAll(['id !=' => 0]);
        $this->hr();

        $conn = ConnectionManager::get('default');
        $conn->execute("ALTER TABLE precos AUTO_INCREMENT = 1; ");
    }

}
