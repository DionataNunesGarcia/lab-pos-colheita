<?php

namespace App\Shell\Task;

use Cake\Console\Shell;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;

/**
 * Simple console wrapper around Psy\Shell.
 */
class ImportarVendedoresTask extends Shell {

    /**
     * Start the shell and interactive console.
     *
     * @return int|null
     */
    public function main() {
        $this->hr();
        $this->out('Importação Vendedores');
        $inicio = Time::now();
        $this->out('Comecando as: ' . $inicio);
        $this->hr();
        $this->out();
        $this->out();

        $conn = ConnectionManager::get('default');
        $conn->begin();

        $this->deletarTodos();
        $this->migrarGerentes();
        $this->migrarSupervisores();
        $this->migrarVendedores();

        $conn->commit();
    }

    public function migrarVendedores() {
        $tableVendedores = TableRegistry::get('Vendedores');
        $tableUsuario = TableRegistry::get('Usuarios');
        $tablePessoa = TableRegistry::get('PessoasFisicas');
        $vendedores = $this->listarVendedoresOld();

        foreach ($vendedores as $item) {
            $this->out('Vendedor ' . $item['nome']);

            $usuario = $tableUsuario->novo();

            $item['data_admissao'] = !empty($item['data']) ? $item['data'] : Time::now();

            $usuario->usuario = !empty($item['nome']) ? $item['nome'] : $item['dp'];
            $usuario->senha = !empty($item['nome']) ? $item['nome'] : $item['dp'];
            $usuario->niveis_id = $item['niveis_id'];
            $usuario->ativo = true;
            $usuario->sexo = 'm';
            $usuario->deletado = 0;
            $usuario->criado = Time::now();

            $usuarioSave = $tableUsuario->save($usuario);
            if (!$usuarioSave) {
                debug($usuario);
                die;
            }

            $pessoa = $tablePessoa->novo();
            $pessoa->nome = !empty($item['nome']) ? $item['nome'] : $item['dp'];
            $pessoa->email = null;
            $pessoa->cpf = null;
            $pessoa->data_nascimento = null;
            $pessoa->sexo = 'm';
            $pessoa->criado = Time::now();

            $pessoaSave = $tablePessoa->save($pessoa);

            $vendedor = $tableVendedores->novo();

            $vendedor->canal = '';
            if ($item['canal'] == 'asi') {
                $vendedor->canal = 'ASI';
            }
            if ($item['canal'] == 'frio') {
                $vendedor->canal = 'Frio';
            }
            if ($item['canal'] == 'premium') {
                $vendedor->canal = 'Premium';
            }

            $vendedor->codigo = $item['codigo'];
            $vendedor->dp = $item['dp'];
            $vendedor->relatorios = (intval($item['relatorios']) == 1) ? true : false;
            $vendedor->fixo = $item['fixo'];

            $vendedor->comissao_ret = $item['comissao'];
            $vendedor->supervisor_id = $item['supervisor_id'];
            $vendedor->usuario_id = $usuarioSave->id;
            $vendedor->pessoa_fisica_id = $pessoaSave->id;
            $vendedor->data_admissao = $item['data_admissao'];

            $save = $tableVendedores->save($vendedor);
        }
        $this->hr();
    }

    public function migrarGerentes() {

        $tableGerentes = TableRegistry::get('Gerentes');
        $tableUsuario = TableRegistry::get('Usuarios');
        $tablePessoa = TableRegistry::get('PessoasFisicas');
        $gerentes = $this->listarGerentesOld();

        foreach ($gerentes as $item) {
            $this->out('Gerente ' . $item['nome']);

            $usuario = $tableUsuario->novo();

            $usuario->usuario = $item['nome_pessoa'];
            $usuario->senha = $item['nome_pessoa'];
            $usuario->niveis_id = $item['niveis_id'];
            $usuario->ativo = true;
            $usuario->sexo = $item['sexo'];
            $usuario->deletado = 0;
            $usuario->criado = $item['criado'];

            $usuarioSave = $tableUsuario->save($usuario);
            if (!$usuarioSave) {
                debug($usuario);
                throw new \Exception(__("Erro ao salvar usuario gerente"));
            }

            $pessoa = $tablePessoa->novo();
            $pessoa->nome = $item['nome_pessoa'];
            $pessoa->email = $item['email'];
            $pessoa->cpf = $item['cpf'];
            $pessoa->data_nascimento = null;
            $pessoa->sexo = $item['sexo'];
            $pessoa->criado = $item['criado'];

            $pessoaSave = $tablePessoa->save($pessoa);
            if (!$pessoaSave) {
                throw new \Exception(__("Erro ao salvar pessoa gerente"));
            }

            $gerente = $tableGerentes->novo();
            $gerente->codigo = $item['codigo'];
            $gerente->dp = $item['dp'];
            $gerente->relatorios = (intval($item['relatorios']) == 1) ? true : false;
            $gerente->usuario_id = $usuarioSave->id;
            $gerente->pessoa_fisica_id = $pessoaSave->id;

            $save = $tableGerentes->save($gerente);
            if (!$save) {
                throw new \Exception(__("Erro ao salvar na tabela gerente"));
            }
        }
        $this->hr();
    }

    public function migrarSupervisores() {

        $tableSupervisores = TableRegistry::get('Supervisores');
        $tableUsuario = TableRegistry::get('Usuarios');
        $tablePessoa = TableRegistry::get('PessoasFisicas');
        $supervisores = $this->listarSupervisoresOld();

        foreach ($supervisores as $item) {
            $this->out('Supervisor ' . $item['nome']);

            $usuario = $tableUsuario->novo();

            $item['criado'] = !empty($item['criado']) ? $item['criado'] : Time::now();

            $usuario->usuario = $item['nome'];
            $usuario->senha = $item['nome'];
            $usuario->niveis_id = $item['niveis_id'];
            $usuario->ativo = true;
            $usuario->sexo = 'm';
            $usuario->deletado = 0;
            $usuario->criado = Time::now();

            $usuarioSave = $tableUsuario->save($usuario);
            if (!$usuarioSave) {
                debug($usuario);
                die;
            }

            $pessoa = $tablePessoa->novo();
            $pessoa->nome = $item['nome'];
            $pessoa->email = null;
            $pessoa->cpf = null;
            $pessoa->data_nascimento = null;
            $pessoa->sexo = 'm';
            $pessoa->criado = Time::now();

            $pessoaSave = $tablePessoa->save($pessoa);

            $supervisor = $tableSupervisores->novo();

            $supervisor->codigo = $item['codigo'];
            $supervisor->dp = $item['dp'];
            $supervisor->relatorios = (intval($item['relatorios']) == 1) ? true : false;
            $supervisor->gerente_id = $item['gerente_id'];
            $supervisor->usuarios_id = $usuarioSave->id;
            $supervisor->pessoa_fisica_id = $pessoaSave->id;

            $save = $tableSupervisores->save($supervisor);
        }
        $this->hr();
    }

    public function listarVendedoresOld() {

        $conn = ConnectionManager::get('bd_old');
        return $conn->execute("
            select 
                id, 
                codigo, 
                gerente, 
                supervisor, 
                nome, 
                dp,
                `data`, 
                case 
                    when relatorios = 's' then 1
                    else 0
                end relatorios,
                canal, 
                fixo, 
                comissao,
                (select s.id from `sao-rafael-bebidas`.`supervisores` s where s.codigo = v.supervisor limit 1) supervisor_id,
                (select n.id from `sao-rafael-bebidas`.`niveis` n where n.nome = 'Vendedor' limit 1) niveis_id 
           from sao_rafael.vendedores v
        ")->fetchAll('assoc');
    }

    public function listarGerentesOld() {

        $conn = ConnectionManager::get('bd_old');
        return $conn->execute("
            select 
                id,
                codigo,
                dp,
                case 
                    when relatorios = 's' then 1
                    else 0
                end relatorios,
                g.*,
                nome nome_pessoa,
                null cpf,
                null email,
                null data_nascimento,
                'm' sexo,
                current_timestamp criado,
                (select id from `sao-rafael-bebidas`.`niveis` where nome = 'Gerente' limit 1) niveis_id
            from sao_rafael.gerentes g
        ")->fetchAll('assoc');
    }

    public function listarSupervisoresOld() {

        $conn = ConnectionManager::get('bd_old');
        return $conn->execute("
            select 
                s.*,
                case 
                    when relatorios = 's' then 1
                    else 0
                end relatorios,
                case 
                    when s.data = '' and data is null then current_date
                    else s.data
                end criado,
                (select ger.id from `sao-rafael-bebidas`.`gerentes` ger where ger.codigo = s.gerente limit 1) gerente_id,
                (select id from `sao-rafael-bebidas`.`niveis` where nome = 'Supervisor' limit 1) niveis_id
            from sao_rafael.supervisores s
        ")->fetchAll('assoc');
    }

    public function deletarTodos() {

        $this->out('Deletando os dados');
        TableRegistry::get('MetasPessoas')->deleteAll(['id !=' => 0]);
        TableRegistry::get('Metas')->deleteAll(['id !=' => 0]);
        TableRegistry::get('ClientesTelefones')->deleteAll(['id !=' => 0]);
        TableRegistry::get('UsuariosMensagens')->deleteAll(['id !=' => 0]);
        TableRegistry::get('Trades')->deleteAll(['id !=' => 0]);
        TableRegistry::get('ranking')->deleteAll(['id !=' => 0]);
        TableRegistry::get('rankingsupervisores')->deleteAll(['id !=' => 0]);
        TableRegistry::get('FundamentosBrkPessoas')->deleteAll(['id !=' => 0]);
        TableRegistry::get('Iav')->deleteAll(['id !=' => 0]);
        TableRegistry::get('MetasGeraisPessoas')->deleteAll(['id !=' => 0]);
        TableRegistry::get('Contratos')->deleteAll(['id !=' => 0]);
        TableRegistry::get('Cev')->deleteAll(['id !=' => 0]);
        TableRegistry::get('Status')->deleteAll(['id !=' => 0]);
        TableRegistry::get('Inadimplencias')->deleteAll(['id !=' => 0]);
        TableRegistry::get('Pesquisas')->deleteAll(['id !=' => 0]);
        TableRegistry::get('PrazosPedidosHistoricos')->deleteAll(['id !=' => 0]);
        TableRegistry::get('PedidosHistoricos')->deleteAll(['id !=' => 0]);
        TableRegistry::get('PrazosPedidos')->deleteAll(['id !=' => 0]);
        TableRegistry::get('Pedidos')->deleteAll(['id !=' => 0]);
        TableRegistry::get('Clientes')->deleteAll(['id !=' => 0]);
        TableRegistry::get('Vendedores')->deleteAll(['id !=' => 0]);
        TableRegistry::get('Supervisores')->deleteAll(['id !=' => 0]);
        TableRegistry::get('Gerentes')->deleteAll(['id !=' => 0]);
        TableRegistry::get('PessoasFisicas')->deleteAll(['id !=' => 0]);
        TableRegistry::get('Usuarios')->deleteAll(['niveis_id !=' => 1]);
        $this->hr();


        $conn = ConnectionManager::get('default');
        $conn->execute(
                "ALTER TABLE clientes_telefones AUTO_INCREMENT = 1; " .
                "ALTER TABLE usuarios_mensagens AUTO_INCREMENT = 1; " .
                "ALTER TABLE trades AUTO_INCREMENT = 1; " .
                "ALTER TABLE ranking AUTO_INCREMENT = 1; " .
                "ALTER TABLE ranking_supervisores AUTO_INCREMENT = 1; " .
                "ALTER TABLE fundamentos_brk_pessoas AUTO_INCREMENT = 1; " .
                "ALTER TABLE iav AUTO_INCREMENT = 1; " .
                "ALTER TABLE contratos AUTO_INCREMENT = 1; " .
                "ALTER TABLE metas AUTO_INCREMENT = 1; " .
                "ALTER TABLE metas_pessoas AUTO_INCREMENT = 1; " .
                "ALTER TABLE cev AUTO_INCREMENT = 1; " .
                "ALTER TABLE status AUTO_INCREMENT = 1; " .
                "ALTER TABLE inadimplencias AUTO_INCREMENT = 1; " .
                "ALTER TABLE pesquisas AUTO_INCREMENT = 1; " .
                "ALTER TABLE prazos_pedidos_historicos AUTO_INCREMENT = 1; " .
                "ALTER TABLE pedidos_historicos AUTO_INCREMENT = 1; " .
                "ALTER TABLE prazos_pedidos AUTO_INCREMENT = 1; " .
                "ALTER TABLE pedidos AUTO_INCREMENT = 1; " .
                "ALTER TABLE clientes AUTO_INCREMENT = 1; " .
                "ALTER TABLE vendedores AUTO_INCREMENT = 1; " .
                "ALTER TABLE supervisores AUTO_INCREMENT = 1; " .
                "ALTER TABLE gerentes AUTO_INCREMENT = 1; " .
                "ALTER TABLE pessoas_fisicas AUTO_INCREMENT = 1; " .
                "ALTER TABLE usuarios AUTO_INCREMENT = 10; "
        );
    }

}
