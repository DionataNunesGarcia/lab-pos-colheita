<?php

namespace App\Shell\Task;

use Cake\Console\Shell;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;

/**
 * Simple console wrapper around Psy\Shell.
 */
class ImportarMetasTask extends Shell {

    /**
     * Start the shell and interactive console.
     *
     * @return int|null
     */
    public function main() {
        $this->hr();
        $this->out('Importação Metas');
        $inicio = Time::now();
        $this->out('Comecando as: ' . $inicio);
        $this->hr();
        $this->out();
        $this->out();

        $conn = ConnectionManager::get('default');
        $conn->begin();

        $this->deletarTodos();
        $this->migrar();

        $conn->commit();
    }

    public function migrar() {
        $table = TableRegistry::get('Metas');
        $produtosTable = TableRegistry::get('Produtos');
        $entidades = $this->listarOld();

        $count = 0;
        foreach ($entidades as $item) {
            $count++;
            if ($count % 100) {
                $this->out('Meta total '. $count . ' - ID ' . $item['id'] . ', produto meta ' . $item['produto_meta']);
            }

            $produtoID = $produtosTable->buscarPorNumero($item['produto']);
            if(empty($produtoID)){
                continue;
            }

            $tipoPessoaId = $table->buscarPessoaCargo($item['pessoa']);
            if(empty($tipoPessoaId)){
                continue;
            }

            $entidade = $table->novo();

            $entidade->produtos_id = $produtoID;
            $entidade->produto_meta = $item['produto_meta'];
            $entidade->meta_cobertura = $item['meta_cobertura'];
            $entidade->meta_pm = $item['meta_pm'];
            $entidade->mes_ano = $item['ano'] . '-' . $item['mes'] . '-01';

            $save = $table->save($entidade);
            if (!$save) {
                debug($entidade);
                die;
            }

            $metasPessoas = TableRegistry::get('MetasPessoas');
            $metaPessoa = $metasPessoas->newEntity();
            $metaPessoa->metas_id = $entidade->id;

            switch ($tipoPessoaId['tipo']){
                case 'v':
                    $metaPessoa->vendedores_id = $tipoPessoaId['id'];
                    break;
                case 'g':
                    $metaPessoa->gerentes_id = $tipoPessoaId['id'];
                    break;
                case 's':
                    $metaPessoa->supervisores_id = $tipoPessoaId['id'];
                    break;
            }

            $saveMetasPessoas = $metasPessoas->save($metaPessoa);
            if (!$saveMetasPessoas) {
                debug($metaPessoa);
                die;
            }
        }
        $this->hr();
    }

    public function listarOld() {

        $conn = ConnectionManager::get('bd_old');
        return $conn->execute("select * from sao_rafael.metas")->fetchAll('assoc');
    }

    public function deletarTodos() {

        $this->out('Deletando os dados');
        TableRegistry::get('Metas')->deleteAll(['id !=' => 0]);
        $this->hr();

        $conn = ConnectionManager::get('default');
        $conn->execute("ALTER TABLE metas AUTO_INCREMENT = 1; ");
    }
}
