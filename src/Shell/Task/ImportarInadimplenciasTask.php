<?php

namespace App\Shell\Task;

use Cake\Console\Shell;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;

/**
 * Simple console wrapper around Psy\Shell.
 */
class ImportarInadimplenciasTask extends Shell {

    /**
     * Start the shell and interactive console.
     *
     * @return int|null
     */
    public function main() {
        $this->hr();
        $this->out('Importação Inadimplencias');
        $inicio = Time::now();
        $this->out('Comecando as: ' . $inicio);
        $this->hr();
        $this->out();
        $this->out();

        $conn = ConnectionManager::get('default');
        $conn->begin();

        $this->deletarTodos();
        $this->migrar();

        $conn->commit();
    }

    public function migrar() {
        $table = TableRegistry::get('Inadimplencias');
        $clientesTable = TableRegistry::get('Clientes');
        $entidades = $this->listarOld();

        $count = 0;
        $countError = 0;
        foreach ($entidades as $item) {
            $count++;
            if(($count % 100) == 0) {
                $this->out("Total $count - Inadimplencia Cliente " . $item['cliente']);
            }

            $clienteId = $clientesTable->buscarPorNumero($item['cliente'], 'codigo');
            if(!$clienteId){
                $countError++;
                $this->warn("não encontrado {$item['cliente']}");
                continue;
            }

            $entidade = $table->novo();
            $entidade->emissao = $item['data_emissao'];
            $entidade->titulo = $item['titulo'];
            $entidade->valor = $item['valor'];
            $entidade->vencimento = $item['data_vencimento'];
            $entidade->tipo = $item['tipo'];
            $entidade->clientes_id = $clienteId;

            $save = $table->save($entidade);
            if (!$save) {
                debug($entidade);
                die;
            }
        }

        $this->hr();
        $this->out("Total inadimplecia importada $count");
        $this->out("Total inadimplecia não importada $countError");
    }

    public function listarOld() {

        $conn = ConnectionManager::get('bd_old');
        return $conn->execute("
            select  * from sao_rafael.inadimplencia
        ")->fetchAll('assoc');
    }

    public function deletarTodos() {

        $this->out('Deletando os dados');
        TableRegistry::get('Inadimplencias')->deleteAll(['id !=' => 0]);
        $this->hr();

        $conn = ConnectionManager::get('default');
        $conn->execute("ALTER TABLE inadimplencias AUTO_INCREMENT = 1; ");
    }

}
