<?php

namespace App\Shell\Task;

use Cake\Console\Shell;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;

/**
 * Simple console wrapper around Psy\Shell.
 */
class ImportarStatusTask extends Shell {

    /**
     * Start the shell and interactive console.
     *
     * @return int|null
     */
    public function main() {
        $this->hr();
        $this->out('Importação Status');
        $inicio = Time::now();
        $this->out('Comecando as: ' . $inicio);
        $this->hr();
        $this->out();
        $this->out();

        $conn = ConnectionManager::get('default');
        $conn->begin();

        $this->deletarTodos();
        $this->migrar();

        $conn->commit();
    }

    public function migrar() {
        $table = TableRegistry::get('Status');
        $clientesTable = TableRegistry::get('Clientes');
        $entidades = $this->listarOld();

        foreach ($entidades as $item) {
            $this->out('Cliente ' . $item['cliente']);

            $clienteId = $clientesTable->buscarPorNumero($item['cliente']);
            if(empty($clienteId)){
                continue;
            }
            $entidade = $table->novo();
            $entidade->status = $item['status'];
            $entidade->observacoes = $item['obs'];
            $entidade->clientes_id = $clienteId;

            $save = $table->save($entidade);
            if (!$save) {
                debug($entidade);
                die;
            }
        }
        $this->hr();
    }

    public function listarOld() {

        $conn = ConnectionManager::get('bd_old');
        return $conn->execute("
            select  * from sao_rafael.status
        ")->fetchAll('assoc');
    }

    public function deletarTodos() {

        $this->out('Deletando os dados');
        TableRegistry::get('Status')->deleteAll(['id !=' => 0]);
        $this->hr();

        $conn = ConnectionManager::get('default');
        $conn->execute("ALTER TABLE status AUTO_INCREMENT = 1; ");
    }

}
