<?php

namespace App\Shell\Task;

use Cake\Console\Shell;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;

/**
 * Simple console wrapper around Psy\Shell.
 */
class ImportarClientesTask extends Shell {

    /**
     * Start the shell and interactive console.
     *
     * @return int|null
     */
    public function main() {
        $this->hr();
        $this->out('Importação Clientes');
        $inicio = Time::now();
        $this->out('Comecando as: ' . $inicio);
        $this->hr();
        $this->out();
        $this->out();

        $conn = ConnectionManager::get('default');
        $conn->begin();

        $this->deletarTodos();
        $this->migrar();

        $conn->commit();
    }

    public function migrar() {
        $tableVendedores = TableRegistry::get('Vendedores');
        $tableEnderecos = TableRegistry::get('Enderecos');
        $tablePessoasJuridicas = TableRegistry::get('PessoasJuridicas');
        $tableCategoriaVarejo = TableRegistry::get('CategoriasVarejos');
        $table = TableRegistry::get('Clientes');

        $entidades = $this->listarOld();
        $count = 0;
        foreach ($entidades as $item) {
            $count++;
            if (($count % 100) == 0)  {
                $this->out("Total CLientes $count - Cliente " . $item['fantasia']);
            }

            $vendedorId = $tableVendedores->findByCodigo($item['vendedor']);
            if (empty($vendedorId)) {
                continue;
            }

            $categoriaVarejoId = $tableCategoriaVarejo->findByCodigo($item['canal']);
            if (empty($categoriaVarejoId)) {
                continue;
            }

            $dataBloqueio = null;
            if (!empty($item['data_bloqueio']) && $item['data_bloqueio'] !== '0000-00-00') {
                $dataBloqueio = new Time($item['data_bloqueio']);
            }

            $ultimaCompra = null;
            if (!empty($item['ultima_compra']) && $item['ultima_compra'] !== '0000-00-00') {
                $ultimaCompra = new Time($item['ultima_compra']);
            }

            $criado = Time::now();
            if (!empty($criado) && $item['data_inclusao'] !== '0000-00-00') {
                $criado = new Time($item['data_inclusao']);
            }

            $modificado = Time::now();
            if (!empty($item['data_alteracao']) && $item['data_alteracao'] !== '0000-00-00') {
                $modificado = new Time($item['data_alteracao']);
            }

            $dadosCliente = [
                'regiao' => $item['regiao'],
                'numero' => $item['numero'], //Campo de verificação//Ou 103
                'codigo' => $item['codigo'],
                'criado' => $criado,
                'modificado' => $modificado,
                'data_bloqueio' => $dataBloqueio,
                'pessoa_juridica_id' => '',
                //pessoa juridica
                'pessoasJuridica' => [
                    'id' => '',
                    'cnpj' => '00000000000000',
                    'nome_fantasia' => $item['fantasia'],
                    'razao_social' => $item['razao'],
                    'endereco_id' => '',
                    'endereco' => [
                        'id' => '',
                        'rua' => $item['rua'],
                        'numero' => ' ',
                        'complemento' => '',
                        'cidade' => $item['cidade'],
                        'bairro' => $item['bairro'],
                        'uf' => 'RS',
                        'cep' => soNumeros($item['cep']),
                    ],
                ],
                'vendedores_id' => $vendedorId,
                'categoria_varejo_id' => $categoriaVarejoId,
                'canal' => $item['canal'],
                'pasta' => $item['pasta'],
                'seq' => $item['seq'],
                'bw' => $item['bw'],
                'motivo_bloqueio' => '',
                'codigo_bloqueio' => $item['codigo_bloqueio'],
                'ultima_compra' => $ultimaCompra,
                'telefones' => [
                    converterTelefoneCliente($item['fone1']),
                    converterTelefoneCliente($item['fone2']),
                    converterTelefoneCliente($item['cel1'], 'Celular'),
                    converterTelefoneCliente($item['cel2'], 'Celular'),
                ],
            ];

            $associacoes =  [
                'PessoasJuridicas' => [
                    'Enderecos'
                ],
                'Telefones'
            ];

            $entidade = $table->montar($dadosCliente, null, $associacoes);
            $salvo = $table->save($entidade, ['associated' => $associacoes]);

            if (!$salvo) {
                throw new \Exception("Erro ao salvar o Cliente");
            }

            if (empty($salvo->pessoasJuridica->endereco_id) && !empty($salvo->pessoasJuridica->endereco)) {

                $retorno = $tableEnderecos->salvar($salvo->pessoasJuridica->endereco);

                if ($retorno['status']) {
                    $juridica = $salvo->pessoasJuridica;
                    $juridica->endereco_id = $retorno['entidade']->id;
                    $tablePessoasJuridicas->save($juridica);
                }

            }
        }

        $this->out("Total Final: $count");
        $this->hr();
    }

    public function listarOld() {

        $conn = ConnectionManager::get('bd_old');
        return $conn->execute("
            select  * from sao_rafael.clientes
        ")->fetchAll('assoc');
    }

    public function deletarTodos() {

        $this->out('Deletando os dados');
        TableRegistry::get('Inadimplencias')->deleteAll(['id !=' => 0]);
        TableRegistry::get('Cev')->deleteAll(['id !=' => 0]);
        TableRegistry::get('PrazosPedidos')->deleteAll(['id !=' => 0]);
        TableRegistry::get('Pedidos')->deleteAll(['id !=' => 0]);
        TableRegistry::get('PrazosPedidosHistoricos')->deleteAll(['id !=' => 0]);
        TableRegistry::get('PedidosHistoricos')->deleteAll(['id !=' => 0]);
        TableRegistry::get('ClientesTelefones')->deleteAll(['id !=' => 0]);
        TableRegistry::get('Telefones')->deleteAll(['id !=' => 0]);
        TableRegistry::get('Clientes')->deleteAll(['id !=' => 0]);
        TableRegistry::get('PessoasJuridicas')->deleteAll(['id !=' => 0]);
        TableRegistry::get('Enderecos')->deleteAll(['id !=' => 0]);
        $this->hr();

        $conn = ConnectionManager::get('default');
        $conn->execute("ALTER TABLE cev AUTO_INCREMENT = 1; ");
        $conn->execute("ALTER TABLE inadimplencias AUTO_INCREMENT = 1; ");
        $conn->execute("ALTER TABLE prazos_pedidos AUTO_INCREMENT = 1; ");
        $conn->execute("ALTER TABLE pedidos AUTO_INCREMENT = 1; ");
        $conn->execute("ALTER TABLE prazos_pedidos_historicos AUTO_INCREMENT = 1; ");
        $conn->execute("ALTER TABLE pedidos_historicos AUTO_INCREMENT = 1; ");
        $conn->execute("ALTER TABLE clientes_telefones AUTO_INCREMENT = 1; ");
        $conn->execute("ALTER TABLE enderecos AUTO_INCREMENT = 1; ");
        $conn->execute("ALTER TABLE telefones AUTO_INCREMENT = 1; ");
        $conn->execute("ALTER TABLE clientes AUTO_INCREMENT = 1; ");
        $conn->execute("ALTER TABLE pessoas_juridicas AUTO_INCREMENT = 1; ");
    }

}
