<?php

namespace App\Shell\Task;

use Cake\Console\Shell;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;

/**
 * Simple console wrapper around Psy\Shell.
 */
class ImportarFundamentosBrkTask extends Shell {

    /**
     * Start the shell and interactive console.
     *
     * @return int|null
     */
    public function main() {
        $this->hr();
        $this->out('Importação ImportarFundamentosBrk');
        $inicio = Time::now();
        $this->out('Comecando as: ' . $inicio);
        $this->hr();
        $this->out();
        $this->out();

        $conn = ConnectionManager::get('default');
        $conn->begin();

        $this->deletarTodos();
        $this->migrar();

        $conn->commit();
    }

    public function migrar() {
        $table = TableRegistry::get('FundamentosBrk');
        $fundamentosBrkPessoasTable = TableRegistry::get('FundamentosBrkPessoas');
        $entidades = $this->listarOld();

        $count = 0;
        foreach ($entidades as $item) {
            $count++;
            if ($count % 100) {
                $this->out('Fundamento total '. $count . ' - ID ' . $item['id']);
            }

            if($item['ano'] == '' && $item['mes'] == ''){
                $item['ano'] = date('Y');
                $item['mes'] = date('m');
            }
            $mesAno = $item['ano'] . '-' . $item['mes'] . '-01';

            $entidade = $table->newEntity();

            $entidade->tendencias = $item['seiscentos'];
            $entidade->trezentos = $item['trezentos'];
            $entidade->especiais = $item['especiais'];
            $entidade->outros = $item['outros'];
            $entidade->nao_alc = $item['nao_alc'];
            $entidade->coringa = $item['coringa'];
            $entidade->total_volume = $item['total_volume'];
            $entidade->refrigeracao = $item['refrigeracao'];
            $entidade->iv = $item['iv'];
            $entidade->coaching = $item['coaching'];
            $entidade->total_execucao = $item['total_execucao'];
            $entidade->total_volume = $item['total_volume'];
            $entidade->cobertura = $item['cobertura'];
            $entidade->demp = $item['demp'];
            $entidade->total_distribuicao = $item['total_distribuicao'];
            $entidade->devolucao = $item['devolucao'];
            $entidade->trocas = $item['trocas'];
            $entidade->terceiros = $item['terceiros'];
            $entidade->total_outros = $item['total_outros'];
            $entidade->total_fundamentos = $item['total_fundamentos'];
            $entidade->ano_mes = $mesAno;

            $save = $table->save($entidade);

            if (!$save) {
                debug($entidade);
                die;
            }

            $tipoPessoaId = $table->buscarPessoaCargo($item['pessoa']);
            if(empty($tipoPessoaId)){
                continue;
            }

            $brkPessoa = $fundamentosBrkPessoasTable->newEntity();
            $brkPessoa->fundamentos_brk_id = $entidade->id;

            switch ($tipoPessoaId['tipo']){
                case 'v':
                    $brkPessoa->vendedores_id = $tipoPessoaId['id'];
                    break;
                case 'g':
                    $brkPessoa->gerentes_id = $tipoPessoaId['id'];
                    break;
                case 's':
                    $brkPessoa->supervisores_id = $tipoPessoaId['id'];
                    break;
            }

            if (!$fundamentosBrkPessoasTable->save($brkPessoa)) {
                debug($brkPessoa);
                die;
            }
        }
        $this->hr();
    }

    public function listarOld() {

        $conn = ConnectionManager::get('bd_old');
        return $conn->execute("select * from sao_rafael.fundamentos_brk")->fetchAll('assoc');
    }

    public function deletarTodos() {

        $this->out('Deletando os dados');
        TableRegistry::get('FundamentosBrkPessoas')->deleteAll(['id !=' => 0]);
        TableRegistry::get('FundamentosBrk')->deleteAll(['id !=' => 0]);

        $this->hr();

        $conn = ConnectionManager::get('default');
        $conn->execute("ALTER TABLE fundamentos_brk AUTO_INCREMENT = 1; ");
        $conn->execute("ALTER TABLE fundamentos_brk_pessoas AUTO_INCREMENT = 1; ");
    }

    public function idVendedores($codigo) {
        $vendendorId = TableRegistry::get('Vendedores')->findByCodigo($codigo, false);
        if (!empty($vendendorId)) {
            return $vendendorId;
        }
        $supervisorId = TableRegistry::get('Supervisores')->findByCodigo($codigo, false);
        if (!empty($supervisorId)) {
            return $supervisorId;
        }
        $gerenteId = TableRegistry::get('Gerentes')->findByCodigo($codigo, false);
        if (!empty($gerenteId)) {
            return $gerenteId;
        }
    }
}
