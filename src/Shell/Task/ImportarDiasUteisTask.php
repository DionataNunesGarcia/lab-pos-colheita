<?php

namespace App\Shell\Task;

use Cake\Console\Shell;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;

/**
 * Simple console wrapper around Psy\Shell.
 */
class ImportarDiasUteisTask extends Shell {

    /**
     * Start the shell and interactive console.
     *
     * @return int|null
     */
    public function main() {
        $this->hr();
        $this->out('Importação Dias Uteis');
        $inicio = Time::now();
        $this->out('Comecando as: ' . $inicio);
        $this->hr();
        $this->out();
        $this->out();

        $conn = ConnectionManager::get('default');
        $conn->begin();

        $this->deletarTodos();
        $this->migrar();

        $conn->commit();
    }

    public function migrar() {
        $table = TableRegistry::get('DiasUteis');
        $entidades = $this->listarOld();

        foreach ($entidades as $item) {
            $this->out('Dia ' . $item['data']);

            $entidade = $table->novo();

            $entidade->data = $item['data'];
            $entidade->util = $item['util'];
            $entidade->crescente = $item['crescente'];
            $entidade->decrecente = $item['decrecente'];

            $save = $table->save($entidade);
            if (!$save) {
                debug($entidade);
                die;
            }
        }
        $this->hr();
    }

    public function listarOld() {

        $conn = ConnectionManager::get('bd_old');
        return $conn->execute("
            select 
                id,
                data,
                case 
                    when util = 'S' then 1
                    else 0
                end util,
                cresc crescente,
                decresc decrecente
            from sao_rafael.dias_uteis 
        ")->fetchAll('assoc');
    }

    public function deletarTodos() {

        $this->out('Deletando os dados');
        TableRegistry::get('DiasUteis')->deleteAll(['id !=' => 0]);
        $this->hr();

        $conn = ConnectionManager::get('default');
        $conn->execute("ALTER TABLE dias_uteis AUTO_INCREMENT = 1; ");
    }

}
