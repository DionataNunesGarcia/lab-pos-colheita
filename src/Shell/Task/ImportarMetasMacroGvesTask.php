<?php

namespace App\Shell\Task;

use Cake\Console\Shell;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;

/**
 * Simple console wrapper around Psy\Shell.
 */
class ImportarMetasMacroGvesTask extends Shell {

    /**
     * Start the shell and interactive console.
     *
     * @return int|null
     */
    public function main() {
        $this->hr();
        $this->out('Importação MetasMacrosGves');
        $inicio = Time::now();
        $this->out('Comecando as: ' . $inicio);
        $this->hr();
        $this->out();
        $this->out();

        $conn = ConnectionManager::get('default');
        $conn->begin();

        $this->deletarTodos();
        $this->migrar();

        $conn->commit();
    }

    public function migrar() {
        $table = TableRegistry::get('MetasMacrosGves');
        $entidades = $this->listarOld();

        foreach ($entidades as $item) {
            $this->out('nome ' . $item['nome']);

            $entidade = $table->novo();

            $entidade->nome = $item['nome'];
            $entidade->sigla = $item['sigla'];
            $entidade->valor_meta = $item['valor_meta'];
            $entidade->mes_validade = $item['mes_validade'];
            $entidade->ativo = $item['ativo'];

            $save = $table->save($entidade);
            if (!$save) {
                debug($entidade);
                die;
            }
        }
        $this->hr();
    }

    public function listarOld() {

        $conn = ConnectionManager::get('bd_old');
        return $conn->execute("select * from sao_rafael.metas_macro_gve")->fetchAll('assoc');
    }

    public function deletarTodos() {

        $this->out('Deletando os dados');
        TableRegistry::get('MetasMacrosGves')->deleteAll(['id !=' => 0]);
        $this->hr();

        $conn = ConnectionManager::get('default');
        $conn->execute("ALTER TABLE metas_macros_gves AUTO_INCREMENT = 1; ");
    }
}
