<?php

/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     3.0.0
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */

namespace App\Shell;

use Cake\Console\ConsoleOptionParser;
use Cake\Console\Shell;
use Cake\Log\Log;
use Psy\Shell as PsyShell;
use Cake\I18n\Time;
use Cake\Console\TaskRegistry;

/**
 * Simple console wrapper around Psy\Shell.
 */
class ImportacoesMensaisShell extends Shell {

    public $tasks = [
        'MensaisMetas',
        'DiariasPedidos',
    ];

    /**
     * Start the shell and interactive console.
     *
     * @return bool|int|void|null
     */
    public function main() {

        $this->out('Olá, para importar tabelas execute um dos comandos a seguir');
        $this->hr();
        $this->out('metas arquivoId dia-mes-ano');
        $this->out('metasGerais arquivoId dia-mes-ano');
        $this->out('pedidosHistorico arquivoId');

    }

    public function metas($arquivoId, $mesAno) {
        $this->MensaisMetas->importarMetas($arquivoId, $mesAno);
    }

    public function metasGerais($arquivoId, $mesAno) {
        $this->MensaisMetas->importarMetasGerais($arquivoId, $mesAno);
    }

    public function pedidosHistorico($arquivoId) {
        $this->DiariasPedidos->importarPedidosHistorico($arquivoId);
    }
}
