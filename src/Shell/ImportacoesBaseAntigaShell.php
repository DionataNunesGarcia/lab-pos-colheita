<?php

/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     3.0.0
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */

namespace App\Shell;

use Cake\Console\ConsoleOptionParser;
use Cake\Console\Shell;
use Cake\Log\Log;
use Psy\Shell as PsyShell;
use Cake\I18n\Time;

/**
 * Simple console wrapper around Psy\Shell.
 */
class ImportacoesBaseAntigaShell extends Shell {

    public $tasks = [
        'ImportarVendedores',
        'ImportarClientes',
        'ImportarSimples',
        'ImportarInadimplencias',
        'ImportarTabelasGrandes',
        'ImportarProdutosPedidos',
        'ImportarDiasUteis',
        'ImportarMetas',
        'ImportarFundamentosBrk',
        'ImportarMetasGerais',
        'ImportarTrades',
        'ImportarStatus',
        'ImportarMetasMacroGves',
        'ImportarPedidos',
        'ImportarPedidosHistoricos',
        'ImportarPrecos',
        'ImportarPesquisas',
    ];

    /**
     * Start the shell and interactive console.
     *
     * @return int|null
     */
    public function main() {

        $this->out('Olá, para importar tabelas execute um dos comandos a seguir');
        $this->hr();
        $this->out('all');
        $this->out('importarVendedores - Importa vendedores, gerentes e superfisores');
        $this->out('importarClientes - Importa tabela de Clientes e suas relações');
        $this->out('importarInadimplencias - Importa tabela de ImportarInadimplencias e suas relações');
        $this->out('importarSimples - Importa várias tabelas pequenas e médias');
        $this->out('migrarCategoriasVarejos - Importa várias tabelas pequenas e médias');
        $this->out('migrarIav - Importa IAV');
        $this->out('tabelasGrandes - Importa tabelas Grandes');
        $this->out('produtosPedidos - Importa tabelas Produtos e Pedidos');
        $this->out('diasUteis - Importa tabela Dias Uteis');
        $this->out('metas - Importa tabela Metas');
        $this->out('fundamentosBrk - Importa tabela FundamentosBrk');
        $this->out('metasGerais - Importa tabela Metas Gerais');
        $this->out('trades - Importa tabela Trades');
        $this->out('status - Importa tabela Status');
        $this->out('metasMacroGves - Importa tabela MetasMacroGeves');
        $this->out('pedidos - Importa tabela ImportarPedidos');
        $this->out('pedidosHistoricos - Importa tabela ImportarPedidosHistoricos');
        $this->out('precos - Importa tabela Precos');
        $this->out('pesquisas - Importa tabela Pesquisas');
    }

    public function all() {
        $inicio = Time::now();

        $this->importarVendedores();
        $this->importarClientes();
        $this->importarSimples();
        $this->importarInadimplencias();
        $this->tabelasGrandes();
        $this->produtosPedidos();
        $this->diasUteis();
        $this->metas();
        $this->fundamentosBrk();
        $this->metasGerais();
        $this->trades();
        $this->status();
        $this->metasMacroGves();
        $this->pedidos();
        $this->pedidosHistoricos();
        $this->precos();
        $this->pesquisas();

        $fim = Time::now();
        $this->out( "Comecou as $inicio");
        $this->out( "final as $fim");
    }

    public function importarVendedores() {
        $this->ImportarVendedores->main();
    }

    public function importarClientes() {
        $this->ImportarClientes->main();
    }

    public function importarInadimplencias() {
        $this->ImportarInadimplencias->main();
    }

    public function importarSimples() {
        $this->ImportarSimples->main();
    }

    public function migrarCategoriasVarejos() {
        $this->ImportarSimples->migrarCategoriasVarejos();
    }

    public function tabelasGrandes() {
        $this->ImportarTabelasGrandes->main();
    }

    public function migrarIav() {
        $this->ImportarTabelasGrandes->migrarIav();
    }

    public function cev() {
        $this->ImportarTabelasGrandes->migrarCev();
    }

    public function produtosPedidos() {
        $this->ImportarProdutosPedidos->main();
    }

    public function diasUteis() {
        $this->ImportarDiasUteis->main();
    }

    public function metas() {
        $this->ImportarMetas->main();
    }

    public function fundamentosBrk() {
        $this->ImportarFundamentosBrk->main();
    }

    public function metasGerais() {
        $this->ImportarMetasGerais->main();
    }

    public function trades() {
        $this->ImportarTrades->main();
    }

    public function status() {
        $this->ImportarStatus->main();
    }

    public function metasMacroGves() {
        $this->ImportarMetasMacroGves->main();
    }

    public function pedidos() {
        $this->ImportarPedidos->main();
    }

    public function pedidosHistoricos() {
        $this->ImportarPedidosHistoricos->main();
    }

    public function precos() {
        $this->ImportarPrecos->main();
    }

    public function pesquisas() {
        $this->ImportarPesquisas->main();
    }

}
