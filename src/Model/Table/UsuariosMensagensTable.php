<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;
use Cake\I18n\Time;
use Cake\Core\Configure;

/**
 * UsuariosMensagens Model
 *
 * @property \App\Model\Table\UsuariosTable|\Cake\ORM\Association\BelongsTo $Usuarios
 *
 * @method \App\Model\Entity\UsuariosMensagen get($primaryKey, $options = [])
 * @method \App\Model\Entity\UsuariosMensagen newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\UsuariosMensagen[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\UsuariosMensagen|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UsuariosMensagen|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UsuariosMensagen patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\UsuariosMensagen[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\UsuariosMensagen findOrCreate($search, callable $callback = null, $options = [])
 */
class UsuariosMensagensTable extends AppTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('usuarios_mensagens');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Usuarios', [
            'foreignKey' => 'usuario_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Usuarios', [
            'foreignKey' => 'enviado_por_id'
        ]);
        
        $this->addBehavior('Timestamp', [
            'events' => [
                'Model.beforeSave' => [
                    'criado' => 'new',
                    'modificado' => 'always',
                ],
            ]
        ]);
    }
    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('titulo')
            ->maxLength('titulo', 255)
            ->requirePresence('titulo', 'create')
            ->notEmpty('titulo');

        $validator
            ->scalar('assunto')
            ->maxLength('assunto', 255)
            ->requirePresence('assunto', 'create')
            ->notEmpty('assunto');

        $validator
            ->scalar('mensagem')
            ->maxLength('mensagem', 4294967295)
            ->requirePresence('mensagem', 'create')
            ->notEmpty('mensagem');

        $validator
            ->scalar('tipo')
            ->maxLength('tipo', 100)
            ->requirePresence('tipo', 'create')
            ->notEmpty('tipo');

        $validator
            ->boolean('visualizado')
            ->allowEmpty('visualizado');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['usuario_id'], 'Usuarios'));
        $rules->add($rules->existsIn(['enviado_por_id'], 'Usuarios'));

        return $rules;
    }
    
    /**
     * 
     * @return type
     */
    public function mensagensLogado()
    {   
        $userSession = Configure::read('SessionUser');
                
        $query = $this->find()
            ->where([
                'usuario_id' => $userSession['id'],
            ])
            ->order([
                'visualizado' => 'ASC',
                'criado' => 'DESC',
            ])
            ->limit(20);
        
        $retorno = [];
        
        foreach ($query as $item) {
            $item->criado = dateToData($item->criado);
            $retorno[] = $item;
        }
        
        return $retorno;
    }
    
    public function totalNaoLidas()
    {   
        $userSession = Configure::read('SessionUser');
        
        return $this->find()
            ->where([
                'usuario_id' => $userSession['id'],
                'visualizado' => false
            ])
            ->count();  
    }
    
    public function buscarMensagem($id)
    {
        $entidade = $this->buscar($id);
        
        if (!$entidade->visualizado) {
            $entidade->visualizado = true;
            $entidade->data_visualizacao = Time::now();
            
            $this->save($entidade);
        }
        
        return $entidade;
    }

    public function salvarMensagem()
    {
        $conn = ConnectionManager::get('default');
        $conn->begin();

        $userSession = Configure::read('SessionUser');
        $entidade = $this->montar($this->getRequestData());

        try {
            foreach ($this->getRequestData()['usuarios_id'] as $usuarioId) {

                $entidade = $this->montar($this->getRequestData(), null);

                $entidade->usuario_id = $usuarioId;
                $entidade->enviado_por_id = $userSession['id'];
                $entidade->tipo = "Enviada por um Usuário";
                $entidade->visualizado = false;
                $entidade->data_visualizacao = null;

                $this->save($entidade);
            }

            $conn->commit();
        } catch (\PDOException $e) {
            $this->retorno['status'] = false;
            $this->retorno['entidade'] = $entidade;
            $this->retorno['mensagem'] = $e->getMessage();
            $conn->rollback();
            return $this->retorno;
        }

        return $this->retorno;
    }

}
