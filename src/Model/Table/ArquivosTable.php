<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;
use Cake\I18n\Time;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;

/**
 * Arquivos Model
 *
 * @method \App\Model\Entity\Arquivo get($primaryKey, $options = [])
 * @method \App\Model\Entity\Arquivo newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Arquivo[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Arquivo|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Arquivo|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Arquivo patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Arquivo[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Arquivo findOrCreate($search, callable $callback = null, $options = [])
 */
class ArquivosTable extends AppTable {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('arquivos');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Xety/Cake3Upload.Upload', [
            'fields' => [
                'arquivo' => [
                    'path' => '/upload/arquivos/:md5',
                ]
            ],
            'suffix' => '_upload',
            'prefix' => '../'
        ]);

        $this->belongsTo('Usuario', [
            'foreignKey' => 'usuario_id',
            'className' => 'Usuarios',
            'joinType' => 'LEFT'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmpty('id', 'create');

        $validator
                ->scalar('arquivo')
                ->maxLength('arquivo', 255)
                ->requirePresence('arquivo', 'create')
                ->notEmpty('arquivo');

        $validator
                ->scalar('tipo')
                ->allowEmpty('tipo');

        $validator
                ->scalar('titulo')
                ->maxLength('titulo', 255)
                ->allowEmpty('titulo');

        $validator
                ->dateTime('criado')
                ->allowEmpty('criado');

        return $validator;
    }

    public function novo() {
        return $this->newEntity();
    }

    public function buscar($id) {
        return $this->get($id, ['contain' => ['Usuario']]);
    }

    public function salvar($arquivo, $tipo = '', $titulo = '', ...$args) {

        $conn = ConnectionManager::get('default');
        $conn->begin();
        $userSession = Configure::read('SessionUser');

        ini_set('post_max_size', '64M');
        ini_set('upload_max_filesize', '64M');

        $existe = $this
            ->exists([
                'tipo' => $tipo,
                'importando' => false,
                'importado' => false,
                'criado >=' => Time::now()->i18nFormat('yyyy-MM-dd 00:00:00'),
                'criado <=' => Time::now()->i18nFormat('yyyy-MM-dd 23:59:59'),
            ]);

        if ($existe) {
            throw new \Exception(__('Arquivo cadastrado e aguardando importação.'), 400);
        }

        $entidade = $this->novo();
        try {

            $entidade->arquivo_upload = $arquivo;
            $entidade->tipo = $tipo;
            $entidade->titulo = $titulo;
            $entidade->criado = Time::now();
            $entidade->tentativa_importacao = 0;
            $entidade->importando = false;
            $entidade->importado = false;
            $entidade->observacoes = null;
            $entidade->observacoes_erro = null;
            $entidade->usuario_id = $userSession['id'];
            $entidade->mais_parametros = !empty($args) ? implode(',', $args) : null;

            $retorno = $this->save($entidade);

            $this->retorno['entidade'] = $entidade;

            if (!$retorno) {
                $this->retorno['status'] = false;
                $this->retorno['mensagem'] = montaMensagemErros($entidade->getErrors());
                $conn->rollback();
                return $this->retorno;
            }
        } catch (\PDOException $e) {

            $this->retorno['status'] = false;
            $this->retorno['entidade'] = $entidade;
            $this->retorno['mensagem'] = $e->getMessage();
            $conn->rollback();
            return $this->retorno;
        }

        $conn->commit();

        return $this->retorno;
    }

    public function buscarImportar($id)
    {
        $entidade = $this->buscar($id);

        if($entidade->importando && $entidade->inicio_importacao >= Time::now()->modify('-7 minutes')){

            $this->retorno['status'] = false;
            $this->retorno['mensagem'] = 'Arquivo já está sendo importado, iniciou na data de ' . dateToDataTime($entidade->inicio_importacao) . '.';
        }else if($entidade->importado){

            $this->retorno['status'] = false;
            $this->retorno['mensagem'] = 'Arquivo já foi importado na data de ' . dateToDataTime($entidade->final_importacao) . '.';
        }else if(!file_exists (str_replace('/', DS, WWW_ROOT . $entidade->arquivo))){

            $this->retorno['status'] = false;
            $this->retorno['mensagem'] = 'Arquivo não foi encontrado no sistema.';
        }else{

            $entidade->importando = true;
            $entidade->inicio_importacao = Time::now();
            $entidade->tentativa_importacao +=1;

            $this->save($entidade);
        }

        $this->retorno['entidade'] = $entidade;

        return $this->retorno;
    }

    public function iniciaImportacao($id)
    {
        $entidade = $this->buscar($id);
        $entidade->importando = true;
        $entidade->inicio_importacao = Time::now();
        $entidade->tentativa_importacao = $entidade->tentativa_importacao + 1;

        $this->save($entidade);
    }

    public function finalizaImportacao($id, $observacoes = '', $mensagensErro = '')
    {
        $entidade = $this->buscar($id);

        if($mensagensErro !== ''){
            $entidade->observacoes_erro = $mensagensErro
                . "\n------------\n"
                . $entidade->observacoes_erro;
        }

        $entidade->observacoes = $observacoes;
        $entidade->importado = true;
        $entidade->importando = false;
        $entidade->final_importacao = Time::now();

        $this->save($entidade);

        $this->enviarMensagem($entidade);
    }

    public function enviarMensagem($entidade)
    {
        $mensagensTable = TableRegistry::get('UsuariosMensagens');
        $mensagem = $mensagensTable->novo();

        $mensagem->titulo = $entidade->titulo;
        $mensagem->assunto = "Arquivo do tipo \"{$entidade->tipo}\" importado na data de " . Time::now() . ".";
        $mensagem->mensagem = $entidade->observacoes;
        $mensagem->tipo = $entidade->titulo;
        $mensagem->usuario_id = $entidade->usuario_id;
        $mensagem->enviado_por_id = null;
        $mensagem->visualizado = false;
        $mensagem->data_visualizacao = null;
        $mensagem->criado = Time::now();

        $mensagensTable->save($mensagem);
    }

    public function getLastUpdate()
    {
        $last = $this
            ->find()
            ->select()
            ->orderDesc('criado')
            ->first();

        return $last->criado ?? null;
    }
}
