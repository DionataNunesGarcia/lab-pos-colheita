<?php
namespace App\Model\Table;

use Cake\Datasource\ConnectionManager;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Sobre Model
 *
 * @method \App\Model\Entity\Sobre get($primaryKey, $options = [])
 * @method \App\Model\Entity\Sobre newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Sobre[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Sobre|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Sobre|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Sobre patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Sobre[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Sobre findOrCreate($search, callable $callback = null, $options = [])
 */
class SobreTable extends AppTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('sobre');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('titulo')
            ->maxLength('titulo', 255)
            ->requirePresence('titulo', 'create')
            ->notEmpty('titulo');

        $validator
            ->scalar('sobre')
            ->requirePresence('sobre', 'create')
            ->notEmpty('sobre');

        $validator
            ->scalar('servicos')
            ->requirePresence('servicos', 'create')
            ->notEmpty('servicos');

        $validator
            ->scalar('missao')
            ->requirePresence('missao', 'create')
            ->notEmpty('missao');

        $validator
            ->scalar('visao')
            ->requirePresence('visao', 'create')
            ->notEmpty('visao');

        $validator
            ->scalar('valores')
            ->requirePresence('valores', 'create')
            ->notEmpty('valores');

        $validator
            ->dateTime('modificado')
            ->allowEmpty('modificado');

        return $validator;
    }

    public function buscar($id = null)
    {
        return $this->find()->first();
    }


    public function salvar($dados, $id = NULL)
    {
        $conn = ConnectionManager::get('default');
        $conn->begin();

        $entidade = $this->montar($dados, $id);
        try {

            $retorno = $this->save($entidade);

            $this->retorno['entidade'] = $entidade;

            if (!$retorno) {
                $this->retorno['status'] = false;
                $this->retorno['mensagem'] = montaMensagemErros($entidade->getErrors());
                $conn->rollback();
                return $this->retorno;
            }
        } catch (\PDOException $e) {

            $this->retorno['status'] = false;
            $this->retorno['entidade'] = $entidade;
            $this->retorno['mensagem'] = $e->getMessage();
            $conn->rollback();
            return $this->retorno;
        }

        $conn->commit();

        return $this->retorno;
    }

}
