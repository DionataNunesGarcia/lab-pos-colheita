<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;

/**
 * ParametrosSistema Model
 *
 * @method \App\Model\Entity\ParametrosSistema get($primaryKey, $options = [])
 * @method \App\Model\Entity\ParametrosSistema newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ParametrosSistema[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ParametrosSistema|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ParametrosSistema|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ParametrosSistema patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ParametrosSistema[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ParametrosSistema findOrCreate($search, callable $callback = null, $options = [])
 */
class ParametrosSistemaTable extends AppTable {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('parametros_sistema');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmpty('id', 'create');

        $validator
                ->scalar('razao_social')
                ->maxLength('razao_social', 255)
                ->requirePresence('razao_social', 'create')
                ->notEmpty('razao_social');

        $validator
                ->scalar('nome_fantasia')
                ->maxLength('nome_fantasia', 255)
                ->requirePresence('nome_fantasia', 'create')
                ->notEmpty('nome_fantasia');

        $validator
                ->scalar('cnpj_cpf')
                ->maxLength('cnpj_cpf', 255)
                ->requirePresence('cnpj_cpf', 'create')
                ->notEmpty('cnpj_cpf');

        $validator
                ->boolean('gerar_log')
                ->requirePresence('gerar_log', 'create')
                ->notEmpty('gerar_log');

        $validator
                ->scalar('emails')
                ->maxLength('emails', 255)
                ->requirePresence('emails', 'create')
                ->notEmpty('emails');

        return $validator;
    }

    // In a table or behavior class
    public function beforeMarshal(\Cake\Event\Event $event, \ArrayObject $data, \ArrayObject $options) {
        if (isset($data['cnpj_cpf'])) {
            $data['cnpj_cpf'] = soNumeros($data['cnpj_cpf']);
        }
        if (isset($data['indice_meta_fundamento'])) {
            $data['indice_meta_fundamento'] = valorToValue($data['indice_meta_fundamento']);
        }
        if (isset($data['salario_base_vendedor_menos_3_anos'])) {
            $data['salario_base_vendedor_menos_3_anos'] = valorToValue($data['salario_base_vendedor_menos_3_anos']);
        }
        if (isset($data['salario_base_vendedor_mais_3_anos'])) {
            $data['salario_base_vendedor_mais_3_anos'] = valorToValue($data['salario_base_vendedor_mais_3_anos']);
        }
        if (isset($data['comissao_vendedor_menos_1_ano'])) {
            $data['comissao_vendedor_menos_1_ano'] = valorToValue($data['comissao_vendedor_menos_1_ano']);
        }
        if (isset($data['comissao_vendedor_mais_1_ano'])) {
            $data['comissao_vendedor_mais_1_ano'] = valorToValue($data['comissao_vendedor_mais_1_ano']);
        }
        if (isset($data['meta_material_pesado_vendedor'])) {
            $data['meta_material_pesado_vendedor'] = valorToValue($data['meta_material_pesado_vendedor']);
        }
        if (isset($data['emails'])) {
            $data['emails'] = implode(',', $data['emails']);
        }
    }

    public function buscar($id = null) {
        return $this->find()->first();
    }

    public function salvar($dados, $id = NULL) {

        $conn = ConnectionManager::get('default');
        $conn->begin();

        $entidade = $this->montar($dados, $id);
        try {

            $retorno = $this->save($entidade);

            $this->retorno['entidade'] = $entidade;

            if (!$retorno) {
                $this->retorno['status'] = false;
                $this->retorno['mensagem'] = montaMensagemErros($entidade->getErrors());
                $conn->rollback();
                return $this->retorno;
            }
        } catch (\PDOException $e) {

            $this->retorno['status'] = false;
            $this->retorno['entidade'] = $entidade;
            $this->retorno['mensagem'] = $e->getMessage();
            $conn->rollback();
            return $this->retorno;
        }

        $conn->commit();

        return $this->retorno;
    }

    public function gerarLogs() {
        return $this->buscar()->gerar_logs;
    }

    public function lembrarSenha() {
        return $this->buscar()->lembrar_senha;
    }

    public function emailsSistema() {
        return $this->buscar()->emails_sistema;
    }

    public function indiceMetaFundamento() {
        return $this->buscar()->indice_meta_fundamento;
    }
}
