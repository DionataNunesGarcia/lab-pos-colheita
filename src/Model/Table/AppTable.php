<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\I18n\Time;
use Cake\Core\Exception\Exception;
use Cake\Event\Event;
use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;

class AppTable extends Table {

    public $requestData = [];

    /**
     * @var string
     */
    public $mensagemLog = '';

    /**
     * @var string
     */
    public $mensagensErro = '';

    /**
     * @var Date
     */
    public $inicio;

    /**
     * @var Date
     */
    public $fim;

    /**
     * @var Date
     */
    public $dateInterval;

    /**
     * @var int
     */
    public $total = 0;

    /**
     * @var int
     */
    public $totalError = 0;

    /**
     * @var int
     */
    public $totalSuccess = 0;

    /**
     * @return array
     */
    public function getRequestData()
    {
        return $this->requestData;
    }

    /**
     * @param array $requestData
     */
    public function setRequestData($requestData)
    {
        $this->requestData = $requestData;
    }

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);
        $this->addBehavior('Timestamp', [
            'events' => [
                'Model.beforeSave' => [
                    'criado' => 'new',
                    'modificado' => 'always',
                ],
            ]
        ]);
    }

    protected $retorno = [
        'status' => true,
        'entidade' => null,
        'mensagem' => null,
    ];

    protected $ignoreLogs = [
        'logs_alteracoes',
        'niveis_permissoes',
        'clientes_telefones',
        'telefones',
        'dias_uteis',
        'coringas_codigos',
        'nomenclaturas_codigos',
        'nomenclaturas_ocorrencias',
    ];

    public function montar($dados, $id = null, $associated = []) {

        if (empty($id)) {
            $entidade = $this->newEntity();
        } else {
            $entidade = $this->get($id, ['contain' => $associated]);
        }

        $entidade = $this->patchEntity($entidade, $dados, ['associated' => $associated]);

        return $entidade;
    }

    public function novo() {
        return $this->newEntity();
    }

    public function buscar($id) {
        return $this->get($id);
    }

    public function salvar($dados, $id = NULL) {
        $conn = ConnectionManager::get('default');
        $conn->begin();
        $entidade = $this->montar($dados, $id);
        try {
            $retorno = $this->save($entidade);

            $this->retorno['entidade'] = $entidade;

            if (!$retorno) {
                $this->retorno['status'] = false;
                $this->retorno['mensagem'] = montaMensagemErros($entidade->getErrors());
                $conn->rollback();
                return $this->retorno;
            }
        } catch (\PDOException $e) {
            $this->retorno['status'] = false;
            $this->retorno['entidade'] = $entidade;
            $this->retorno['mensagem'] = $e->getMessage();
            $conn->rollback();
            return $this->retorno;
        }
        $conn->commit();
        return $this->retorno;
    }

    public function excluir($ids) {
        $resultado = true;
        $error = [];

        $conn = ConnectionManager::get('default');
        $conn->begin();

        if (!empty($ids)) {
            foreach (explode(',', $ids) as $i) {

                $entidade = $this->get($i);
                $resultado = $this->delete($entidade);

                try {

                } catch (\PDOException $e) {
                    $error[] = $e->getMessage();
                }
            }
        }
        if (!empty($error)) {

            $conn->rollback();
            return $error;
        }
        $conn->commit();

        return $resultado;
    }

    public function autocomplete($term = null, $id = null) {

        $query = $this->find('all')->limit(20);

        if (!empty($id)) {
            //Se carregar o id, busca por ele
            $query->where(['id in ' => explode(',',$id)]);
        } else if (!empty($term)) {
            //se pesquisar, busca pelo termo
            $query->where(['upper(nome) like' => '%' . strtoupper($term) . '%']);
        }

        $retorno = [];
        foreach ($query as $result) {
            $retorno[] = [
                'id' => $result->id,
                'value' => $result->nome,
            ];
        }

        return $retorno;
    }

    public function enviarEmail($emailDestinatario, $assunto, $mensagem, $remetente = NULL) {

        if (empty($remetente)) {
            $remetente = 'nome@nome.com';
        }

        $email = new Email('default');
        $email->from([$remetente => 'Nome - Lugar - Site Oficial'])
            ->to($emailDestinatario)
//                ->addCc('email@yahoo.com.br', 'Sistema ')
            ->subject($assunto)
            ->send();
    }

    public function afterSave($event, $entity, $options) {
        $this->gerarLogsSave($event, $entity, $options);
    }

    public function gerarLogsSave($event, $entity, $options) {
        //Verifica se o parametro de gerar logs está ativo
        $parametrosTable = TableRegistry::get('ParametrosSistema');
        if (!$parametrosTable->gerarLogs()) {
            return false;
        }
        //Verifica se a tabela está na lista de ignoradas
        $tabela = $this->_table;
        if (in_array($tabela, $this->ignoreLogs)) {
            return false;
        }
        //Carrega a tabela de logs e o usuário na sessão
        $logsTable = TableRegistry::get('LogsAlteracoes');
        $userSession = Configure::read('SessionUser');

        //Cria os valores novos e antigos para salvar
        $novosValores = [];
        $antigosValores = [];
        foreach ($entity->getDirty() as $k => $i) {
            if($i == 'password'){
                continue;
            }
            if (!empty($entity->getOriginal($i))) {
                $antigosValores[$i] = limitarTexto($entity->getOriginal($i), 500);
            }
            $novosValores[$i] = limitarTexto($entity->$i, 500);
        }

        //verifica se é um conteudo novo para dizer que é uma ação de Criação ou de Alteração
        if ($entity->isNew()) {
            $acao = 'Criado';
            $valorNovo = convertArrayString($novosValores);
            $valorAntigo = '';
        } else {
            $acao = 'Alterado';
            $valorNovo = convertArrayString($novosValores);
            $valorAntigo = convertArrayString($antigosValores);
        }

        //Carrega os dados para salvar
        $dados = [
            'id_tabela' => $entity->id,
            'tabela' => $tabela,
            'usuario' => $userSession['usuario'],
            'acao' => $acao,
            'valor_novo' => $valorNovo,
            'valor_antigo' => $valorAntigo,
        ];

        $logsTable->gerarLogs($dados);
    }

    public function afterDelete($event, $entity, $options) {
        //Verifica se o parametro de gerar logs está ativo
        $parametrosTable = TableRegistry::get('ParametrosSistema');
        if (!$parametrosTable->gerarLogs()) {
            return false;
        }
        //Verifica se a tabela está na lista de ignoradas
        $tabela = $this->_table;
        if (in_array($tabela, $this->ignoreLogs)) {
            return false;
        }
        //Carrega a tabela de logs e o usuário na sessão
        $logsTable = TableRegistry::get('LogsAlteracoes');
        $userSession = Configure::read('SessionUser');

        //Cria os valores antigos para salvar
        $valorAntigo = convertArrayString(json_decode(json_encode($entity), true));
        //Carrega os dados para salvar em logs
        $dados = [
            'tabela' => $this->_table,
            'usuario' => $userSession['login'],
            'acao' => 'Excluído',
            'valor_novo' => '',
            'valor_antigo' => $valorAntigo,
            'id_tabela' => $entity->id,
        ];
        $logsTable->gerarLogs($dados);
    }

    public function buscarPessoaCargo($codigo) {
        $vendedoresTable = TableRegistry::get('Vendedores');
        $gerentesTable = TableRegistry::get('Gerentes');
        $supervisoresTable = TableRegistry::get('Supervisores');
        $vendedorId = $vendedoresTable->findByCodigo($codigo);
        if(!empty($vendedorId)){
            return [
                'tipo' => 'v',
                'id' => $vendedorId
            ];
        }

        $gerenteId = $gerentesTable->findByCodigo($codigo);
        if(!empty($gerenteId)){
            return [
                'tipo' => 'g',
                'id' => $gerenteId
            ];
        }

        $supervisorId = $supervisoresTable->findByCodigo($codigo);
        if(!empty($supervisorId)){
            return [
                'tipo' => 's',
                'id' => $supervisorId
            ];
        }
    }

    // In a table or behavior class
    public function beforeMarshal(\Cake\Event\Event $event, \ArrayObject $data, \ArrayObject $options) {
        if (isset($data['mes_ano'])) {
            $data['mes_ano'] = dataToDate('01/'.$data['mes_ano']);
        }
    }

    public function iniciaLog()
    {
        $this->inicio = Time::now();
        $this->mensagemLog = "<strong>Comecando as:</strong> {$this->inicio}. <br/>";
    }

    public function finalizaLog()
    {
        //Mensagem de tempo
        $this->fim = Time::now();

        $this->mensagemLog .= "--------------------------------------------<br/>";
        $this->mensagemLog .= "<strong>Terminando as:</strong> {$this->fim}. <br/>";

        $this->dateInterval = $this->inicio->diff($this->fim);

        $this->mensagemLog .= "<strong>Tempo total de:</strong> {$this->dateInterval->h} "
            . "hora(s), {$this->dateInterval->m} minuto(s) e {$this->dateInterval->s} segundo(s). <br/>";

        //Mensagem de totais
        $this->mensagemLog .= "<strong>Total salvo com sucesso:</strong> $this->totalSuccess <br/>";
        $this->mensagemLog .= "<strong>Total salvo com erro:</strong> $this->totalError <br/>";
        $this->mensagemLog .= "<strong>Total de linhas válidas:</strong> $this->total <br/>";
    }
}
