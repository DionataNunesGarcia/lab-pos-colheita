<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Arquivo Entity
 *
 * @property int $id
 * @property string $arquivo
 * @property string $tipo
 * @property string $titulo
 * @property \Cake\I18n\FrozenTime $criado
 */
class Arquivo extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'arquivo' => true,
        'arquivo_upload' => true,
        'tipo' => true,
        'mais_parametros' => true,
        'titulo' => true,
        'criado' => true,
        'modificado' => true,
        'tentativa_importacao' => true,
        'importando' => true,
        'inicio_importacao' => true,
        'importado' => true,
        'final_importacao' => true,
        'observacoes' => true,
        'observacoes_erro' => true,
    ];
}
