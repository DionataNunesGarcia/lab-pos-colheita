<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\Auth\DefaultPasswordHasher;

/**
 * Usuario Entity
 *
 * @property int $id
 * @property string $login
 * @property string $senha
 * @property string $imagem
 * @property \Cake\I18n\FrozenTime $criado
 * @property \Cake\I18n\FrozenTime $modificado
 * @property bool $ativo
 * @property bool $deletado
 * @property int $niveis_id
 */
class Usuario extends Entity {

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'usuario' => true,
        'senha' => true,
        'imagem' => true,
        'imagem_upload' => true,
        'criado' => true,
        'modificado' => true,
        'ativo' => true,
        'deletado' => true,
        'niveis_id' => true
    ];

    // Add this method
    protected function _setSenha($value) {
        if (strlen($value)) {
            $hasher = new DefaultPasswordHasher();

            return $hasher->hash($value);
        }
    }
    
    protected function _setDeletado($value) {
        if (empty($value)) {

            return 0;
        }
    }

}
