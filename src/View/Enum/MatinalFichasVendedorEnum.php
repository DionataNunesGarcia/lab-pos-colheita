<?php

namespace App\View\Enum;

class MatinalFichasVendedorEnum
{
    /**
     * Tipos de Relatorios Matinais
     */
    const LISTA_CLIENTES = 'lista_clientes';
    const FOTOGRAFIA_SUCESSO = 'fotografia_sucesso';
    const ABERTURA = 'abertura';
    const CLIENTES_VENDA_ZERO = 'venda_zero';

    /**
     * Array de tipos de Relatórios
     */
    const TYPES = [
        self::LISTA_CLIENTES => 'Lista de Clientes',
        self::FOTOGRAFIA_SUCESSO => 'Fotografia de Sucesso',
        self::ABERTURA => 'Abertura - Ficha do Vendedor',
        self::CLIENTES_VENDA_ZERO => 'Clientes Venda Zero',
    ];

    /**
     * @param $typeUser
     * @return mixed
     */
    public static function getType($typeUser) {
        return self::TYPES[$typeUser];
    }
}