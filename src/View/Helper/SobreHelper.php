<?php

namespace App\View\Helper;
use Cake\View\Helper;
use Cake\View\View;
use Cake\ORM\TableRegistry;

class sobreHelper extends Helper {

    
    public function buscar() {        
        return TableRegistry::get('Sobre')->query()->first();
    }
    
    public function Testimonial() {
        return TableRegistry::get('Testimonial')->query()->where(['ativo' => true]);
    }
    
    public function Portfolio() {
        return TableRegistry::get('Portfolio')->query()->where(['ativo' => true]);
    }
    
    public function galeria() {
        return TableRegistry::get('Galeria')->query();
    }
}
