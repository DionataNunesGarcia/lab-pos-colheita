<?php

/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */

namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use ReflectionClass;
use ReflectionMethod;
use Cake\Core\Configure;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');
     *
     * @return void
     */
    public function initialize() {
        parent::initialize();

        $this->loadComponent('Auth', [
            'authenticate' => [
                'Form' => [
                    'fields' => [
                        'username' => 'usuario',
                        'password' => 'senha',
//                        'finder' => 'auth'
                    ],
                    'userModel' => 'Usuarios'
                ]
            ],
            'loginRedirect' => [
                'controller' => 'Home',
                'action' => 'index',
                'prefix' => 'admin',
            ],
            'logoutRedirect' => [
                'controller' => 'Usuarios',
                'action' => 'login',
                'prefix' => 'admin',
            ],
            'loginAction' => [
                'controller' => 'Usuarios',
                'action' => 'login',
                'prefix' => 'admin',
            ],
            'unauthorizedRedirect' => $this->referer()
        ]);

        $this->loadComponent('RequestHandler', [
            'enableBeforeRedirect' => false,
        ]);
        $this->loadComponent('Flash');

//        $this->loadComponent('Remember');

        $this->loadComponent('Cookie');

        $this->loadComponent('Paginator');

        /*
         * Enable the following component for recommended CakePHP security settings.
         * see https://book.cakephp.org/3.0/en/controllers/components/security.html
         */
        //$this->loadComponent('Security');
    }

    public $paginate = [
        'limit' => 10,
    ];

    /**
     * Before render callback.
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     * @return void
     */
    public function beforeRender(Event $event) {
        if (!array_key_exists('_serialize', $this->viewVars) &&
            in_array($this->response->type(), ['application/json', 'application/xml'])
        ) {
            $this->set('_serialize', true);
        }
    }

    public function beforeFilter(Event $event) {
        if (isset($this->request->params['prefix']) == 'admin') {

            $userSession = $this->Auth->user();

            if (empty($userSession)) {
                $this->Auth->config('authError', false);
            }

            Configure::write('SessionUser', $userSession);

            $this->viewBuilder()->layout('admin');

            $this->set(compact('userSession'));

            if (!empty($userSession) && !$this->temPermissao($userSession)) {
                $this->Flash->error(__('Você não tem permissão para acessar essa URL "' . $this->request->here . '"'));
                return $this->redirect(['controller' => 'Home', 'action' => 'index']);
            }
        }
    }

    protected $ignoreListController = [
        '.',
        '..',
        'Component',
        'AppController.php',
        'HomeController.php',

        //não terá cadastro no sistema
        'CoringasController.php',
        'MetasStmController.php',
        'TradesController.php',
        'DempsController.php',
        'FundamentosController.php',
        'FundamentosStmController.php',
        'FundamentosBrkController.php',
        'RankingController.php',
        'RankingSupervisoresController.php',
        'StatusController.php',
//        'TiposAcordosController.php',
    ];

    public function getControllers($prefix = null) {
        $prefixo = !empty($prefix) ? $prefix . DS : '';

        $files = scandir('../src/Controller/' . $prefixo);
        $results = [];
        $ignoreList = $this->ignoreListController;

        foreach ($files as $file) {
            if (!in_array($file, $ignoreList)) {
                $controller = explode('.', $file)[0];
                array_push($results, str_replace('Controller', '', $controller));
            }
        }
        return $results;
    }

    protected $ignoreListActions = [
        'beforeFilter',
        'afterFilter',
        'initialize',
        'ignoreListActions',
        'autocomplete',
        'cropImageAjax',
        'selecionaVendedor',
        'downloadArquivoPadrao',
    ];

    private function getActionsIgnoreController($controllerName, $prefix = null) {
        //Concatena o prefixo se estiver setado
        $prefixo = !empty($prefix) ? ucfirst($prefix) . '\\' : '';

        //Monta o nome da class
        $className = 'App\\Controller\\' . $prefixo . $controllerName . 'Controller';

        $ignoreListActions = [];
        if (method_exists($className, 'ignoreListActions')) {
            $ignoreListActions = $className::ignoreListActions();
        }

        $ignoreList = array_merge($this->ignoreListActions, $ignoreListActions);

        return $ignoreList;
    }

    public function getActions($controllerName, $prefix = null) {
        //Concatena o prefixo se estiver setado
        $prefixo = !empty($prefix) ? $prefix . '\\' : '';
        //Monta o nome da class
        $className = 'App\\Controller\\' . $prefixo . $controllerName . 'Controller';
        //Reflete a classe
        $class = new ReflectionClass($className);
        //Lista todas as actions publicas
        $actions = $class->getMethods(ReflectionMethod::IS_PUBLIC);

        //Array de actions para ser ignoradas
        $ignoreList = $this->getActionsIgnoreController($controllerName, str_replace('\\', '', $prefixo));

        $results = [$controllerName => []];

        foreach ($actions as $action) {
            if (substr($action->name, -4) === 'Ajax') {
                $ignoreList[] = $action->name;
            }

            if ($action->class == $className && !in_array($action->name, $ignoreList)) {
                array_push($results[$controllerName], $action->name);
            }
        }

        return $results;
    }

    public function getListaPermissoes($prefix = null) {
        $controllers = $this->getControllers($prefix);
        $resources = [];

        foreach ($controllers as $controller) {
            $actions = $this->getActions($controller, $prefix);
            array_push($resources, $actions);
        }
        return $resources;
    }

    private function temPermissao($usuario) {
        $controller = $this->request->controller;
        $action = $this->request->action;

        //Se tiver prefixo preenche, se não deixa null
        $prefix = !empty($this->request->params['prefix']) ? $this->request->params['prefix'] : null;

        //Verifica se o controller atual existe na lista de ignorado
        if (in_array($controller . 'Controller.php', $this->ignoreListController)) {
            return true;
        }

        //Busca a lista de actions ignoradas por controller
        $ignoreListActions = $this->getActionsIgnoreController($controller, $prefix);
        if (in_array($action, $ignoreListActions)) {
            return true;
        }

        if (!empty($usuario)) {
            foreach ($usuario['niveis']['niveisPermissoes'] AS $permissoes) {
                if (
                    (strtolower($permissoes['action']) === strtolower($action)) && (strtolower($permissoes['controller']) === strtolower($controller)) && (strtolower($permissoes['prefix']) === strtolower($prefix))
                ) {
                    return true;
                }
            }
        } else {
            $this->Auth->config('authError', false);
        }

        return false;
    }

    public function cropImageAjax() {
        $this->layout = '';
        if (empty($this->request->getData("image"))) {
            return;
        }

        $imagem = $this->request->getData("image");

        $image_array_1 = explode(";", $imagem);
        $image_array_2 = explode(",", $image_array_1[1]);

        $data = base64_decode($image_array_2[1]);

        $imageName = tempnam(sys_get_temp_dir(), 'upload');

        file_put_contents( $imageName, $data);

        move_uploaded_file($imageName, $imageName);

        $this->set(compact('imageName', 'imagem'));
        $this->render('/Element/admin/imagem-crop');
    }
}
