<?php

namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Vendedores Controller
 *
 * @property \App\Model\Table\VendedoresTable $Vendedores
 *
 * @method \App\Model\Entity\Vendedore[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class VendedoresController extends AppController
{
    /**
     * @return array
     */
    public static function ignoreListActions() {
        return [
            'getRotas'
        ];
    }

    public function initialize()
    {
        parent::initialize();

        $this->loadModel('Clientes');
    }
    /**
     * autocomplete method
     *
     * @return \Cake\Http\Response|void
     */
    public function autocomplete() {

        $this->autoRender = false;

        $retorno = $this->Vendedores->autocomplete(@$this->request->query['term'], @$this->request->query['id']);

        echo json_encode($retorno);
    }

    /**
     * Pesquisar method
     *
     * @return \Cake\Http\Response|void
     */
    public function pesquisar() {
        $this->loadModel('VwVendedores');
        $contitions = [];

        if (!empty($this->request->query())) {
            if ($this->request->query('nome')) {
                $contitions['or'] = [
                    'nome like' => '%' . $this->request->query('nome') . '%',
                ];
            }
            if ($this->request->query('email')) {
                $contitions['or'] = [
                    'email like' => '%' . $this->request->query('email') . '%',
                ];
            }
            if ($this->request->query('cpf')) {
                $contitions['or'] = [
                    'cpf like' => '%' . soNumeros($this->request->query('cpf')) . '%',
                ];
            }
            if ($this->request->query('supervisor')) {
                $contitions['or'] = [
                    'supervisor like' => '%' . $this->request->query('supervisor') . '%',
                ];
            }
        }

        $this->paginate = [
            'conditions' => $contitions,
            'order' => ['nome' => 'ASC']
        ];

        $entidade = $this->paginate($this->VwVendedores);

        $this->set(compact('entidade'));
    }

    public function abrir($id) {
        $entidade = $this->Vendedores->buscar($id);

        $this->set(compact('entidade'));
    }

    public function incluir() {
        $entidade = $this->Vendedores->novo();

        $entidade->pessoaFisica = $this->Vendedores->pessoaFisica->novo();
        $entidade->pessoaFisica->endereco = $this->Vendedores->pessoaFisica->Endereco->novo();
        $entidade->usuario = $this->Vendedores->Usuario->novo();

        $this->set(compact('entidade'));
        $this->render('abrir');
    }

    public function salvar() {

        $id = $this->request->data['id'];
        $dados = $this->request->data;

        try {
            $retorno = $this->Vendedores->salvar($dados, $id);
            $entidade = $retorno['entidade'];

            if (!$retorno['status']) {
                $this->Flash->error(__($retorno['mensagem']));

                $this->set(compact('entidade'));
                $this->render('abrir');
            } else {
                $this->Flash->success(__('O conteúdo foi salvo com sucesso'));
                return $this->redirect(['action' => 'abrir', $entidade->id]);
            }
        } catch (Exception $ex) {

            $this->Flash->error(__($ex->getMessage()));

            $this->set(compact('entidade'));
            $this->render('abrir');
        }
    }

    /**
     * Delete method
     *
     * @param string|null $ids Usuario id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function excluir($ids = null) {

        if (empty($ids)) {
            $this->Flash->error(__('Nenhum registro foi selecionado.'));
            return $this->redirect(['action' => 'pesquisar']);
        }

        $resultado = $this->Vendedores->excluir($ids);

        if ($resultado) {
            $this->Flash->success(__('Registro(s) excluídos com sucesso.'));
        } else {
            $mensagens = '';
            foreach ($resultado as $i) {
                $mensagens .= $i;
            }
            $this->Flash->error(__($mensagens));
        }

        return $this->redirect(['action' => 'pesquisar']);
    }

    public function getRotas($id)
    {
        $this->viewBuilder()->setLayout(false);

        $pastas = $this->Clientes->gePastasVendedor($id);

        $this->set(compact('pastas'));
        $this->set('_serialize', 'pastas');
        $this->render('mostrar_pastas');
    }
}
