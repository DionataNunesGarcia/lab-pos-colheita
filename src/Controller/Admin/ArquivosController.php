<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\I18n\Time;

/**
 * Arquivos Controller
 *
 * @property \App\Model\Table\ArquivosTable $Arquivos
 *
 * @method \App\Model\Entity\Arquivo[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ArquivosController extends AppController
{

    private $_table = 'Arquivos';

    public static function ignoreListActions() {
        return [
            'download',
        ];
    }

    /**
     * autocomplete method
     *
     * @return \Cake\Http\Response|void
     */
    public function autocomplete()
    {

        $this->autoRender = false;

        $retorno = $this->{$this->_table}->autocomplete(@$this->request->getQuery('term'), @$this->request->getQuery('id'));

        echo json_encode($retorno);
    }

    /**
     * Pesquisar method
     *
     * @return \Cake\Http\Response|void
     */
    public function pesquisar()
    {
        $contitions = [];


        if (!empty($this->request->getQuery('datas'))) {
            $datas = explode(' - ', $this->request->getQuery('datas'));
            $inicio = dataToDate($datas[0]);
            $fim = dataToDate($datas[1]);

            $contitions['and']['Arquivos.criado >='] = $inicio;
            $contitions['and']['Arquivos.criado <='] = $fim;
        }

        if (!empty($this->request->getQuery('tipo'))) {
            $contitions['and'] = [
                'tipo' => $this->request->getQuery('tipo'),
            ];
        }

        if (!empty($this->request->getQuery('usuario'))) {
            $contitions['and'] = [
                'Usuario.usuario like' => '%' . $this->request->getQuery('usuario') . '%',
            ];
        }

        $this->paginate = [
            'contain' => ['Usuario'],
            'conditions' => $contitions,
            'order' => ['criado' => 'DESC']
        ];

        $entidade = $this->paginate($this->{$this->_table});

        $this->set(compact('entidade'));
    }

    public function ver($id)
    {
        $entidade = $this->{$this->_table}->buscar($id);

        $this->layout = '';
        $this->set(compact('entidade'));
        $this->render('ver');
    }

    public function download($id)
    {
        $entidade = $this->{$this->_table}->buscar($id);

        $file_path = WWW_ROOT . $entidade->arquivo;
        $this->response->file($file_path, array(
            'download' => true,
            'name' => Time::now()->i18nFormat('yyyy-MM-dd HH-mm-ss')
                . " {$entidade->tipo}."
                . extensao($entidade->arquivo),
        ));

        return $this->response;
    }

}
