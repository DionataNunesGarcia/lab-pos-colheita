<?php

namespace App\Controller\Admin;

use App\Controller\AppController;

class HomeController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
        $this->loadModel('UsuariosMensagens');
    }

    public function index()
    {

    }

    public function mensagensLogado()
    {
        $this->autoRender = false;
        $this->layout = '';

        $retorno = [
            'mensagens' => $this->UsuariosMensagens->mensagensLogado(),
            'total' => $this->UsuariosMensagens->totalNaoLidas()
        ];

        $this->set(compact('retorno'));
        $this->set('_serialize', ['retorno']);
        $this->render('mensagens');
    }

    public function mensagem($id) {
        $entidade = $this->UsuariosMensagens->buscarMensagem($id);

        $this->layout = '';
        $this->set(compact('entidade'));
        $this->render('mensagem');
    }

    /**
     * Pesquisar method
     *
     * @return \Cake\Http\Response|void
     */
    public function pesquisarMensagens() {
        $contitions = [];

        if (!empty($this->request->getQuery())) {
            if ($this->request->getQuery('nome')) {
                $contitions['or'] = [
                    'titulo like' => '%' . $this->request->getQuery('texto') . '%',
                    'assunto like' => '%' . $this->request->getQuery('texto') . '%',
                    'mensagem like' => '%' . $this->request->getQuery('texto') . '%',
                ];
            }
        }

        $this->paginate = [
            'conditions' => $contitions,
            'order' => ['criado' => 'DESC']
        ];

        $entidade = $this->paginate($this->UsuariosMensagens);

        $this->set(compact('entidade'));
    }

}
