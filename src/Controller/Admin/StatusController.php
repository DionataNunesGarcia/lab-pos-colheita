<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Status Controller
 *
 * @property \App\Model\Table\StatusTable $Status
 *
 * @method \App\Model\Entity\Status[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class StatusController extends AppController
{

    private $_table = 'Status';

    /**
     * autocomplete method
     *
     * @return \Cake\Http\Response|void
     */
    public function autocomplete()
    {

        $this->autoRender = false;

        $retorno = $this->{$this->_table}->autocomplete(@$this->request->getQuery('term'), @$this->request->getQuery('id'));

        echo json_encode($retorno);
    }

    /**
     * Pesquisar method
     *
     * @return \Cake\Http\Response|void
     */
    public function pesquisar()
    {
        $contitions = [];

        if (!empty($this->request->getQuery('status'))) {
            $contitions['or'] = [
                'status like' => '%' . $this->request->getQuery('status') . '%',
            ];
        }

        $this->paginate = [
            'conditions' => $contitions,
            'contain' => ['Clientes.PessoasJuridicas'],
            'order' => ['nome' => 'ASC']
        ];

        $entidade = $this->paginate($this->{$this->_table});

        $this->set(compact('entidade'));
    }

    public function abrir($id)
    {
        $entidade = $this->{$this->_table}->buscar($id);

        $this->set(compact('entidade'));
    }

    public function incluir()
    {
        $entidade = $this->{$this->_table}->novo();

        $this->set(compact('entidade'));
        $this->render('abrir');
    }

    public function salvar()
    {

        $id = $this->request->getData('id');
        $dados = $this->request->getData();

        try {
            $retorno = $this->{$this->_table}->salvar($dados, $id);
            $entidade = $retorno['entidade'];

            if (!$retorno['status']) {
                $this->Flash->error(__($retorno['mensagem']));

                $this->set(compact('entidade'));
                $this->render('abrir');
            } else {
                $this->Flash->success(__('O conteúdo foi salvo com sucesso'));
                return $this->redirect(['action' => 'abrir', $entidade->id]);
            }
        } catch (Exception $ex) {

            $this->Flash->error(__($ex->getMessage()));

            $this->set(compact('entidade'));
            $this->render('abrir');
        }
    }

    /**
     * Delete method
     *
     * @param string|null $ids Usuario id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function excluir($ids = null)
    {

        if (empty($ids)) {
            $this->Flash->error(__('Nenhum registro foi selecionado.'));
            return $this->redirect(['action' => 'pesquisar']);
        }

        $resultado = $this->{$this->_table}->excluir($ids);

        if ($resultado) {
            $this->Flash->success(__('Registro(s) excluídos com sucesso.'));
        } else {
            $mensagens = '';
            foreach ($resultado as $i) {
                $mensagens .= $i;
            }
            $this->Flash->error(__($mensagens));
        }

        return $this->redirect(['action' => 'pesquisar']);
    }
    
}
