<?php

namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * ParametrosSistema Controller
 *
 * @property \App\Model\Table\ParametrosSistemaTable $ParametrosSistema
 *
 * @method \App\Model\Entity\ParametrosSistema[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ParametrosSistemaController extends AppController {

    public function abrir() {
        $entidade = $this->ParametrosSistema->buscar();

        $this->set(compact('entidade'));
    }

    public function salvar() {

        $id = $this->request->data['id'];
        $dados = $this->request->data;

        try {
            $retorno = $this->ParametrosSistema->salvar($dados, $id);
            $entidade = $retorno['entidade'];

            if (!$retorno['status']) {
                $this->Flash->error(__($retorno['mensagem']));

                $this->set(compact('entidade'));
                $this->render('abrir');
            } else {
                $this->Flash->success(__('O conteúdo foi salvo com sucesso'));
                return $this->redirect(['action' => 'abrir', $entidade->id]);
            }
        } catch (Exception $ex) {

            $this->Flash->error(__($ex->getMessage()));

            $this->set(compact('entidade'));
            $this->render('abrir');
        }
    }

}
