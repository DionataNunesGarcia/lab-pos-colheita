<?php

namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Niveis Controller
 *
 * @property \App\Model\Table\NiveisTable $Niveis
 *
 * @method \App\Model\Entity\Nivei[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class NiveisController extends AppController {

    /**
     * autocomplete method
     *
     * @return \Cake\Http\Response|void
     */
    public function autocomplete() {

        $this->autoRender = false;

        $retorno = $this->Niveis->autocomplete(@$this->request->query['term'], @$this->request->query['id']);

        echo json_encode($retorno);
    }

    /**
     * Pesquisar method
     *
     * @return \Cake\Http\Response|void
     */
    public function pesquisar() {
        $this->loadModel('VwNiveis');
        $contitions = [];

        if (!empty($this->request->query())) {
            if ($this->request->query('nome')) {
                $contitions['or'] = [
                    'Niveis.nome like' => '%' . $this->request->query('nome') . '%',
                ];
            }
        }

        $this->paginate = [
            'conditions' => $contitions,
            'order' => ['Usuarios.login' => 'ASC']
        ];

        $entidade = $this->paginate($this->VwNiveis);

        $this->set(compact('entidade'));
    }

    public function abrir($id) {
        $entidade = $this->Niveis->buscar($id);
        $listaPermissoes = $this->getListaPermissoes('Admin');
        
        $permissoes = [];
        
        foreach ($entidade->niveisPermissoes as $permissao) {

            $permissoes[] = [
                'id' => $permissao->id,
                'controller' => $permissao->controller,
                'action' => $permissao->action,
                'action_str' => $permissao->controller . '.' . $permissao->action
            ];
        }

        $this->set(compact('entidade', 'listaPermissoes', 'permissoes'));
    }

    public function incluir() {
        $entidade = $this->Niveis->novo();
        $this->set(compact('entidade'));
        $this->render('abrir');
    }

    public function salvar() {

        $id = $this->request->data['id'];
        $dados = $this->request->data;

        try {
            $entidade = $this->Niveis->salvar($dados, $id);

            if (!$entidade) {
                $this->Flash->error(__('O conteúdo não pôde ser salvo. Por favor, tente novamente.'));
                if (!empty($id)) {
                    return $this->redirect(['action' => 'abrir', $id]);
                } else {
                    return $this->redirect(['action' => 'incluir']);
                }
            } else {
                $this->Flash->success(__('O conteúdo foi salvo com sucesso'));
                return $this->redirect(['action' => 'abrir', $entidade->id]);
            }
        } catch (Exception $ex) {

            $this->Flash->error(__('O conteúdo não pôde ser salvo. Por favor, tente novamente.'));

            $this->set(compact('entidade'));
            $this->render('abrir');
        }
    }

    /**
     * Delete method
     *
     * @param string|null $ids Usuario id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function excluir($ids = null) {

        if (empty($ids)) {
            $this->Flash->error(__('Nenhum registro foi selecionado.'));
            return $this->redirect(['action' => 'pesquisar']);
        }

        $resultado = $this->Niveis->excluir($ids);

        if ($resultado) {
            $this->Flash->success(__('Registro(s) excluídos com sucesso.'));
        } else {
            $mensagens = '';
            foreach ($resultado as $i) {
                $mensagens .= $i;
            }
            $this->Flash->error(__($mensagens));
        }

        return $this->redirect(['action' => 'pesquisar']);
    }

}
