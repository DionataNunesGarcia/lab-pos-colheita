<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * LogsAlteracoes Controller
 *
 * @property \App\Model\Table\LogsAlteracoesTable $LogsAlteracoes
 *
 * @method \App\Model\Entity\LogsAlteraco[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class LogsAlteracoesController extends AppController
{

    /**
     * Pesquisar method
     *
     * @return \Cake\Http\Response|void
     */
    public function pesquisar() {
        $contitions = [];

        if (!empty($this->request->query())) {
            if ($this->request->query('nome')) {
                $contitions['or'] = [
                    'Niveis.nome like' => '%' . $this->request->query('nome') . '%',
                ];
            }
        }

        $this->paginate = [
            'conditions' => $contitions,
            'order' => ['data_hora' => 'DESC']
        ];

        $entidade = $this->paginate($this->LogsAlteracoes);

        $this->set(compact('entidade'));
    }

    public function ver($id) {
        $entidade = $this->LogsAlteracoes->buscar($id);
        
        $this->layout = '';
        $this->set(compact('entidade'));
        $this->render('ver');
    }
}
