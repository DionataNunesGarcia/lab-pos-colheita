<?php

namespace App\Controller;

class HomeController extends \App\Controller\AppController {

    public function initialize() {
        parent::initialize();
        $this->Auth->allow();
    }

    public function index() {
        
    }

    
    public function convidado($slug) {
        $this->loadModel('Convidados');
        $convidado = $this->Convidados->buscar_nome($slug);       
        
        $this->set(compact('convidado'));
        $this->render('index');
    }

}
